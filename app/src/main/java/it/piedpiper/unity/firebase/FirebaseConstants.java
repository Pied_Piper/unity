package it.piedpiper.unity.firebase;

public class FirebaseConstants {
    public static String USERS_COLLECTION = "users";
    public static String UNIVERSITIES_COLLECTION = "universities";
    public static String GROUPS_COLLECTION = "groups";
    public static String ASSOCIATIONS_COLLECTION = "associations";
    public static String EVENTS_COLLECTION = "events";
    public static String LESSONS_COLLECTION = "lessons";
    public static String FILES_COLLECTION = "files";
}

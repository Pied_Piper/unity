package it.piedpiper.unity.firebase;


import android.annotation.SuppressLint;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;

public class FirebaseDatabaseReference {
    private FirebaseDatabaseReference() {
    }

    @SuppressLint("StaticFieldLeak")
    public static final FirebaseFirestore DATABASE = FirebaseFirestore .getInstance();
    public static final FirebaseAuth AUTH = FirebaseAuth .getInstance();
    public static final FirebaseUser USER = AUTH.getCurrentUser();
    public static final FirebaseStorage STORAGE = FirebaseStorage.getInstance();
}

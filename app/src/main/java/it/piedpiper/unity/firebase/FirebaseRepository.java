package it.piedpiper.unity.firebase;

import android.net.Uri;
import android.util.Log;

import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.MetadataChanges;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.SetOptions;
import com.google.firebase.firestore.WriteBatch;
import com.google.firebase.storage.StorageReference;

import java.util.Map;

import it.piedpiper.unity.callback.CallBack;
import it.piedpiper.unity.callback.FirebaseCallBack;
import it.piedpiper.unity.model.User;
import it.piedpiper.unity.utils.Constant;

public abstract class FirebaseRepository {

    private static final String TAG = "FirebaseRepository";

    /**
     * Insert data on FireStore
     *
     * @param documentReference Document reference of data to be add
     * @param model             Model to insert into Document
     * @param callback          callback for event handling
     */
    protected final void fireStoreCreate(final DocumentReference documentReference, final Object model, final CallBack callback) {
        documentReference.set(model)
                .addOnSuccessListener(aVoid -> callback.onSuccess(Constant.SUCCESS))
                .addOnFailureListener(callback::onError);
    }

    /**
     * Update data to FireStore
     *
     * @param documentReference Document reference of data to update
     * @param map               Data map to update
     * @param callback          callback for event handling
     */
    protected final void fireStoreUpdate(final DocumentReference documentReference, final Map<String, Object> map, final CallBack callback) {
        documentReference.update(map)
                .addOnSuccessListener(aVoid -> callback.onSuccess(Constant.SUCCESS))
                .addOnFailureListener(callback::onError);
    }

    /**
     * FireStore Create or Merge
     *
     * @param documentReference Document reference of data to create update
     * @param model             Model to create or update into Document
     */
    protected final void fireStoreCreateOrMerge(final DocumentReference documentReference, final Object model, final CallBack callback) {
        documentReference.set(model, SetOptions.merge())
                .addOnSuccessListener(aVoid -> callback.onSuccess(Constant.SUCCESS))
                .addOnFailureListener(callback::onError);
    }

    /**
     * Delete data from FireStore
     *
     * @param documentReference Document reference of data to delete
     * @param callback          callback for event handling
     */
    protected final void fireStoreDelete(final DocumentReference documentReference, final CallBack callback) {
        documentReference.delete()
                .addOnSuccessListener(aVoid -> callback.onSuccess(Constant.SUCCESS))
                .addOnFailureListener(callback::onError);
    }

    /**
     * FireStore Batch write
     *
     * @param batch    Document reference of data to delete
     * @param callback callback for event handling
     */
    protected final void batchWrite(WriteBatch batch, final CallBack callback) {
        batch.commit().addOnCompleteListener(task -> {
            if (task.isSuccessful())
                callback.onSuccess(Constant.SUCCESS);
            else
                callback.onError(Constant.FAIL);
        });
    }

    /**
     * One time data fetch from FireStore with Document reference
     *
     * @param documentReference query of Document reference to fetch data
     * @param callBack          callback for event handling
     */
    protected final void readDocument(final DocumentReference documentReference, final CallBack callBack) {
        documentReference.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                DocumentSnapshot document = task.getResult();
                if (document != null && document.exists()) {
                    callBack.onSuccess(document); //in questo caso ci passo il documento
                } else {
                    callBack.onSuccess(null);
                }
            } else {
                callBack.onError(task.getException());
            }
        });
    }

    /**
     * One time data fetch from FireStore with Document reference
     *
     * @param query query of Document reference to fetch data
     * @param callBack          callback for event handling
     */
    protected final void readDocument(final Query query, final CallBack callBack) {
        query.get()
                .addOnCompleteListener(task -> {
                    if(task.isSuccessful()){
                        if(task.getResult().size() > 0){
                            for (QueryDocumentSnapshot doc : task.getResult()){
                                if(doc.exists()){
                                    Log.d("TAG", "readDocument: " + doc.toObject(User.class).getEmail());
                                    callBack.onSuccess(doc);
                                }
                                else{
                                    callBack.onSuccess(null);
                                }
                            }
                        }
                        else
                            callBack.onSuccess(null);
                    }
                    else {
                        callBack.onSuccess(null);
                    }
                });
    }

    /**
     * Data fetch listener with Document reference
     *
     * @param documentReference to add childEvent listener
     * @param callBack          callback for event handling
     * @return EventListener
     */
    protected final ListenerRegistration readDocumentByListener(final DocumentReference documentReference, final CallBack callBack) {
        return documentReference.addSnapshotListener((snapshot, e) -> {
            if (e != null) {
                callBack.onError(e);
                return;
            }
            if (snapshot != null && snapshot.exists()) {
                callBack.onSuccess(snapshot);
            } else {
                callBack.onSuccess(null);
            }
        });
    }

    /**
     * One time data fetch from FireStore with Query reference
     *
     * @param query    query of Document reference to fetch data
     * @param callBack callback for event handling
     */
    protected final void readQueryDocuments(final Query query, final CallBack callBack) {
        query.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                QuerySnapshot querySnapshot = task.getResult();

                if (querySnapshot != null && !querySnapshot.isEmpty()) {
                    callBack.onSuccess(querySnapshot);
                } else {
                    callBack.onSuccess(null);
                }
            } else {
                callBack.onError(task.getException());
            }
        }).addOnFailureListener(callBack::onError);
    }

    /**
     * Data fetch listener with Query reference
     *
     * @param query    query of Document reference to fetch data
     * @param callBack callback for event handling
     */
    protected final ListenerRegistration readQueryDocumentsByListener(final Query query, final CallBack callBack) {
        return query.addSnapshotListener((value, e) -> {
            if (e != null) {
                callBack.onError(e);
                return;
            }
            callBack.onSuccess(value);
        });
    }

    /**
     * Data fetch ChildEventListener with Query reference
     *
     * @param query    to add childEvent listener
     * @param callBack callback for event handling
     * @return ChildEventListener
     */
    protected final ListenerRegistration readQueryDocumentsByChildEventListener(final Query query, final FirebaseCallBack callBack) {
        return query.addSnapshotListener((snapshots, e) -> {
            if (e != null || snapshots == null || snapshots.isEmpty()) {
                callBack.onCancelled(e);
                return;
            }
            for (DocumentChange documentChange : snapshots.getDocumentChanges()) {
                switch (documentChange.getType()) {
                    case ADDED:
                        callBack.onChildAdded(documentChange.getDocument());
                        break;
                    case MODIFIED:
                        callBack.onChildChanged(documentChange.getDocument());
                        break;
                    case REMOVED:
                        callBack.onChildRemoved(documentChange.getDocument());
                        break;
                }
            }
        });
    }

    /**
     * Read offline data from FireBase
     *
     * @param query Document reference of data to create
     */
    protected final void fireStoreOfflineRead(final Query query, final CallBack callBack) {
        query.addSnapshotListener(MetadataChanges.INCLUDE, (querySnapshot, e) -> {
            if (e != null) {
                callBack.onError(e);
                return;
            }
            callBack.onSuccess(querySnapshot);
        });
    }


    /**
     * Insert data on Firebase Storage
     *
     * @param reference Document reference of data to be add
     * @param data             Model to insert into Document
     * @param callback          callback for event handling
     */
    protected final void storageCreate(final StorageReference reference, final byte[] data, final CallBack callback) {
        reference.putBytes(data).addOnSuccessListener(taskSnapshot -> {
                    Log.d("TAG", "addUserPhotoToStorage: Success");
                    reference.getDownloadUrl()
                            .addOnSuccessListener(uri -> {
                                String photoUrl = uri.toString();
                                callback.onSuccess(photoUrl);
                            });
                }
        )
                .addOnFailureListener(exception -> Log.d("TAG", "addUserPhotoToStorage: Failed to storage photo"));
    }

    /**
     * Insert data on Firebase Storage
     *
     * @param reference Document reference of data to be add
     * @param data             Model to insert into Document
     * @param callback          callback for event handling
     */
    protected final void storageCreate(final StorageReference reference, final Uri data, final CallBack callback) {
        reference.putFile(data).addOnSuccessListener(task -> callback.onSuccess("CREATED"));
    }
}
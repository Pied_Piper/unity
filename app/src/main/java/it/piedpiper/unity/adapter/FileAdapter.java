package it.piedpiper.unity.adapter;

import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import it.piedpiper.unity.R;
import it.piedpiper.unity.callback.CallBack;
import it.piedpiper.unity.databinding.CardFileBinding;
import it.piedpiper.unity.model.FilePdf;
import it.piedpiper.unity.repository.IStorageRepository;
import it.piedpiper.unity.repository.impl.StorageRepository;
import it.piedpiper.unity.ui.PDFActivity;
import it.piedpiper.unity.ui.main.MainActivity;
import it.piedpiper.unity.utils.Constant;

public class FileAdapter extends RecyclerView.Adapter<FileAdapter.ViewHolder>{
    private static final String TAG = "FileAdapter";
    private final List<FilePdf> filePdfList;
    private final MainActivity activity;
    private final IStorageRepository storageRepository;


    public FileAdapter(MainActivity activity, List<FilePdf> filePdfList) {
        this.filePdfList = filePdfList;
        this.activity = activity;
        this.storageRepository = new StorageRepository(activity);
    }

    @NonNull
    @Override
    public FileAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CardFileBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.card_file, parent, false);
        return new ViewHolder(binding, activity);
    }

    @Override
    public void onBindViewHolder(@NonNull FileAdapter.ViewHolder holder, int position) {
        if(filePdfList != null && filePdfList.size() != 0) {
            FilePdf filePdf = filePdfList.get(position);
            holder.bind(filePdf, storageRepository);
        }

        //deleteItem(position);
    }

    @Override
    public int getItemCount() {
        if(filePdfList != null)
            return filePdfList.size();
        return 0;
    }

    public void deleteItem(int position) {
        FilePdf file = filePdfList.remove(position);

        storageRepository.deleteFilePDFDao(file.getId(), new CallBack() {
            @Override
            public void onSuccess(Object object) {

            }

            @Override
            public void onError(Object object) {

            }
        });

        /*storageRepository.deleteFilePDF(file.getId(), file.getStorageUrl(), new CallBack() {
            @Override
            public void onSuccess(Object object) {
                Log.d(TAG, "onSuccess: file deleted!");
            }

            @Override
            public void onError(Object object) {

            }
        });*/

        notifyItemRemoved(position);
    }

    public FilePdf getFile(int position){
        return filePdfList.get(position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final CardFileBinding binding;
        private final MainActivity activity;
        private final CardView card;

        public ViewHolder(CardFileBinding binding, MainActivity activity) {
            super(binding.getRoot());
            this.binding = binding;
            this.card = binding.cardView;
            this.activity = activity;
        }

        public void bind(FilePdf filePdf, IStorageRepository storageRepository) {
            binding.setFilePdf(filePdf);

            card.setOnClickListener(c -> storageRepository.getFilePDFDao(filePdf.getId(), new CallBack() {
                @Override
                public void onSuccess(Object object) {

                    FilePdf file = (FilePdf) object;
                    Intent intent = new Intent(activity, PDFActivity.class);
                    intent.putExtra(Constant.URL_PARAMETER, file.getStorageUrl());

                    activity.startActivity(intent);
                }

                @Override
                public void onError(Object object) {
                    Log.d(TAG, "onSuccess: file not exists");

                    storageRepository.downloadPDF(filePdf, new CallBack() {
                        @Override
                        public void onSuccess(Object object) {
                            FilePdf file = (FilePdf) object;
                            Intent intent = new Intent(activity, PDFActivity.class);
                            intent.putExtra(Constant.URL_PARAMETER, file.getStorageUrl());

                            activity.runOnUiThread(() -> {
                                Handler handler = new Handler();
                                Runnable runnable = () -> activity.startActivity(intent);

                                handler.postDelayed(runnable, 1000);
                            });
                        }

                        @Override
                        public void onError(Object object) {

                        }
                    });
                }
            }));
        }

    }
}

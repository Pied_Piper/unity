package it.piedpiper.unity.adapter;

import android.app.Activity;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import java.util.ArrayList;

public class AdapterActions {

    public static void setAdapter(Activity activity, AutoCompleteTextView tv, ArrayList<String> array){
        ArrayAdapter<String> adapter = new ArrayAdapter<>(activity, android.R.layout.simple_list_item_1, array);
        tv.setThreshold(1);
        tv.setAdapter(adapter);
    }

    public static void setAdapter(Activity activity, AutoCompleteTextView tv, String[] array){
        ArrayAdapter<String> adapter = new ArrayAdapter<>(activity, android.R.layout.simple_list_item_1, array);
        tv.setThreshold(1);
        tv.setAdapter(adapter);
    }
}

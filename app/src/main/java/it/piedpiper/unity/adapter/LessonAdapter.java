package it.piedpiper.unity.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.getstream.chat.android.client.models.Channel;
import it.piedpiper.unity.R;
import it.piedpiper.unity.callback.CallBack;
import it.piedpiper.unity.databinding.CardLessonBinding;
import it.piedpiper.unity.model.Lesson;
import it.piedpiper.unity.model.User;
import it.piedpiper.unity.repository.IChannelRepository;
import it.piedpiper.unity.repository.IUserRepository;
import it.piedpiper.unity.repository.impl.ChannelRepository;
import it.piedpiper.unity.repository.impl.UserRepository;
import it.piedpiper.unity.ui.main.MainActivity;
import it.piedpiper.unity.ui.main.chat.ChatActivity;
import it.piedpiper.unity.utils.Animations;
import it.piedpiper.unity.utils.Constant;

public class LessonAdapter extends RecyclerView.Adapter<LessonAdapter.ViewHolder> {
    private static final String TAG = "LessonAdapter";
    private final List<Lesson> lessonList;
    private final MainActivity activity;
    private final IChannelRepository channelRepository;
    private final IUserRepository userRepository;


    public LessonAdapter(List<Lesson> lessonList, MainActivity activity) {
        this.lessonList = lessonList;
        this.activity = activity;
        this.userRepository = new UserRepository(activity);
        this.channelRepository = new ChannelRepository();
    }

    @NonNull
    @Override
    public LessonAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CardLessonBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.card_lesson, parent, false);
        return new ViewHolder(binding);
    }


    @Override
    public void onBindViewHolder(@NonNull LessonAdapter.ViewHolder holder, int position) {
        Lesson lesson = lessonList.get(position);

        userRepository.readUserByID(lesson.getUserID(), new CallBack() {
            @Override
            public void onSuccess(Object object) {
                holder.bind(lesson, (User) object);
            }

            @Override
            public void onError(Object object) {

            }
        });
    }

    @Override
    public int getItemCount() {

        if (lessonList != null){
            return lessonList.size();
        }
        return 0;

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final CardLessonBinding binding;
        private final CardView cardView;
        private final CardView headerView;
        private final CardView hiddenView;
        private final ImageView arrow;
        private final Button contactBtn;
        private boolean isExpanded;
        private String teacherID;

        public ViewHolder(CardLessonBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

            cardView = binding.cardView;
            headerView = binding.cardHeader;
            hiddenView = binding.hiddenView;

            arrow = binding.expandActivitiesButton;
            contactBtn = binding.contactBtn;

            isExpanded = false;

            expandListener();
        }

        public void bind(Lesson lesson, User user) {
            binding.setLesson(lesson);
            binding.setUser(user);

            io.getstream.chat.android.client.models.User teacher = new io.getstream.chat.android.client.models.User();
            teacher.setId(user.getId());
            teacher.setName(user.getUsername());

            if(user.getPhotoUrl().isEmpty())
                teacher.setImage("");
            else
                teacher.setImage(user.getPhotoUrl());

            binding.avatarView.setUserData(teacher);

            teacherID = lesson.getUserID();
            binding.executePendingBindings();

            onClickBtn(lesson);
        }

        private void expandListener(){
            cardView.setOnClickListener(c -> onClick());
        }

        private void onClick() {
            //TransitionManager.beginDelayedTransition(cardView);
            if (!isExpanded) {
                TransitionManager.beginDelayedTransition(cardView);
                TransitionManager.beginDelayedTransition(headerView);
                Animations.alphaView(hiddenView, 0, 1);
                hiddenView.setVisibility(View.VISIBLE);
                isExpanded = true;
                arrow.animate().rotation(180);
            } else {
                Animations.alphaView(hiddenView, 1, 0);
                hiddenView.setVisibility(View.GONE);
                isExpanded = false;
                arrow.animate().rotation(0);
            }
            notifyItemChanged(getBindingAdapterPosition());
        }

        public void onClickBtn(Lesson lesson){
            contactBtn.setOnClickListener(c -> {
                List<String> members = new ArrayList<>();
                SharedPreferences sharedPreferences = activity.getSharedPreferences(Constant.SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
                String userID = sharedPreferences.getString(Constant.USER_ID, null);

                members.add(userID);
                members.add(teacherID);

                Channel channel = new Channel();

                Map<String, Object> extraData = new HashMap<>();

                //extraData.put("name", lesson.getSubject());
                extraData.put("createdAt", Calendar.getInstance().getTime());
                //extraData.put("image", "");
                extraData.put("members", members);

                channel.setId(lesson.getId());
                channel.setType(Constant.LESSON.toLowerCase());
                channel.setExtraData(extraData);

                Log.d(TAG, "onClickBtn: creation lesson");

                channelRepository.createChannel(channel, members, new CallBack() {
                    @Override
                    public void onSuccess(Object object) {
                        activity.setCheck(true);
                        activity.onBackPressed();
                        activity.startActivity(ChatActivity.newIntent(activity, ((Channel) object)));
                    }

                    @Override
                    public void onError(Object object) {
                        Log.d(TAG, "onError: " + channel.getId() + "  type: " + channel.getType());
                    }
                });
            });
        }

    }
}

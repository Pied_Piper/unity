package it.piedpiper.unity.adapter;


import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;
import java.util.Objects;

import io.getstream.chat.android.client.ChatClient;
import it.piedpiper.unity.R;
import it.piedpiper.unity.callback.CallBack;
import it.piedpiper.unity.databinding.CardEventBinding;
import it.piedpiper.unity.model.Event;
import it.piedpiper.unity.repository.IEventRepository;
import it.piedpiper.unity.repository.impl.EventRepository;
import it.piedpiper.unity.ui.preferences.PreferencesActivity;
import it.piedpiper.unity.utils.dialog.ConfirmationDialogFragment;

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.ViewHolder>{


    private static final String TAG = "EventAdapter";
    private final List<Event> eventList;
    private final PreferencesActivity activity;

    private static final String GROUP = "group";
    private static final String ASSOCIATION = "association";
    private static final String LESSON = "lesson";

    private final IEventRepository eventRepository;


    public EventAdapter(List<Event> eventList, PreferencesActivity activity) {
        this.eventList = eventList;
        this.activity = activity;
        eventRepository = new EventRepository(activity);
    }


    @NonNull
    @Override
    public EventAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CardEventBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.card_event, parent, false);
        return new EventAdapter.ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if(eventList != null && eventList.size() != 0) {
            Event event = eventList.get(position);
            Log.d(TAG, "onBindViewHolder: " + event.getTitle());
            holder.bind(activity, event);
        }
    }

    @Override
    public int getItemCount() {
        if(eventList != null)
            return eventList.size();
        return 0;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private final CardEventBinding binding;

        public ViewHolder(CardEventBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(PreferencesActivity activity, Event event) {
            final int groupColor = R.color.purple_500;
            final int associationColor = com.google.android.libraries.places.R.color.quantum_googyellowA700;
            final int lessonColor = R.color.pastel_red_200;

            switch (event.getType()){
                case GROUP:
                    binding.color.setBackgroundColor(activity.getResources().getColor(groupColor, activity.getTheme()));
                    break;
                case ASSOCIATION:
                    binding.color.setBackgroundColor(activity.getResources().getColor(associationColor, activity.getTheme()));
                    break;
                case LESSON:
                    binding.color.setBackgroundColor(activity.getResources().getColor(lessonColor, activity.getTheme()));
                    break;

            }

            binding.setEvent(event);
            binding.executePendingBindings();

            binding.joinBtn.setOnClickListener(c -> {
                boolean check = false;

                //controllo per verificar ese sono gia dentro il gruppo (esce leave oppure join)
                for(String userId : event.getMembers()){
                    if(userId.equals(Objects.requireNonNull(ChatClient.instance().getCurrentUser()).getId())) {
                        check = true;  //se userId = mio Id
                        break;
                    }
                }

                ConfirmationDialogFragment dialog;

                if(check)
                    dialog = ConfirmationDialogFragment.newLeaveEvent(activity);

                else
                    dialog = ConfirmationDialogFragment.newJoinEvent(activity);

                dialog.confirmClickListener = () -> {
                    updateEventMembers(event.getId(), Objects.requireNonNull(ChatClient.instance().getCurrentUser()).getId());
                };

                dialog.show(activity.getSupportFragmentManager(), null);
            });
        }

        public void updateEventMembers(String id, String newMember) {
            eventRepository.getEventById(id, new CallBack() {
                @Override
                public void onSuccess(Object object) {
                    Event event = (Event) object;
                    boolean check = false;

                    for(int i = 0; i < event.getMembers().size(); i++){
                        if(event.getMembers().get(i).equals(newMember)) {
                            event.getMembers().remove(i);
                            i = event.getMembers().size();
                            check = true;
                        }
                    }

                    if(!check)
                        event.getMembers().add(newMember);

                    eventRepository.updateEvent(id, event.getMap(), new CallBack() {
                        @Override
                        public void onSuccess(Object object) {
                            Log.d(TAG, "onSuccess: Members list updated");
                        }

                        @Override
                        public void onError(Object object) {

                        }
                    });
                }

                @Override
                public void onError(Object object) {

                }
            });
        }
    }

}

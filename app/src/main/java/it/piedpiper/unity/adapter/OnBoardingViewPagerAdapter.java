package it.piedpiper.unity.adapter;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import it.piedpiper.unity.ui.on_boarding.OnBoarding_1;
import it.piedpiper.unity.ui.on_boarding.OnBoarding_2;
import it.piedpiper.unity.ui.on_boarding.OnBoarding_3;
import it.piedpiper.unity.ui.on_boarding.OnBoarding_4;


//non creo unico fragment e poi ci passo il tipo (questo in view pager adapter) ma qua in base a dove sono creo fragment
public class OnBoardingViewPagerAdapter extends FragmentStatePagerAdapter {

    private static final String TAG = "OnBoardingViewPagerAdapter";

    public OnBoardingViewPagerAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                Log.d(TAG, "getItem: 1");
                return new OnBoarding_1();
            case 1:
                Log.d(TAG, "getItem: 2");
                return new OnBoarding_2();
            case 2:
                Log.d(TAG, "getItem: 3");
                return new OnBoarding_3();
            case 3:
                Log.d(TAG, "getItem: 4");
                return new OnBoarding_4();
        }
        return new Fragment();
    }

    @Override
    public int getCount() {
        return 4;
    }
}

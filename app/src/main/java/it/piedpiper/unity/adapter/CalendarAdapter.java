package it.piedpiper.unity.adapter;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import it.piedpiper.unity.R;
import it.piedpiper.unity.databinding.CalendarCellBinding;
import it.piedpiper.unity.model.Event;

public class CalendarAdapter extends ArrayAdapter<Date> {

    private static final String TAG = "CalendarAdapter";
    private final LayoutInflater inflater;
    private CalendarCellBinding binding;
    private TextView cell;
    private TextView cellEvent;

    private final Calendar calendarSelected;

    private final List<Event> eventList;

    public CalendarAdapter(Activity activity, ArrayList<Date> days, Calendar calendarSelected, List<Event> eventList) {
        super(activity, R.layout.fragment_calendar, days);

        this.inflater = LayoutInflater.from(activity);
        this.calendarSelected = calendarSelected;

        this.eventList = eventList;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        Calendar calendar = Calendar.getInstance();
        Date date = getItem(position);
        calendar.setTime(date);
        int day = calendar.get(Calendar.DATE);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);

        // inflate item if it does not exist yet
        if (view == null){
            binding = CalendarCellBinding.inflate(inflater, parent, false);
            cell = binding.dateText;
            cellEvent = binding.eventsChecker;
        }

        // clear styling
        cell.setTypeface(null, Typeface.NORMAL);

        switch (parent.getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK) {
            case Configuration.UI_MODE_NIGHT_YES:
                cell.setTextColor(Color.WHITE);
                break;
            case Configuration.UI_MODE_NIGHT_NO:
                cell.setTextColor(Color.BLACK);
                break;
        }

        if (month != calendarSelected.get(Calendar.MONTH) ||
            year != calendarSelected.get(Calendar.YEAR) ||
            (day < Calendar.getInstance().get(Calendar.DATE)  && month == Calendar.getInstance().get(Calendar.MONTH))
        ) {
            // if this day is outside current month, grey it out
            cell.setTextColor(getContext().getResources().getColor(com.google.android.libraries.places.R.color.quantum_grey, getContext().getTheme()));
        }
        else if (day == calendarSelected.get(Calendar.DATE)) {
            // if it is today, set it to blue/bold
            cell.setTextColor(Color.WHITE);
            cell.setBackgroundTintList(ColorStateList.valueOf(getContext().getResources().getColor(R.color.pastel_blue_200, getContext().getTheme())));
            cellEvent.setBackgroundTintList(ColorStateList.valueOf(getContext().getResources().getColor(R.color.colorOnPrimary, getContext().getTheme())));
        }
        // set text
        cell.setText(String.valueOf(calendar.get(Calendar.DATE)));

        for(Event event : eventList){
            @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("dd");
            String eventDate = dateFormat.format(event.getDate().toDate());
            Log.d(TAG, "getView: " + eventDate + " " + (position-1));

            String p;
            if(position < 10)
                p = "0" + (position-1);
            else
                p = "" + (position-1);

            if(eventDate.equals(p))
                cellEvent.setVisibility(View.VISIBLE);
        }


        return binding.getRoot();
    }


}

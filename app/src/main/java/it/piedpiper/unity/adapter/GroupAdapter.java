package it.piedpiper.unity.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import io.getstream.chat.android.client.models.Channel;
import io.getstream.chat.android.client.models.User;
import it.piedpiper.unity.R;
import it.piedpiper.unity.callback.CallBack;
import it.piedpiper.unity.databinding.CardGroupBinding;
import it.piedpiper.unity.model.Group;
import it.piedpiper.unity.repository.IChannelRepository;
import it.piedpiper.unity.repository.IGroupRepository;
import it.piedpiper.unity.repository.impl.ChannelRepository;
import it.piedpiper.unity.repository.impl.GroupRepository;
import it.piedpiper.unity.ui.main.MainActivity;
import it.piedpiper.unity.ui.main.chat.ChatActivity;
import it.piedpiper.unity.utils.Constant;

public class GroupAdapter extends RecyclerView.Adapter<GroupAdapter.ViewHolder> {
    private final List<Group> groupList;
    private final MainActivity activity;
    private final IChannelRepository channelRepository;
    private final IGroupRepository groupRepository;

    public GroupAdapter(List<Group> groupList, MainActivity activity) {
        this.groupList = groupList;
        this.activity = activity;
        channelRepository = new ChannelRepository();
        groupRepository = new GroupRepository(activity);
    }

    @NonNull
    @Override
    public GroupAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CardGroupBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.card_group, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Group group = groupList.get(position);
        holder.bind(group);
    }


    @Override
    public int getItemCount() {
        return groupList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final CardGroupBinding binding;

        private Button chatBtn;

        private String id;

        private Group group;

        public ViewHolder(CardGroupBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(Group group) {
            binding.setGroup(group);
            binding.executePendingBindings();

            User g = new User();
            g.setName(group.getName());
            g.setImage(group.getPhotoUrl());

            binding.groupImage.setUserData(g);

            id = group.getId();

            this.group = group;

            initUI();
        }

        private void initUI(){
            chatBtn = binding.chatBtn;

            onClickButton();
        }

        public void onClickButton(){
            chatBtn.setOnClickListener(c -> {
                SharedPreferences sharedPreferences = activity.getSharedPreferences(Constant.SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
                String userID = sharedPreferences.getString(Constant.USER_ID, null);

                Channel channel = new Channel();
                channel.setCid(Constant.GROUP.toLowerCase() + ":" + id);
                channel.setType(Constant.GROUP.toLowerCase());

                group.getMembers().add(userID);

                groupRepository.updateMembers(id, group, new CallBack() {
                    @Override
                    public void onSuccess(Object object) {
                        channelRepository.joinChannel(channel, userID);
                        activity.setCheck(true);
                        activity.onBackPressed();
                        activity.startActivity(ChatActivity.newIntent(activity, channel));
                    }

                    @Override
                    public void onError(Object object) {

                    }
                });
            });
        }
    }
}

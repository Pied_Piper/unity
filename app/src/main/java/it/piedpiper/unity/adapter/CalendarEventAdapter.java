package it.piedpiper.unity.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.icu.text.SimpleDateFormat;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import it.piedpiper.unity.R;
import it.piedpiper.unity.databinding.CalendarEventCardBinding;
import it.piedpiper.unity.model.Event;
import it.piedpiper.unity.ui.preferences.PreferencesActivity;

public class CalendarEventAdapter extends RecyclerView.Adapter<CalendarEventAdapter.ViewHolder>{

    private final List<Event> eventList;
    private final PreferencesActivity activity;

    private static final String GROUP = "group";
    private static final String ASSOCIATION = "association";
    private static final String LESSON = "lesson";

    public CalendarEventAdapter(List<Event> eventList, PreferencesActivity activity) {
        this.eventList = eventList;
        this.activity = activity;
    }


    @NonNull
    public CalendarEventAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CalendarEventCardBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.calendar_event_card, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull CalendarEventAdapter.ViewHolder holder, int position) {
        if(eventList.size() != 0) {
            Event event = eventList.get(position);
            holder.bind(activity, event);
        }

    }

    @Override
    public int getItemCount() {
        return eventList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final CalendarEventCardBinding binding;

        public ViewHolder(CalendarEventCardBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(Activity activity, Event event) {
            binding.setEvent(event);

            @SuppressLint("SimpleDateFormat")
            android.icu.text.SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");

            final int groupColor = R.color.purple_500;
            final int associationColor = com.google.android.libraries.places.R.color.quantum_googyellowA700;
            final int lessonColor = R.color.pastel_red_200;

            switch (event.getType()){
                case GROUP:
                    binding.color.setBackgroundColor(activity.getResources().getColor(groupColor, activity.getTheme()));
                    break;
                case ASSOCIATION:
                    binding.color.setBackgroundColor(activity.getResources().getColor(associationColor, activity.getTheme()));
                    break;
                case LESSON:
                    binding.color.setBackgroundColor(activity.getResources().getColor(lessonColor, activity.getTheme()));
                    break;

            }
            binding.calendarEventTime.setText(sdf.format(event.getDate().toDate()));

            binding.executePendingBindings();
        }
    }
}


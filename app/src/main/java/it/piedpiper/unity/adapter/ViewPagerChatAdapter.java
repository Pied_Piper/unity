package it.piedpiper.unity.adapter;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import it.piedpiper.unity.ui.main.ChatListFragment;


//on boarding o per le chat dei gruppi e associazoni
public class ViewPagerChatAdapter extends FragmentStateAdapter {

    public ViewPagerChatAdapter(@NonNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }

    public ViewPagerChatAdapter(@NonNull Fragment fragment) {
        super(fragment);
    }

    public ViewPagerChatAdapter(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle) {
        super(fragmentManager, lifecycle);
    }


    @NonNull
    @Override
    public Fragment createFragment(int position) {
        // Return a NEW fragment instance in createFragment(int)

        Fragment fragment = new ChatListFragment();
        Bundle args = new Bundle();  //bundle di argomenti

        String type = "";
        switch (position){   //la posizione la prende in base al tab checkato
            case 0:
                type = "group";
                break;
            case 1:
                type = "association";
                break;
            case 2:
                type = "lesson";
                break;
        }
        args.putString("type", type);
        fragment.setArguments(args); //setto argomenti del fragment
        return fragment;
    }

    @Override
    public int getItemCount() {
        return 3;
    }
}

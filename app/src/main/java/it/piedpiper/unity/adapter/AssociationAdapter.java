package it.piedpiper.unity.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;


import io.getstream.chat.android.client.models.Channel;
import it.piedpiper.unity.R;
import it.piedpiper.unity.callback.CallBack;
import it.piedpiper.unity.databinding.CardAssociationBinding;
import it.piedpiper.unity.model.Association;
import it.piedpiper.unity.repository.IAssociationRepository;
import it.piedpiper.unity.repository.IChannelRepository;
import it.piedpiper.unity.repository.impl.AssociationRepository;
import it.piedpiper.unity.repository.impl.ChannelRepository;
import it.piedpiper.unity.ui.main.MainActivity;
import it.piedpiper.unity.ui.main.chat.ChatActivity;
import it.piedpiper.unity.utils.Animations;
import it.piedpiper.unity.utils.Constant;

public class AssociationAdapter extends RecyclerView.Adapter<AssociationAdapter.ViewHolder> {
    private final List<Association> associationList;
    private final MainActivity activity;

    private final IChannelRepository channelRepository;
    private final IAssociationRepository associationRepository;


    public AssociationAdapter(List<Association> associationList, MainActivity activity) {
        this.associationList = associationList;
        this.activity = activity;
        this.channelRepository = new ChannelRepository();
        this.associationRepository = new AssociationRepository(activity);
    }

    @NonNull
    @Override
    public AssociationAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CardAssociationBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.card_association, parent, false);
        return new ViewHolder(binding);
    }


    @Override
    public void onBindViewHolder(@NonNull AssociationAdapter.ViewHolder holder, int position) {
        Association association = associationList.get(position);

        holder.bind(association);
    }

    @Override
    public int getItemCount() {
        return associationList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final CardAssociationBinding binding;

        private Button chatBtn;

        private CardView cardView;
        private LinearLayout hiddenView;
        private boolean isExpanded;
        private ImageView arrow;

        private Association association;
        private String id;

        public ViewHolder(CardAssociationBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(Association association) {
            binding.setAssociation(association);
            binding.executePendingBindings();

            id = association.getId();
            this.association = association;



            //sdk usa avatar view (chat associarion xml0 a cui passo un utente --> creo utente come associazione, setto  nome  eutente e passo associazione
            // se immagine e stringa allora ê link altrimenti esce iniziale
            io.getstream.chat.android.client.models.User a = new io.getstream.chat.android.client.models.User();
            a.setName(association.getName());
            a.setImage(association.getPhotoUrl());

            binding.groupImage.setUserData(a);

            initUI();
        }

        private void initUI(){
            cardView = binding.cardView;
            chatBtn = binding.chatBtn;

            arrow = binding.expandActivitiesButton;
            hiddenView = binding.hiddenView;
            isExpanded = false;

            binding.setHolder(this);
            expandListener();
            //setAssociationImage(association);
            onClickButton();
        }

        //al click apro la card view
        public void expandListener(){
            cardView.setOnClickListener(this::onClick);
        }

        private void onClickButton(){
            chatBtn.setOnClickListener(c -> {
                SharedPreferences sharedPreferences = activity.getSharedPreferences(Constant.SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
                String userID = sharedPreferences.getString(Constant.USER_ID, null);

                Channel channel = new Channel();
                channel.setCid(Constant.ASSOCIATION.toLowerCase() + ":" + id);
                channel.setType(Constant.ASSOCIATION.toLowerCase());

                association.getMembers().add(userID);

                associationRepository.updateMembers(id, association, new CallBack() {
                    @Override
                    public void onSuccess(Object object) {
                        channelRepository.joinChannel(channel, userID);
                        activity.setCheck(true);
                        activity.onBackPressed();
                        activity.startActivity(ChatActivity.newIntent(activity, channel));
                    }

                    @Override
                    public void onError(Object object) {

                    }
                });
            });
        }

        private void onClick(View c) {
            TransitionManager.beginDelayedTransition(cardView);
            if (!isExpanded) {
                Animations.alphaView(hiddenView, 0, 1);
                hiddenView.setVisibility(View.VISIBLE);
                isExpanded = true;
                arrow.animate().rotation(180);
                notifyItemChanged(getBindingAdapterPosition()-1); //notifico le altre card
            } else {
                Animations.alphaView(hiddenView, 1, 0);
                hiddenView.setVisibility(View.GONE); //la rendo invisibile
                isExpanded = false;
                arrow.animate().rotation(0);
                notifyItemChanged(getBindingAdapterPosition());
            }
        }
    }
}


package it.piedpiper.unity.api.service;

import java.util.List;

import it.piedpiper.unity.model.University;
import it.piedpiper.unity.utils.Constant;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

//interfaccia creata con metodo in service locatore
public interface UniversitiesApiService {

    //passo nome e nazione a query di tipo get
    @GET(Constant.SEARCH_ENDPOINT)  //corrisponde alla search
    Call<List<University>> getUniversities(
            @Query(Constant.SEARCH_NAME_PARAMETER) String name,
            @Query(Constant.SEARCH_COUNTRY_PARAMETER) String country
    );

    @GET(Constant.SEARCH_ENDPOINT)
    Call<List<University>> getUniversities(
            @Query(Constant.SEARCH_NAME_PARAMETER) String name
    );

    @GET(Constant.SEARCH_ENDPOINT)
    Call<List<University>> getUniversitiesCountry(
            @Query(Constant.SEARCH_COUNTRY_PARAMETER) String country
    );

    @GET(Constant.SEARCH_ENDPOINT)
    Call<List<University>> getAllUniversities();
}

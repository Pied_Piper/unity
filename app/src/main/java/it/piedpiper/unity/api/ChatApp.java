package it.piedpiper.unity.api;

import android.app.Activity;

import java.util.ArrayList;

import io.getstream.chat.android.client.ChatClient;
import io.getstream.chat.android.client.logger.ChatLogLevel;
import io.getstream.chat.android.client.models.Channel;
import io.getstream.chat.android.client.models.User;
import io.getstream.chat.android.client.notifications.handler.NotificationConfig;
import io.getstream.chat.android.client.notifications.handler.NotificationHandler;
import io.getstream.chat.android.client.notifications.handler.NotificationHandlerFactory;
import io.getstream.chat.android.livedata.ChatDomain;
import io.getstream.chat.android.pushprovider.firebase.FirebasePushDeviceGenerator;
import it.piedpiper.unity.ui.main.chat.ChatActivity;
import it.piedpiper.unity.utils.Constant;

public class ChatApp {

    //per mandare notifiche
    public static void initClient(Activity activity){
        NotificationConfig notificationConfig = new NotificationConfig(true, new ArrayList<FirebasePushDeviceGenerator>(), () -> true);
        NotificationHandler notificationHandler = NotificationHandlerFactory.INSTANCE.createNotificationHandler(
                activity,
                (message, channelType, channelId) -> {
                    Channel channel = new Channel();
                    channel.setId(channelId);
                    channel.setType(channelType);
                    return ChatActivity.newIntent(activity, channel);
                });


        ChatClient client = new ChatClient.Builder(Constant.CHAT_API_KEY, activity)
                .logLevel(ChatLogLevel.ALL)
                .notifications(notificationConfig, notificationHandler)
                .build();

        new ChatDomain.Builder(activity, client)
                .offlineEnabled()
                .build();
    }


    public static void connectUser(User user){
        ChatClient client = ChatClient.instance();
        String token = client.devToken(user.getId());   //stesso toker per tutti (dev token)

        client.connectUser(
                user,
                token
        ).enqueue();
    }

    public static void disconnectUser(){
        ChatClient.instance().disconnect();
    }
}

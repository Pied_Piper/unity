package it.piedpiper.unity.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import it.piedpiper.unity.api.service.LogoApiService;
import it.piedpiper.unity.api.service.UniversitiesApiService;
import it.piedpiper.unity.utils.Constant;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceLocator {

    private static ServiceLocator instance = null;

    private ServiceLocator() {}

    public static ServiceLocator getInstance() {
        if (instance == null) {
            synchronized(ServiceLocator.class) {
                instance = new ServiceLocator();
            }
        }
        return instance;
    }

    public UniversitiesApiService getUniversitiesApiService() {
        Retrofit retrofit = new Retrofit.Builder()  //creo servizo con api
                .baseUrl(Constant.UNIVERSITIES_API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).build(); //gson builder syandard
        return retrofit.create(UniversitiesApiService.class);
    }

    public LogoApiService getLogoApiService() {
        Gson gson = new GsonBuilder()  //uso gson builder non standard
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.LOGO_API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson)).build();
        return retrofit.create(LogoApiService.class);
    }
}

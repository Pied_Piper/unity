package it.piedpiper.unity.api.service;

import it.piedpiper.unity.model.LogoResponse;
import it.piedpiper.unity.utils.Constant;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface LogoApiService {

    //query esegue post e non get
    @Headers({"Accept: application/json"})
    @POST(Constant.CATEGORIZE_ENDPOINT)
    Call<LogoResponse> getLogo(
            @Query(Constant.URL_PARAMETER) String url,
            @Header("Authorization") String apiKey);

}

package it.piedpiper.unity.ui.on_boarding;

import it.piedpiper.unity.R;
import it.piedpiper.unity.databinding.ActivityOnBoardingBinding;
import it.piedpiper.unity.viewmodel.main.MainActivityViewModel;
import it.piedpiper.unity.viewmodel.on_boarding.OnBoardingViewModel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;


public class OnBoardingActivity extends AppCompatActivity {
    private OnBoardingViewModel viewModel;
    private ActivityOnBoardingBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_on_boarding);

        viewModel = new OnBoardingViewModel(this, binding);
        viewModel.initUI();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        binding = null;
        viewModel = null;
    }
}
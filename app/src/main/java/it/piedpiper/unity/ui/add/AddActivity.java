package it.piedpiper.unity.ui.add;

import static it.piedpiper.unity.utils.Constant.PERMISSION_REQUEST_CODE;
import static it.piedpiper.unity.utils.Constant.PERMISSION_REQUEST_CODE_ALL;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NavUtils;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import io.getstream.chat.android.client.models.Channel;
import it.piedpiper.unity.R;
import it.piedpiper.unity.databinding.ActivityAddBinding;
import it.piedpiper.unity.ui.main.MainActivity;
import it.piedpiper.unity.ui.main.chat.ChatActivity;
import it.piedpiper.unity.utils.Constant;
import it.piedpiper.unity.viewmodel.add.AddActivityViewModel;


public class AddActivity extends AppCompatActivity {
    private static final String TAG = "AddActivity";
    private ActivityAddBinding binding;
    private AddActivityViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_add);

        viewModel = new AddActivityViewModel(this, binding);
        viewModel.initUI();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        binding = null;
        viewModel = null;
    }

    @Override
    public void onBackPressed() {
        finish();
        //super.onBackPressed();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        Log.d(TAG, "onRequestPermissionsResult: " + requestCode);
        if (requestCode == PERMISSION_REQUEST_CODE) {
            Log.d(TAG, "onRequestPermissionsResult: permission");
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.d(TAG, "onRequestPermissionsResult: permission granted");
                pickPDF();
            }

            else {
                Toast.makeText(this, "Permission Denied... \nYou Should Allow External Storage Permission To Upload files.", Toast.LENGTH_LONG).show();
                finish();
            }
        }
        else{
            Log.d(TAG, "onRequestPermissionsResult: permission");
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.d(TAG, "onRequestPermissionsResult: permission granted");
            }

            else {
                Toast.makeText(this, "Permission Denied... \nYou Should Allow External Storage Permission To Upload files.", Toast.LENGTH_LONG).show();
            }
        }
    }

    public boolean checkPermission(){
        int write = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int read = ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE);
        int camera = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA);
        return read == PackageManager.PERMISSION_GRANTED &&
                write == PackageManager.PERMISSION_GRANTED &&
                camera == PackageManager.PERMISSION_GRANTED;
    }

    public void requestPermission(){
        this.requestPermissions(new String[]{
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE,
                        android.Manifest.permission.CAMERA},
                PERMISSION_REQUEST_CODE_ALL);
    }

    public Uri getUri(){
        return viewModel.getUri();
    }

    public void pickPDF(){
        viewModel.pickPDF();
    }

}
package it.piedpiper.unity.ui.main.chat;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.r0adkll.slidr.Slidr;
import com.r0adkll.slidr.model.SlidrInterface;
import com.vanniktech.emoji.EmojiManager;
import com.vanniktech.emoji.ios.IosEmojiProvider;

import io.getstream.chat.android.client.models.Channel;
import it.piedpiper.unity.R;
import it.piedpiper.unity.databinding.ActivityChatBinding;
import it.piedpiper.unity.databinding.ActivityLaunchScreenBinding;
import it.piedpiper.unity.viewmodel.main.chat.ChatViewModel;

public class ChatActivity extends AppCompatActivity {

    private ActivityChatBinding binding;
    private ChatViewModel viewModel;

    public final static String CID_KEY = "key:cid";

    public static Intent newIntent(Context context, Channel channel) {
        final Intent intent = new Intent(context, ChatActivity.class);
        intent.putExtra(CID_KEY, channel.getCid());
        intent.putExtra("type", channel.getType());
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SlidrInterface slidr = Slidr.attach(this);

        binding = ActivityChatBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        viewModel = new ChatViewModel(this, binding);
        viewModel.initUI();
        viewModel.checkIsTyping();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        binding = null;
        viewModel = null;
    }
}
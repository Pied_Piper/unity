package it.piedpiper.unity.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import io.getstream.chat.android.client.ChatClient;
import io.getstream.chat.android.client.models.User;
import it.piedpiper.unity.api.ChatApp;
import it.piedpiper.unity.callback.CallBack;
import it.piedpiper.unity.databinding.ActivityLaunchScreenBinding;
import it.piedpiper.unity.repository.IUserRepository;
import it.piedpiper.unity.repository.impl.UserRepository;
import it.piedpiper.unity.ui.login.LoginActivity;
import it.piedpiper.unity.ui.main.MainActivity;
import it.piedpiper.unity.ui.on_boarding.OnBoardingActivity;
import it.piedpiper.unity.utils.Constant;

/**
 * Launch (Splash) Screen Activity to manage the startup of
 * the application and choose which Activity to start.
 */
@SuppressLint("CustomSplashScreen")
public class LaunchScreenActivity extends AppCompatActivity {

    private static final String TAG = "LaunchScreenActivity";

    private ActivityLaunchScreenBinding binding;
    private FirebaseUser mUser;
    private IUserRepository userRepository;

    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityLaunchScreenBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        ChatApp.initClient(this);
        /*
         * Handle the splash screen transition with SplashScreen API
         * (see https://developer.android.com/guide/topics/ui/splash-screen)
         * SplashScreen splashScreen = SplashScreen.installSplashScreen(this);
         */

        ChatClient client = ChatClient.instance();

        initDB();

        if (mUser != null) {
            SharedPreferences sharedPreferences = this.getSharedPreferences(Constant.SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);

            boolean userSetted = sharedPreferences.getBoolean(Constant.SHARED_PREFERENCES_USER_SETTED, false);

            if (mUser.isEmailVerified()) {
                if(!userSetted)
                    launchIntent("User");
                else
                    userRepository.readUserDao(new CallBack() {
                        @Override
                        public void onSuccess(Object object) {
                            it.piedpiper.unity.model.User tmp = (it.piedpiper.unity.model.User) object;

                            User user = new User();
                            user.setId(tmp.getId());
                            user.setName(tmp.getUsername());

                            if (!tmp.getPhotoUrl().isEmpty())
                                user.setImage(tmp.getPhotoUrl());
                            else
                                user.setImage("");

                            client.connectUser(user, client.devToken(user.getId())).enqueue();

                            intent = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(intent);
                            finish();
                        }

                    @Override
                    public void onError(Object object) {

                    }
                });
            }
            else {
                launchIntent(Constant.VERIFY_STRING);
            }
        } else {
            launchIntent(Constant.ONBOARDING_STRING);
        }
    }

    private void initDB() {
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();

        userRepository = new UserRepository(this);
    }

    private void launchIntent(String fragment) {
        if(fragment.equals(Constant.ONBOARDING_STRING))
            intent = new Intent(this, OnBoardingActivity.class);
        else
            intent = new Intent(this, LoginActivity.class);


        intent.putExtra(Constant.FRAGMENT_STRING, fragment);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        binding = null;
    }
}
package it.piedpiper.unity.ui.add;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import it.piedpiper.unity.R;
import it.piedpiper.unity.databinding.FragmentCreateLessonBinding;
import it.piedpiper.unity.databinding.FragmentLessonsBinding;
import it.piedpiper.unity.viewmodel.add.CreateLessonViewModel;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CreateLessonFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CreateLessonFragment extends Fragment {

    private FragmentCreateLessonBinding binding;
    private CreateLessonViewModel viewModel;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public CreateLessonFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CreateLessonFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CreateLessonFragment newInstance(String param1, String param2) {
        CreateLessonFragment fragment = new CreateLessonFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_create_lesson, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new CreateLessonViewModel((AddActivity) getActivity(), binding);
        viewModel.initUI();
        viewModel.setOnClickListeners();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        binding = null;
        viewModel = null;
    }
}
package it.piedpiper.unity.ui.main;

import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import static it.piedpiper.unity.utils.Constant.PERMISSION_REQUEST_CODE;
import static it.piedpiper.unity.utils.LocaleHelper.setLocale;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.preference.ListPreference;

import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;

import java.util.Arrays;
import java.util.Objects;

import io.getstream.chat.android.client.ChatClient;
import it.piedpiper.unity.R;
import it.piedpiper.unity.databinding.ActivityMainBinding;
import it.piedpiper.unity.databinding.HeaderNavigationDrawerBinding;
import it.piedpiper.unity.model.User;
import it.piedpiper.unity.utils.Constant;
import it.piedpiper.unity.utils.LocaleHelper;
import it.piedpiper.unity.utils.NetworkConnection;
import it.piedpiper.unity.viewmodel.main.MainActivityViewModel;
import it.piedpiper.unity.viewmodel.main.SocialViewModel;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    private MainActivityViewModel viewModel;
    private ActivityMainBinding binding;

    private boolean check = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        viewModel = new MainActivityViewModel(this, binding);

        viewModel.setBottomBar();
        viewModel.setFabs();

        viewModel.setActionBar();
        viewModel.setFabsOnClickListeners();

        viewModel.checkDestination();

        //viewModel.checkFragment(getIntent().getStringExtra(Constant.FRAGMENT_STRING));
    }

    public void changeLanguage(String lan, ListPreference list){
        Context context = LocaleHelper.setLocale(this, lan);
        Resources resources = context.getResources();
    }

    @Override
    protected void onResume() {
        super.onResume();

        viewModel.reloadChannelListHeaderView();  //quando ritorno nella main atticvity eseguo reload delle informazioni dell'utente
        viewModel.setDrawerItemNotChecked();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        binding = null;
        viewModel = null;
    }


    @Override
    //richiesta deipermessi --> se approvato oppure denied
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        Log.d(TAG, "onRequestPermissionsResult: " + requestCode);
        if (requestCode == PERMISSION_REQUEST_CODE) {
            Log.d(TAG, "onRequestPermissionsResult: permission");
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.d(TAG, "onRequestPermissionsResult: permission granted");
            }
            else {
                Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show();
                onBackPressed();
            }
        }
    }

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }
}
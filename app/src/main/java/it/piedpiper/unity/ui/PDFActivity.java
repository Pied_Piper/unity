package it.piedpiper.unity.ui;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.r0adkll.slidr.Slidr;
import com.r0adkll.slidr.model.SlidrInterface;

import java.io.File;

import it.piedpiper.unity.R;
import it.piedpiper.unity.databinding.ActivityPdfBinding;
import it.piedpiper.unity.utils.Constant;

public class PDFActivity extends AppCompatActivity {

    private static final String TAG = "PDFActivity";
    private ActivityPdfBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_pdf);

        SlidrInterface slidr = Slidr.attach(this);

        init();
    }

    private void init(){
        //Log.d(TAG, "init: " + Uri.parse(getIntent().getStringExtra(Constant.URL_PARAMETER)));
        File file = new File(getIntent().getStringExtra(Constant.URL_PARAMETER));
        binding.pdfView
                .fromFile(file)
                .enableSwipe(true)
                .enableAnnotationRendering(true)
                .enableDoubletap(true)
                .swipeHorizontal(true)
                .defaultPage(1)
                .onLoad(nbPages -> {

                })
                .onPageChange((page, pageCount) -> {

                })
                .enableAnnotationRendering(false)
                .password(null)
                .load();
    }
}
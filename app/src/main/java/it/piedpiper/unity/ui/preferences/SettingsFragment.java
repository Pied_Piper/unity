
package it.piedpiper.unity.ui.preferences;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.SwitchPreferenceCompat;

import it.piedpiper.unity.R;
import it.piedpiper.unity.utils.Constant;

public class SettingsFragment extends PreferenceFragmentCompat{

    private static final String TAG = "SettingsFragment";

    @SuppressLint("RestrictedApi")
    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {

        addPreferencesFromResource(R.xml.settings_preferences);


        Preference mPreferenceAccount = findPreference(Constant.PROFILE_STRING);
        Preference mPreferenceEmail = findPreference(Constant.EMAIL_STRING);
        Preference mPreferencePassword = findPreference(Constant.PASSWORD_STRING);
        SwitchPreferenceCompat mSwitchDark = findPreference("Darkmode");

        if ((getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK) == Configuration.UI_MODE_NIGHT_YES) {
            assert mSwitchDark != null;
            mSwitchDark.performClick();
        }

        SwitchPreferenceCompat mSwitchNotifications = findPreference("Notification");
        ListPreference mListLanguage = findPreference("Languages");

        assert mPreferenceAccount != null;
        mPreferenceAccount.setOnPreferenceClickListener(preference -> {
            Log.d(TAG, "onPreferenceClick: Account");
            Intent intent = new Intent(getActivity(), PreferencesActivity.class);
            intent.putExtra(Constant.FRAGMENT_STRING, Constant.PROFILE_STRING);
            requireActivity().startActivity(intent);
            return true;
        });

        assert mPreferenceEmail != null;
        mPreferenceEmail.setOnPreferenceClickListener(preference -> {
            Intent intent = new Intent(getActivity(), PreferencesActivity.class);
            intent.putExtra(Constant.FRAGMENT_STRING, Constant.EMAIL_STRING);
            requireActivity().startActivity(intent);
            return true;
        });

        assert mPreferencePassword != null;
        mPreferencePassword.setOnPreferenceClickListener(preference -> {
            Intent intent = new Intent(getActivity(), PreferencesActivity.class);
            intent.putExtra(Constant.FRAGMENT_STRING, Constant.PASSWORD_STRING);
            requireActivity().startActivity(intent);
            return true;
        });

        assert mSwitchDark != null;
        mSwitchDark.setOnPreferenceChangeListener((preference, newValue) -> {
            boolean mSwitchDarkChecked = newValue.toString().equalsIgnoreCase("true");

            if (mSwitchDarkChecked)
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            else
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

            return true;
        });

        assert mSwitchNotifications != null;
        mSwitchNotifications.setOnPreferenceChangeListener((preference, newValue) -> {
            Log.d(TAG, "onPreferenceChange: Notifications");
            //to implement when notifications are ready
            return true;
        });


        assert mListLanguage != null;
        mListLanguage.setOnPreferenceChangeListener((preference, newValue) -> {
            Log.d(TAG, "onPreferenceChange: " + newValue.toString());
            switch (newValue.toString()){
                case "uk":
                    Log.d(TAG, "onPreferenceChange: Language english ");
                    //activity.changeLanguage("en", mListLanguage);
                    break;
                case "fr":
                    Log.d(TAG, "onPreferenceChange: Language french ");
                    //activity.changeLanguage("fr", mListLanguage);
                    break;
                case "sp":
                    Log.d(TAG, "onPreferenceChange: Language spanish ");
                   // activity.changeLanguage("es", mListLanguage);
                    break;
                case "it":
                    Log.d(TAG, "onPreferenceChange: Language italian ");
                   // activity.changeLanguage("it", mListLanguage);
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + newValue);
            }
            return true;
        });
    }

}
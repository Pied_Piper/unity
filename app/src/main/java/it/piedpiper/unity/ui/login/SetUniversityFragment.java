package it.piedpiper.unity.ui.login;

import android.os.Bundle;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import it.piedpiper.unity.R;

import it.piedpiper.unity.databinding.FragmentSetUniversityBinding;
import it.piedpiper.unity.viewmodel.login.SetUniversityViewModel;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SetUniversityFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SetUniversityFragment extends Fragment {
    private FragmentSetUniversityBinding binding;
    private SetUniversityViewModel viewModel;


    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String USERNAME = "Username";
    private static final String IMAGE = "Image";

    // TODO: Rename and change types of parameters
    private String username;
    private byte[] data;

    public SetUniversityFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param username Parameter 1.
     * @param data Parameter 2.
     * @return A new instance of fragment setUniversityFragment.

     */

    public static SetUniversityFragment newInstance(String username, byte[] data) {
        SetUniversityFragment fragment = new SetUniversityFragment();
        Bundle args = new Bundle();
        args.putString(USERNAME, username);
        args.putByteArray(IMAGE, data);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            username = getArguments().getString(USERNAME);

            if(getArguments().getByteArray(IMAGE) != null)
                data = getArguments().getByteArray(IMAGE);
            else
                data = null;
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_set_university, container, false);
        return binding.getRoot();
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new SetUniversityViewModel((LoginActivity) getActivity(), binding, username, data);

        //Init UI
        viewModel.initUI();

        //Setting actions
        viewModel.setUniversityActions();
        viewModel.onClickFinish();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        binding = null;
        viewModel = null;
    }
}
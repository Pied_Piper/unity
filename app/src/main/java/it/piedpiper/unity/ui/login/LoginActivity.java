package it.piedpiper.unity.ui.login;

import static it.piedpiper.unity.utils.Constant.PERMISSION_REQUEST_CODE;

import it.piedpiper.unity.R;
import it.piedpiper.unity.databinding.ActivityLoginBinding;
import it.piedpiper.unity.databinding.ActivityMainBinding;
import it.piedpiper.unity.viewmodel.login.LoginActivityViewModel;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;
import android.widget.Toolbar;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Objects;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";
    private LoginActivityViewModel viewModel;

    private ActivityLoginBinding binding;
    private NavHostFragment navHostFragment;
    private NavController navController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);

        viewModel = new LoginActivityViewModel(this, binding);
        viewModel.initUI();
    }

    public void replaceFragment(String fragmentName){
        viewModel.replaceFragment(fragmentName);
    }

    public void replaceFragment(String fragmentName, Bundle args){
        viewModel.replaceFragment(fragmentName, args);
    }

    /**
     * It shows a warning message to the user with a Snackbar.
     * @param message The warning message to be shown in the Snackbar.
     */
    public void updateUIForFailure(String message) {
        Snackbar.make(this.findViewById(android.R.id.content),
                message, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        binding = null;
        viewModel = null;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        Log.d(TAG, "onRequestPermissionsResult: " + requestCode);
        if (requestCode == PERMISSION_REQUEST_CODE) {
            Log.d(TAG, "onRequestPermissionsResult: permission");
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.d(TAG, "onRequestPermissionsResult: permission granted");
            }
            else {
                Toast.makeText(this, "Permission Denied... \nYou Should Allow External Storage Permission To Upload images.", Toast.LENGTH_LONG).show();
            }
        }
    }

    public boolean checkPermission() {
        int write = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int read = ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE);
        int camera = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA);
        return read == PackageManager.PERMISSION_GRANTED &&
               write == PackageManager.PERMISSION_GRANTED &&
               camera == PackageManager.PERMISSION_GRANTED;
    }


    public void requestPermission() {
        this.requestPermissions(new String[]{
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                android.Manifest.permission.READ_EXTERNAL_STORAGE,
                android.Manifest.permission.CAMERA},
                PERMISSION_REQUEST_CODE);
    }
}
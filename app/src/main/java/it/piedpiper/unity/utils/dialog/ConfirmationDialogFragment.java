package it.piedpiper.unity.utils.dialog;

import android.app.FragmentManager;
import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.ColorRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;


import it.piedpiper.unity.R;
import it.piedpiper.unity.databinding.ConfirmationDialogFragmentBinding;

public class ConfirmationDialogFragment extends BottomSheetDialogFragment {
    public ConfirmClickListener confirmClickListener = null;

    private ConfirmationDialogFragmentBinding binding;

    String TAG = "ConfirmationDialogFragment";
    private static final String ARG_ICON_RES_ID = "icon_res_id";
    private static final String ARG_ICON_TINT_RES_ID = "icon_tint_res_id";
    private static final String ARG_TITLE = "title";
    private static final String ARG_DESCRIPTION = "description";
    private static final String ARG_CONFIRM_TEXT = "confirm_text";
    private static final String ARG_CANCEL_TEXT = "cancel_text";
    private static final String ARG_HAS_CONFIRM_BUTTON = "has_confirm_button";

    private static int iconResId;
    private static int iconTintResId;
    private static String title;
    private static String description;
    private static String cancelText;
    private static String confirmText;
    private static boolean hasConfirmButton;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = ConfirmationDialogFragmentBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public int getTheme() {
        return R.style.StreamUiBottomSheetDialogTheme;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.iconImageView.setImageResource(iconResId);
        binding.iconImageView.setColorFilter(view.getContext().getColor(iconTintResId));

        binding.titleTextView.setText(title);
        binding.descriptionTextView.setText(description);

        binding.cancelButton.setText(cancelText);
        binding.cancelButton.setOnClickListener(c -> dismiss());

        if(hasConfirmButton){
            if(iconResId == R.drawable.ic_check)
                binding.confirmButton.setBackgroundTintList(ColorStateList.valueOf(requireActivity().getResources().getColor(R.color.pastel_green_200, requireActivity().getTheme())));

            binding.confirmButton.setVisibility(View.VISIBLE);
            binding.confirmButton.setText(confirmText);
            binding.confirmButton.setOnClickListener(c -> {
                confirmClickListener.onClick();
                dismiss();
            });
        }
        else
            binding.confirmButton.setVisibility(View.GONE);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        binding = null;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        confirmClickListener = null;
    }

    public interface ConfirmClickListener {
        void onClick();
    }

    public static ConfirmationDialogFragment newInstance(
            @DrawableRes int iconResId,
            @ColorRes int iconTintResId,
            String title,
            String description,
            String confirmText,
            String cancelText,
            boolean hasConfirmButton
            ) {
        ConfirmationDialogFragment confirmationDialogFragment = new ConfirmationDialogFragment();

        Bundle arguments = new Bundle();
        arguments.putInt(ARG_ICON_RES_ID, iconResId);
        arguments.putInt(ARG_ICON_TINT_RES_ID, iconTintResId);
        arguments.putString(ARG_TITLE, title);
        arguments.putString(ARG_DESCRIPTION, description);
        arguments.putString(ARG_CONFIRM_TEXT, confirmText);
        arguments.putString(ARG_CANCEL_TEXT, cancelText);
        arguments.putBoolean(ARG_HAS_CONFIRM_BUTTON, hasConfirmButton);

        confirmationDialogFragment.setArguments(arguments);

        return confirmationDialogFragment;
    }

    public static ConfirmationDialogFragment newDeleteMessageInstance(Context context) {
        return newInstance(
            iconResId = R.drawable.ic_delete,
            iconTintResId = R.color.pastel_red_700,
            title = context.getString(R.string.stream_ui_message_list_delete_confirmation_title),
            description = context.getString(R.string.stream_ui_message_list_delete_confirmation_message),
            confirmText = context.getString(R.string.stream_ui_message_list_delete_confirmation_positive_button),
            cancelText = context.getString(R.string.stream_ui_message_list_delete_confirmation_negative_button),
            hasConfirmButton = true
        );
    }

    public static ConfirmationDialogFragment newDeleteChannelInstance(Context context) {
        return newInstance(
            iconResId = R.drawable.ic_delete,
            iconTintResId = R.color.pastel_red_700,
            title = context.getString(R.string.chat_info_option_delete_conversation),
            description = context.getString(R.string.chat_info_delete_conversation_confirm),
            confirmText = context.getString(R.string.delete),
            cancelText = context.getString(R.string.cancel),
            hasConfirmButton = true
        );
    }

    public static ConfirmationDialogFragment newLeaveChannelInstance(Context context, String channelType, String channelName) {
        return newInstance(
            iconResId = R.drawable.ic_leave_group,
            iconTintResId = io.getstream.chat.android.ui.R.color.stream_ui_grey,
            title = context.getString(R.string.chat_group_info_option_leave),
            description = context.getString(R.string.chat_group_info_leave_confirm, channelType, channelName),
            confirmText = context.getString(R.string.leave),
            cancelText = context.getString(R.string.cancel),
            hasConfirmButton = true
        );
    }

    public static ConfirmationDialogFragment newFlagMessageInstance(Context context) {
        return newInstance(
            iconResId = io.getstream.chat.android.ui.R.drawable.stream_ui_ic_flag,
            iconTintResId = R.color.pastel_red_700,
            title = context.getString(io.getstream.chat.android.ui.R.string.stream_ui_message_list_flag_confirmation_title),
            description = context.getString(io.getstream.chat.android.ui.R.string.stream_ui_message_list_flag_confirmation_message),
            confirmText = context.getString(io.getstream.chat.android.ui.R.string.stream_ui_message_list_flag_confirmation_positive_button),
            cancelText = context.getString(R.string.cancel),
            hasConfirmButton = true
        );
    }

    public static ConfirmationDialogFragment newMessageFlaggedInstance(Context context) {
        return newInstance(
            iconResId = io.getstream.chat.android.ui.R.drawable.stream_ui_ic_flag,
            iconTintResId = R.color.pastel_red_700,
            title = context.getString(R.string.message_flagged_title),
            description = context.getString(R.string.message_flagged_description),
            confirmText = context.getString(R.string.ok),
            cancelText = context.getString(R.string.ok),
            hasConfirmButton = false
        );
    }

    public static ConfirmationDialogFragment newJoinEvent(Context context) {
        return newInstance(
                iconResId = R.drawable.ic_check,
                iconTintResId = R.color.pastel_green_200,
                title = context.getString(R.string.join_event),
                description = context.getString(R.string.join_question),
                confirmText = context.getString(R.string.join),
                cancelText = context.getString(R.string.cancel),
                hasConfirmButton = true
        );
    }

    public static ConfirmationDialogFragment newLeaveEvent(Context context) {
        return newInstance(
                iconResId = R.drawable.ic_round_logout_24,
                iconTintResId = R.color.pastel_red_700,
                title = context.getString(R.string.leave_event),
                description = context.getString(R.string.leave_question),
                confirmText = context.getString(R.string.leave),
                cancelText = context.getString(R.string.cancel),
                hasConfirmButton = true
        );
    }
}

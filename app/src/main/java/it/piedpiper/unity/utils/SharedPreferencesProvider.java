package it.piedpiper.unity.utils;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

//voglio settar einformazioni particolari dentro il telefono a cui posso acceder
//verifico se utente settato correttamente
//se setatto --> main activity
//se non settato --> setUser
public class SharedPreferencesProvider {

    private final SharedPreferences sharedPref;

    public SharedPreferencesProvider(Application application) {
        sharedPref = application.getSharedPreferences(Constant.SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
    }

    /**
     * It gets the last time in which news was downloaded from the Web Service.
     * @return the last time in which news was downloaded from the Web Service.
     */
    public long getLastUpdate() {
        return sharedPref.getLong(Constant.LAST_UPDATE, 0);
    }

    /**
     * It saves the last time in which news was downloaded from the Web Service.
     * @param lastUpdate last time in which news was downloaded from the Web Service.
     */
    public void setLastUpdate(long lastUpdate) {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putLong(Constant.LAST_UPDATE, lastUpdate);
        editor.apply();
    }

    public void setAuthenticationToken(String token) {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(Constant.AUTHENTICATION_TOKEN, token);
        editor.apply();
    }

    public void setUserId(String userId) {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(Constant.USER_ID, userId);
        editor.apply();
    }

    public void setUserSetted(boolean check) {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(Constant.SHARED_PREFERENCES_USER_SETTED, true);
        editor.apply();
    }

    public void deleteAll() {
        sharedPref.edit().clear().apply();
    }
}

package it.piedpiper.unity.utils.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;

import it.piedpiper.unity.R;
import it.piedpiper.unity.databinding.LoadingDialogBinding;
import it.piedpiper.unity.utils.Animations;
import jp.wasabeef.blurry.Blurry;

public class LoadingDialog {
    private Activity activity;
    private AlertDialog dialog;
    private LoadingDialogBinding binding;

    public LoadingDialog(Activity activity) {
        this.activity = activity;
    }

    public void start(){
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        LayoutInflater inflater = activity.getLayoutInflater();
        binding = LoadingDialogBinding.inflate(inflater);

        builder.setView(binding.getRoot());
        builder.setCancelable(true);

        dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialog.show();
    }

    public void stop(String message){
        TextView text = binding.text;
        text.setText(message);

        ImageView image = binding.image;
        image.setVisibility(View.VISIBLE);
        Animations.alphaView(image,0,1);

        Animations.alphaView(binding.spinner, 1, 0);

        Handler handler = new Handler();

        Runnable runnable = () -> {
            dialog.dismiss();
        };

        handler.postDelayed(runnable, 750);
    }

    public void stop(int time){

        Handler handler = new Handler();
        Runnable runnable = () -> {
            TextView text = binding.text;
            text.setText("Done");

            ImageView image = binding.image;
            image.setVisibility(View.VISIBLE);
            Animations.alphaView(image,0,1);

            Animations.alphaView(binding.spinner, 1, 0);

            Runnable r = () -> {
                dialog.dismiss();
            };

            handler.postDelayed(r, 1500);
        };
        handler.postDelayed(runnable, time);
    }

    public void stop(){
        Handler handler = new Handler();
        Runnable runnable = () -> {
            TextView text = binding.text;
            text.setText("Done");

            ImageView image = binding.image;
            image.setVisibility(View.VISIBLE);
            Animations.alphaView(image,0,1);

            Animations.alphaView(binding.spinner, 1, 0);

            Runnable r = () -> {
                dialog.dismiss();
            };

            handler.postDelayed(r, 750);
        };
        handler.postDelayed(runnable, 750);
    }

    public void stopWithError(String error, int time){
        Handler handler = new Handler();
        Runnable runnable = () -> {
            TextView text = binding.text;
            text.setText(error);

            ImageView image = binding.imageError;
            image.setVisibility(View.VISIBLE);
            Animations.alphaView(image,0,1);

            Animations.alphaView(binding.spinner, 1, 0);

            Runnable r = () -> dialog.dismiss();

            handler.postDelayed(r, 3500);
        };
        handler.postDelayed(runnable, time);
    }
}

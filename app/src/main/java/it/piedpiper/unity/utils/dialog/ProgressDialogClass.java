package it.piedpiper.unity.utils.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import it.piedpiper.unity.databinding.LoadingDialogBinding;
import it.piedpiper.unity.databinding.ProgressDialogBinding;

public class ProgressDialogClass {
    private Activity activity;
    private AlertDialog dialog;
    private ProgressDialogBinding binding;

    public ProgressDialogClass(final Activity activity) {
        this.activity = activity;
    }

    public void showDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        LayoutInflater inflater = activity.getLayoutInflater();
        binding = ProgressDialogBinding.inflate(inflater);

        builder.setView(binding.getRoot());
        builder.setCancelable(true);

        dialog = builder.create();

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialog.setCanceledOnTouchOutside(false);

        activity.runOnUiThread(() -> {
            dialog.show();
        });
    }

    public void dismissDialog() {
        Handler handler = new Handler();
        Runnable runnable = () -> {
            if (dialog.isShowing())
                dialog.dismiss();
        };
        handler.postDelayed(runnable, 1500);
    }

    public boolean isShowing() {
        return dialog.isShowing();
    }

}

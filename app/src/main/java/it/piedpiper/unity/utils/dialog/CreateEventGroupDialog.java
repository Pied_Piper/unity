package it.piedpiper.unity.utils.dialog;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TimePicker;

import androidx.annotation.ColorRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.firebase.Timestamp;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import io.getstream.chat.android.client.ChatClient;
import io.getstream.chat.android.client.call.Call;
import io.getstream.chat.android.client.channel.ChannelClient;
import io.getstream.chat.android.client.models.Attachment;
import io.getstream.chat.android.client.models.Message;
import io.getstream.chat.android.client.utils.Result;
import it.piedpiper.unity.R;
import it.piedpiper.unity.callback.CallBack;
import it.piedpiper.unity.databinding.CreateGroupEventDialogBinding;
import it.piedpiper.unity.model.Association;
import it.piedpiper.unity.model.Event;
import it.piedpiper.unity.model.Group;
import it.piedpiper.unity.model.Lesson;
import it.piedpiper.unity.repository.IAssociationRepository;
import it.piedpiper.unity.repository.IEventRepository;
import it.piedpiper.unity.repository.IGroupRepository;
import it.piedpiper.unity.repository.ILessonRepository;
import it.piedpiper.unity.repository.impl.AssociationRepository;
import it.piedpiper.unity.repository.impl.EventRepository;
import it.piedpiper.unity.repository.impl.GroupRepository;
import it.piedpiper.unity.repository.impl.LessonRepository;
import it.piedpiper.unity.utils.Constant;

public class CreateEventGroupDialog extends BottomSheetDialogFragment {

    private CreateGroupEventDialogBinding binding;
    private IEventRepository eventRepository;
    private IGroupRepository groupRepository;
    private IAssociationRepository associationRepository;
    private ILessonRepository lessonRepository;

    private static final String CID = "cid";
    private static final String TYPE = "type";

    private static final String GROUP = "group";
    private static final String ASSOCIATION = "association";
    private static final String LESSON = "lesson";

    private String type;
    private String cid;

    private Map<String, Object> appointment;

    private Calendar calendar;

    String TAG = "CreateEventGroupDialog";


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = CreateGroupEventDialogBinding.inflate(inflater, container, false);

        eventRepository = new EventRepository(getActivity());
        groupRepository = new GroupRepository(getActivity());
        associationRepository = new AssociationRepository(getActivity());
        lessonRepository = new LessonRepository(getActivity());

        return binding.getRoot();
    }

    @Override
    public int getTheme() {
        return R.style.StreamUiBottomSheetDialogTheme;
    }

    @SuppressLint("SimpleDateFormat")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        type = getArguments().getString(TYPE);
        cid = getArguments().getString(CID);

        Log.d(TAG, "onViewCreated: " + cid);

        calendar = Calendar.getInstance();

        appointment = new HashMap<>();

        checkErrors();

        onClickDate();

        onClickTime();

        binding.cancelButton.setOnClickListener(c -> dismiss());


        binding.confirmButton.setOnClickListener(c -> {
            if(checkFields()){
                appointment.put("subject", binding.subject.getText().toString());
                appointment.put("place", binding.place.getText().toString());

                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                appointment.put("date", format.format(calendar.getTime()));

                Event event = new Event();
                event.setTitle(binding.subject.getText().toString());
                event.setType(type);
                event.setPlace(binding.place.getText().toString());
                event.setDate(new Timestamp(calendar.getTime()));
                event.setMembers(Collections.singletonList(
                        Objects.requireNonNull(ChatClient.instance().getCurrentUser()).getId()));

                String id = cid.substring(type.length()+1);
                switch (type){
                    case ASSOCIATION:
                        associationRepository.getAssociationById(id, new CallBack() {
                            @Override
                            public void onSuccess(Object object) {
                                Association association = (Association) object;

                                event.setUniversity(association.getUniversity());
                                createEvent(event);
                            }

                            @Override
                            public void onError(Object object) {

                            }
                        });
                        break;
                    case GROUP:
                        groupRepository.getGroupById(id, new CallBack() {
                            @Override
                            public void onSuccess(Object object) {
                                Group group = (Group) object;

                                event.setUniversity(group.getUniversity());
                                event.setCourse(group.getCourse());

                                createEvent(event);
                            }

                            @Override
                            public void onError(Object object) {

                            }
                        });
                        break;
                    case LESSON:
                        lessonRepository.getLessonById(id, new CallBack() {
                            @Override
                            public void onSuccess(Object object) {
                                createEvent(event);
                            }

                            @Override
                            public void onError(Object object) {

                            }
                        });
                        break;
                }
                dismiss();
            }
        });
    }

    private void createEvent(Event event){
        eventRepository.createEvent(event, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                appointment.put("id", ((Event) object).getId());
                sendEvent();
                Log.d(TAG, "onSuccess: Event created");
            }

            @Override
            public void onError(Object object) {
                Log.d(TAG, "onError: Event not created");
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        binding = null;
    }

    public static CreateEventGroupDialog newInstance(
            String cid,
            String type
    ) {
        CreateEventGroupDialog createEventGroupDialog = new CreateEventGroupDialog();

        Bundle arguments = new Bundle();
        arguments.putString(CID, cid);
        arguments.putString(TYPE, type);

        createEventGroupDialog.setArguments(arguments);

        return createEventGroupDialog;
    }

    public static CreateEventGroupDialog newCreateEvent(Context context, String type, String cid) {
        return newInstance(
                cid,
                type
        );
    };

    private void sendEvent(){
        Message message = new Message();
        ChatClient client = ChatClient.instance();

        String id = cid.substring(type.length()+1);

        ChannelClient channelClient = client.channel(type, id);

        Log.d(TAG, CID + ": " + id);

        Attachment attachment = new Attachment();
        attachment.setType("appointment");
        attachment.setExtraData(appointment);

        message.setUser(Objects.requireNonNull(client.getCurrentUser()));
        message.setText("");
        message.getAttachments().add(attachment);

        channelClient.sendMessage(message).enqueue(result -> {
            Log.d(TAG, "onResult: sending...");
            if(result.isSuccess())
                Log.d(TAG, "onResult: event sent");
        });
    }

    private void onClickDate(){
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), R.style.DatePickerDialog,
                (datePicker, yearPicked, monthPicked, dayPicked) -> {
                    binding.date.setText(dayPicked + "/" + (monthPicked + 1) + "/" + yearPicked);

                    calendar.set(yearPicked, monthPicked, dayPicked);

                }, year, month, dayOfMonth);

        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());

        binding.btnDate.setOnClickListener(c -> {
            Log.d(TAG, "onClickDate: Clicked");
            if(!datePickerDialog.isShowing())
                requireActivity().runOnUiThread(datePickerDialog::show);
        });
    }

    private void onClickTime(){
        Calendar currentTime = Calendar.getInstance();
        int hour = currentTime.get(Calendar.HOUR_OF_DAY);
        int minute = currentTime.get(Calendar.MINUTE);
        @SuppressLint("SetTextI18n") TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), R.style.DatePickerDialog,
                (timePicker, selectedHour, selectedMinute) -> {
                    String selectedHourS = selectedHour + "";
                    String selectedMinuteS = selectedMinute + "";

                    if(selectedHourS.length() == 1)
                        selectedHourS = "0" + selectedHourS;

                    if(selectedMinuteS.length() == 1)
                        selectedMinuteS = "0" + selectedMinuteS;

                    binding.time.setText(selectedHourS + ":" + selectedMinuteS);

                    calendar.set(Calendar.HOUR_OF_DAY, selectedHour);
                    calendar.set(Calendar.MINUTE, selectedMinute);

                    Log.d(TAG, "onClickTime: " + calendar.get(Calendar.DATE) + " " + calendar.getTime());

                }, hour, minute, true);

        binding.btnTime.setOnClickListener(c -> {
            Log.d(TAG, "onClickTime: Clicked");
            if(!timePickerDialog.isShowing())
                requireActivity().runOnUiThread(timePickerDialog::show);
        });
    }

    private boolean checkFields(){
        if(binding.subject.getText() == null){
            binding.subjectInputLayout.setError("Insert the subject of study");
            return false;
        }
        if(binding.place.getText() == null){
            binding.placeInputLayout.setError("Insert the place");
            return false;
        }
        if(binding.date.getText() == null){
            binding.dateInputLayout.setError("Select a date");
            return false;
        }
        if(binding.time.getText() == null){
            binding.timeInputLayout.setError("Insert the time of appointment");
            return false;
        }


        if(binding.subject.getText().toString().isEmpty()) {
            binding.subjectInputLayout.setError("Insert the subject of study");
            return false;
        }
        if(binding.place.getText().toString().isEmpty()) {
            binding.placeInputLayout.setError("Insert the place");
            return false;
        }
        if(binding.date.getText().toString().isEmpty()) {
            binding.dateInputLayout.setError("Select a date");
            return false;
        }
        if(binding.time.getText().toString().isEmpty()) {
            binding.timeInputLayout.setError("Insert the time of appointment");
            return false;
        }
        return true;
    }

    private void checkErrors(){
        binding.subject.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(binding.subjectInputLayout.getError() != null)
                    binding.subjectInputLayout.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        binding.place.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(binding.placeInputLayout.getError() != null)
                    binding.placeInputLayout.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        binding.date.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(binding.dateInputLayout.getError() != null)
                    binding.dateInputLayout.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        binding.time.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(binding.timeInputLayout.getError() != null)
                    binding.timeInputLayout.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

}

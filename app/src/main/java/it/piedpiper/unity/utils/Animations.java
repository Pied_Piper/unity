package it.piedpiper.unity.utils;

import android.app.Activity;
import android.text.Layout;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class Animations {

    public static void alphaView(View v, float startScale, float endScale) {
        android.view.animation.Animation anim = new AlphaAnimation(startScale, endScale); // Pivot point of Y scaling
        anim.setFillAfter(true); // Needed to keep the result of the animation
        anim.setDuration(250);
        v.startAnimation(anim);
    }

    public static void alphaView(RelativeLayout v, float startScale, float endScale) {
        android.view.animation.Animation anim = new AlphaAnimation(startScale, endScale); // Pivot point of Y scaling
        anim.setFillAfter(true); // Needed to keep the result of the animation
        anim.setDuration(375);
        ((View)v).startAnimation(anim);
    }
}

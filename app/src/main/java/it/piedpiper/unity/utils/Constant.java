package it.piedpiper.unity.utils;

import android.app.Activity;

import it.piedpiper.unity.R;

public class Constant {

    public static final String CHAT_API_KEY = "babbarxfuu8v";

    public static final String LOGIN_STRING = "Login";
    public static final int LOGIN = R.id.login;
    public static final int SIGNUP = R.id.signup;
    public static final int EMAIL = R.id.email;
    public static final int PASSWORD = R.id.password;
    public static final int VERIFY_EMAIL = R.id.verify_email;
    public static final int FORGOT_PASSWORD = R.id.forgot_password;
    public static final int SET_USER = R.id.set_user;
    public static final int SET_UNIVERSITY = R.id.set_university;
    public static final String USER_STRING = "User";

    public static final String EMAIL_STRING = "Email";
    public static final String PASSWORD_STRING = "Password";

    public static final int ADD_GROUP = R.id.create_group;
    public static final int ADD_ASSOCIATION = R.id.create_association;
    public static final int ADD_LESSON = R.id.create_lesson;
    public static final int ADD_FILE = R.id.upload_file;
    public static final String GROUP = "Group";
    public static final String ASSOCIATION = "Association";
    public static final String LESSON = "Lesson";
    public static final String FILE = "File";

    public static final String FRAGMENT_STRING = "Fragment";

    //Nav drawer
    public static final String PROFILE_STRING = "Profile";
    public static final String SETTINGS_STRING = "Settings";
    public static final String EVENTS_STRING = "Events";
    public static final String CALENDAR_STRING = "Calendar";
    public static final String ABOUT_STRING = "About";
    public static final String HELP_STRING = "Help";
    public static final String CONTACTS_STRING = "Contacts";
    public static final String FAQ_STRING = "Faq";

    //Fragments
    public static final String VERIFY_STRING = "Verify";
    public static final String SIGNUP_STRING = "SignUp";
    public static final String ONBOARDING_STRING = "OnBoarding";


    public static final int PROFILE = R.id.profile;
    public static final int SETTINGS = R.id.settings;
    public static final int EVENTS = R.id.events;
    public static final int CALENDAR = R.id.calendar;
    public static final int ABOUT = R.id.about;
    public static final int HELP = R.id.help;
    public static final int LOGOUT = R.id.nav_item_logout;
    public static final int CONTACTS = R.id.contacts;
    public static final int FAQ = R.id.faq;

    //Bottom Nav View
    public static final int SOCIAL = R.id.social;
    public static final int GROUPS = R.id.groups;
    public static final int LESSONS = R.id.lessons;
    public static final int LIBRARY = R.id.library;

    //
    public static final String SUCCESS = "SUCCESS";
    public static final String FAIL = "FAIL";
    public static final String ADD = "ADD";
    public static final String UPDATE = "UPDATE";
    public static final String DELETE = "DELETE";
    public static final String BR = "BR";

    public static final String USER_DATABASE = "user_database";
    public static final String AUTHENTICATION_TOKEN = "AUTHENTICATION_TOKEN";
    public static final String USER_ID = "USER_ID";
    public static final String SHARED_PREFERENCES_FILE_NAME = "it.piedpiper.unity.preferences";
    public static final String SHARED_PREFERENCES_USER_SETTED = "USER_SETTED";
    public static final String LAST_UPDATE = "last_update";

    public static final String FILE_DATABASE = "file_database";


    //API
    public static final String UNIVERSITIES_API_BASE_URL = "http://universities.hipolabs.com/";
    public static final String SEARCH_ENDPOINT = "search";
    public static final String SEARCH_NAME_PARAMETER = "name";
    public static final String SEARCH_COUNTRY_PARAMETER = "country";

    public static final String LOGO_API_KEY = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiZGMwODk4ZTY2ZWQ0OTkwNjdjY2U5MGUzNDhlZTExMWVlMzAzNTNiMGZhMDAwYWEwNmE5M2I1YWY4MDlhMmViNjdiZTMzNDQwNzQ1YjM3NGYiLCJpYXQiOjE2NDM5MTkyNjcsIm5iZiI6MTY0MzkxOTI2NywiZXhwIjoxNjc1NDU1MjY3LCJzdWIiOiI1NzA4Iiwic2NvcGVzIjpbXX0.m4C_1Ony9q5yX-ygCtya5_m3L1jRWyJ4UDySBTP2V5vhYFSZCUsYsP-fDacrf1qvFiblR3954J9_pc8VejYocg";
    public static final String LOGO_API_BASE_URL = "https://www.klazify.com/api/";
    public static final String CATEGORIZE_ENDPOINT = "categorize";
    public static final String URL_PARAMETER = "url";
    public static final String OPEN = "Open";
    public static final String DOWNLOAD = "Download";


    public static String[] getCourses(Activity activity) {
        return activity.getResources().getStringArray(R.array.university_courses);
    }


    public static final int PERMISSION_REQUEST_CODE_ALL = 0;
    public static final int PERMISSION_REQUEST_CODE = 1;

}

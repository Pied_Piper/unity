package it.piedpiper.unity.model;

import android.annotation.SuppressLint;

import com.google.firebase.Timestamp;
import com.google.firebase.firestore.Exclude;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class Event implements Serializable {

    private String id;
    private String title;

    private String type;

    private String course;
    private String description;
    private String university;
    private String place;
    private Timestamp date;

    private List<String> members;

    public Event(){

    }

    public Event(String title, String description, String type, String university, String course, String place, Timestamp date) {
        this.title = title;
        this.description = description;
        this.university = university;
        this.place = place;
        this.date = date;
        this.course = course;
        this.type = type;

        this.members = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public List<String> getMembers() {
        return members;
    }

    public void setMembers(List<String> members) {
        this.members = members;
    }


    @Exclude
    public Calendar getCalendar(){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(getDate().toDate());

        return calendar;
    }

    @Exclude
    public String getCalendarDay() {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(getDate().toDate());

            return "" + calendar.get(Calendar.DAY_OF_MONTH);
    }

    @Exclude
    public String getCalendarMonth(){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(getDate().toDate());

        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");
        String[] dateToday = sdf.format(calendar.getTime()).split("/");
        return "" + dateToday[1];
    }

    @Exclude
    public HashMap<String, Object> getMap() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("id", getId());
        map.put("title", getTitle());
        map.put("type", getType());
        map.put("course", getCourse());
        map.put("description", getDescription());
        map.put("university", getUniversity());
        map.put("place", getPlace());
        map.put("date", getDate());
        map.put("members", getMembers());
        return map;
    }
}

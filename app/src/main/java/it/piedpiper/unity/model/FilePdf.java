package it.piedpiper.unity.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.firebase.database.Exclude;

import java.io.Serializable;

@Entity
public class FilePdf implements Serializable {

    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "id")
    private String id;

    @Exclude
    @ColumnInfo(name = "storageUrl")
    private String storageUrl;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "category")
    private String category;

    @ColumnInfo(name = "uri")
    private String uri;
    
    public FilePdf(){
        id = "";
    }

    public FilePdf(@NonNull String id, String storageUrl, String name, String category, String uri){
        this.id = id;
        this.storageUrl = storageUrl;
        this.name = name;
        this.category = category;
        this.uri = uri;
    }

    public FilePdf(String name, String uri, String category) {
        this.name = name;
        this.uri = uri;
        this.category = category;
        id = "";
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    @Exclude
    public String getStorageUrl() {
        return storageUrl;
    }

    @Exclude
    public void setStorageUrl(String storageUrl) {
        this.storageUrl = storageUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @NonNull
    @Override
    public String toString() {
        return "FilePdf{" +
                "id='" + id + '\'' +
                ", storageUrl='" + storageUrl + '\'' +
                ", name='" + name + '\'' +
                ", category='" + category + '\'' +
                ", uri='" + uri + '\'' +
                '}';
    }
}

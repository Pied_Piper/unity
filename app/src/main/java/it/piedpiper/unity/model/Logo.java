package it.piedpiper.unity.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Logo implements Parcelable {
    private final String domain_url;

    public String getDomain_url() {
        return domain_url;
    }

    public String getLogo_url() {
        return logo_url;
    }

    private final String logo_url;

    protected Logo(String domain_url, Parcel in) {
        this.domain_url = domain_url;
        this.logo_url = in.readString();
    }

    protected Logo(Parcel in) {
        domain_url = in.readString();
        logo_url = in.readString();
    }

    public static final Creator<Logo> CREATOR = new Creator<Logo>() {
        @Override
        public Logo createFromParcel(Parcel in) {
            return new Logo(in);
        }

        @Override
        public Logo[] newArray(int size) {
            return new Logo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(domain_url);
        dest.writeString(logo_url);
    }
}

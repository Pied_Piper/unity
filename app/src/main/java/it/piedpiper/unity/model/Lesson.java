package it.piedpiper.unity.model;

import com.google.firebase.firestore.Exclude;

import java.io.Serializable;
import java.util.HashMap;

public class Lesson implements Serializable {
    private String id, subject, description, place, price;
    private String userID;

    public Lesson(){

    }

    public Lesson(String subject, String description, String place, String price, String userID) {
        this.subject = subject;
        this.description = description;
        this.place = place;
        this.price = price;
        this.userID = userID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String id) {
        this.userID = id;
    }

    @Exclude
    public HashMap<String, Object> getMap() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("id", getId());
        map.put("subject", getSubject());
        map.put("description", getDescription());
        map.put("place", getPlace());
        map.put("price", getPrice());
        map.put("user", getUserID());

        return map;
    }
}

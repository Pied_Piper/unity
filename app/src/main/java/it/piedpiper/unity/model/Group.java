package it.piedpiper.unity.model;

import com.google.firebase.firestore.Exclude;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

public class Group implements Serializable {
    private String id, name, university, course, subject, photoUrl;
    private List<String> members;

    public Group() {
    }

    public Group(String name, String university, String course, String subject, String photoUrl, List<String> members) {
        this.name = name;
        this.university = university;
        this.course = course;
        this.subject = subject;
        this.members = members;
        this.photoUrl = photoUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public List<String> getMembers() {
        return members;
    }

    public void setMembers(List<String> members) {
        this.members = members;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    @Exclude
    public HashMap<String, Object> getMap() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("id", getId());
        map.put("name", getName());
        map.put("university", getUniversity());
        map.put("course", getCourse());
        map.put("subject", getSubject());
        map.put("photoUrl", getPhotoUrl());
        map.put("users", getMembers());

        return map;
    }
}

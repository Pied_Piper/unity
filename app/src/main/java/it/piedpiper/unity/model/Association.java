package it.piedpiper.unity.model;

import com.google.firebase.firestore.Exclude;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

public class Association implements Serializable {
    private String id, name, description, university, photoUrl;
    private List<String> members;

    public Association(){

    }

    public Association(String name, String description, String university, String photoUrl, List<String> members) {
        this.name = name;
        this.description = description;
        this.university = university;
        this.photoUrl = photoUrl;
        this.members = members;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public List<String> getMembers() {
        return members;
    }

    public void setMembers(List<String> members) {
        this.members = members;
    }

    @Exclude
    public HashMap<String, Object> getMap() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("id", getId());
        map.put("name", getName());
        map.put("description", getDescription());
        map.put("university", getUniversity());
        map.put("photoUrl", getPhotoUrl());
        map.put("members", getMembers());

        return map;
    }
}

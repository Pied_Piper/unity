package it.piedpiper.unity.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.firebase.firestore.Exclude;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;

@Entity
public class User implements Serializable {

    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "id")
    private String id;

    @ColumnInfo(name = "username")
    private String username;
    @ColumnInfo(name = "email")
    private String email;
    @ColumnInfo(name = "photoUrl")
    private String photoUrl;

    @Ignore
    private UserUniversity university;

    @ColumnInfo(name = "userUniversity")
    @Exclude
    private String userUniversity;

    @ColumnInfo(name = "userCourse")
    @Exclude
    private String userCourse;

    @ColumnInfo(name = "photo")
    @Exclude
    private byte[] photo;

    @Ignore
    public User() {
        id = "";
    }

    @Ignore
    public User(@NonNull String id, String username, String email, String userUniversity, String userCourse, String photoUrl) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.university = new UserUniversity(userUniversity, userCourse);
        this.userUniversity = userUniversity;
        this.userCourse = userCourse;
        this.photoUrl = photoUrl;
    }

    public User(@NonNull String id, String username, String email, String userUniversity, String userCourse, String photoUrl, byte[] photo) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.university = new UserUniversity(userUniversity, userCourse);
        this.userUniversity = userUniversity;
        this.userCourse = userCourse;
        this.photoUrl = photoUrl;
        this.photo = photo;
    }


    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserUniversity getUniversity() {
        return university;
    }

    public void setUniversity(UserUniversity university) {
        this.university = university;
    }

    @Exclude
    public String getUserUniversity() {
        return userUniversity;
    }

    @Exclude
    public void setUserUniversity(String userUniversity) {
        this.userUniversity = userUniversity;
    }

    @Exclude
    public String getUserCourse() {
        return userCourse;
    }

    @Exclude
    public void setUserCourse(String userCourse) {
        this.userCourse = userCourse;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    @Exclude
    public HashMap<String, Object> getMap() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("id", getId());
        map.put("username", getUsername());
        map.put("email", getEmail());
        map.put("photoUrl", getPhotoUrl());
        map.put("university", getUniversity());
        return map;
    }

    @NonNull
    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", photoUrl='" + photoUrl + '\'' +
                ", university=" + university +
                ", userUniversity='" + userUniversity + '\'' +
                ", userCourse='" + userCourse + '\'' +
                ", photo=" + Arrays.toString(photo) +
                '}';
    }
}


package it.piedpiper.unity.model;

import android.os.Parcel;
import android.os.Parcelable;

public class LogoResponse implements Parcelable {
    private final Logo domain;

    public Logo getDomain() {
        return domain;
    }

    protected LogoResponse(Parcel in) {
        domain = in.readParcelable(Logo.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(domain, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<LogoResponse> CREATOR = new Creator<LogoResponse>() {
        @Override
        public LogoResponse createFromParcel(Parcel in) {
            return new LogoResponse(in);
        }

        @Override
        public LogoResponse[] newArray(int size) {
            return new LogoResponse[size];
        }
    };
}

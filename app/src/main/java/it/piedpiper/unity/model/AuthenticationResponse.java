package it.piedpiper.unity.model;

import androidx.annotation.NonNull;

/**
 * It represents a response used to manage the
 * Firebase Authentication response.
 */
public class AuthenticationResponse {

    private boolean success;
    private String message;

    public AuthenticationResponse() {
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @NonNull
    @Override
    public String toString() {
        return "AuthenticationResponse{" +
                "success=" + success +
                ", message='" + message + '\'' +
                '}';
    }
}

package it.piedpiper.unity.model;

import com.google.firebase.Timestamp;

public class CalendarEvent extends Event{

    public CalendarEvent() {
    }

    public CalendarEvent(String title, String description, String type, String university, String course, String place, Timestamp date) {
        super(title, description, type, university, course, place, date);
    }
}

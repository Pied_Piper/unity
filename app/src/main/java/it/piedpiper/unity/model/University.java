package it.piedpiper.unity.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import com.google.firebase.firestore.Exclude;

import java.util.HashMap;
import java.util.List;

public class University implements Parcelable {
    private String name;
    private String country;
    private List<String> web_pages;
    private List<String> domains;

    public University() {

    }

    public University(String name, String country, List<String> web_pages, List<String> domains) {
        this.name = name;
        this.country = country;
        this.web_pages = web_pages;
        this.domains = domains;
    }

    protected University(Parcel in) {
        name = in.readString();
        country = in.readString();
        web_pages = in.createStringArrayList();
        domains = in.createStringArrayList();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(country);
        dest.writeStringList(web_pages);
        dest.writeStringList(domains);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<University> CREATOR = new Creator<University>() {
        @Override
        public University createFromParcel(Parcel in) {
            return new University(in);
        }

        @Override
        public University[] newArray(int size) {
            return new University[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public List<String> getWeb_pages() {
        return web_pages;
    }

    public void setWeb_pages(List<String> web_pages) {
        this.web_pages = web_pages;
    }

    public List<String> getDomains() {
        return domains;
    }

    public void setDomains(List<String> domains) {
        this.domains = domains;
    }

    @NonNull
    @Override
    public String toString() {
        return "UniversityApi{" +
                "name='" + name + '\'' +
                ", country='" + country + '\'' +
                ", web_pages=" + web_pages +
                ", domains=" + domains +
                '}';
    }

    @Exclude
    public HashMap<String, Object> getMap() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("name", getName());
        map.put("country", getCountry());
        map.put("web_pages", getWeb_pages());
        map.put("domains", getDomains());
        return map;
    }
}

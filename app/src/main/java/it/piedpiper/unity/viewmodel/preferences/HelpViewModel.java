package it.piedpiper.unity.viewmodel.preferences;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.View;

import androidx.cardview.widget.CardView;
import androidx.navigation.NavController;

import it.piedpiper.unity.databinding.FragmentHelpBinding;
import it.piedpiper.unity.ui.main.MainActivity;
import it.piedpiper.unity.ui.preferences.PreferencesActivity;
import it.piedpiper.unity.utils.Constant;

public class HelpViewModel {

    private static final String TAG = "Help";
    private final PreferencesActivity activity;
    private final FragmentHelpBinding binding;

    private CardView mFaqCard;
    private CardView mContactsCard;

  //  private NavController navController;

    public HelpViewModel(PreferencesActivity activity, FragmentHelpBinding binding){
        this.binding = binding;
        this.activity = activity;
    }

    public void initUI(){
        setCards();
    }

    public void setCards(){
        //mFeedbackCard = binding.feedbackCard;
        mFaqCard = binding.faqCard;
        mContactsCard = binding.contactsCard;

        /*mFeedbackCard.setOnClickListener(v -> {
            Log.d(TAG, "setCards: Feedback");
        });*/

        /*mFaqCard.setOnClickListener(v -> {
            Log.d(TAG, "setCards: Faq");
        });*/

        mContactsCard.setOnClickListener(v -> {
            Intent intent = new Intent(activity, PreferencesActivity.class);
            intent.putExtra(Constant.FRAGMENT_STRING, Constant.CONTACTS_STRING);
            activity.startActivity(intent);
        });

        mFaqCard.setOnClickListener(v -> {
            Intent intent = new Intent(activity, PreferencesActivity.class);
            intent.putExtra(Constant.FRAGMENT_STRING, Constant.FAQ_STRING);
            activity.startActivity(intent);
        });
    }

}

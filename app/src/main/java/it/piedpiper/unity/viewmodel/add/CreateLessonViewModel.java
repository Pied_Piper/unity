package it.piedpiper.unity.viewmodel.add;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.util.Log;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;

import it.piedpiper.unity.callback.CallBack;
import it.piedpiper.unity.databinding.FragmentCreateLessonBinding;
import it.piedpiper.unity.model.Lesson;
import it.piedpiper.unity.model.User;
import it.piedpiper.unity.repository.ILessonRepository;
import it.piedpiper.unity.repository.impl.LessonRepository;
import it.piedpiper.unity.ui.add.AddActivity;
import it.piedpiper.unity.utils.Constant;

public class CreateLessonViewModel {
    private FragmentCreateLessonBinding binding;
    private AddActivity activity;
    private ArrayList<String> universities;
    private ILessonRepository lessonRepository;


    private TextInputLayout mArgumentInputLayout;
    private TextInputEditText mArgument;
    private TextInputLayout mDescriptionInputLayout;
    private TextInputEditText mDescription;
    private TextInputLayout mPlaceInputLayout;
    private AutoCompleteTextView mPlace;
    private TextInputLayout mPriceInputLayout;
    private TextInputEditText mPrice;

    private Button mBtnDone;

    public CreateLessonViewModel(AddActivity activity, FragmentCreateLessonBinding binding){
        this.activity = activity;
        this.binding = binding;
        this.lessonRepository = new LessonRepository(activity);
    }

    public void initUI(){
        mArgumentInputLayout = binding.argumentInputLayout;
        mArgument = binding.argument;

        mDescriptionInputLayout = binding.descriptionInputLayout;
        mDescription = binding.description;

        mPlaceInputLayout = binding.placeInputLayout;
        mPlace = binding.place;

        mPriceInputLayout = binding.priceInputLayout;
        mPrice = binding.price;

        mBtnDone = binding.btnCreateLesson;
    }

    public void setOnClickListeners(){
        mBtnDone.setOnClickListener(c -> {
            if(checkFields()){
                String argument = mArgument.getText().toString();
                String description = mDescription.getText().toString();
                String place = mPlace.getText().toString();
                String price = mPrice.getText().toString();

                SharedPreferences sharedPreferences = activity.getSharedPreferences(Constant.SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
                String userID = sharedPreferences.getString(Constant.USER_ID, null);

                Lesson lesson = new Lesson(argument, description, place, price, userID);
                lessonRepository.createLesson(lesson, new CallBack() {
                    @Override
                    public void onSuccess(Object object) {
                        Runnable runnable = activity::finish;
                        Handler handler = new Handler();
                        handler.postDelayed(runnable, 1501);
                    }

                    @Override
                    public void onError(Object object) {

                    }
                });
            }
        });
    }

    private boolean checkFields(){
        String argument = mArgument.getText().toString();
        String description = mDescription.getText().toString();
        String place = mPlace.getText().toString();
        String price = mPrice.getText().toString();

        if(argument.isEmpty()) {
            mArgumentInputLayout.setError("Insert the lessons subject of study!");
            return false;
        }
        if(description.isEmpty()) {
            mDescriptionInputLayout.setError("Insert a presentation of your lessons!");
            return false;
        }
        if(place.isEmpty()){
            mPlace.setError("Insert the city where you teach!");
            return false;
        }
        if(price.isEmpty()) {
            mPriceInputLayout.setError("Insert the price per lesson!");
            return false;
        }
        else if(Double.parseDouble(price) > 50){
            mPriceInputLayout.setError("You are so expansive. Insert a value < 51");
            return false;
        }
        return true;
    }
}

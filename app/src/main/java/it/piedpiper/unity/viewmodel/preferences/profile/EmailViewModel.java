package it.piedpiper.unity.viewmodel.preferences.profile;

import android.os.Handler;
import android.util.Patterns;

import com.google.firebase.auth.FirebaseAuth;

import java.util.Objects;

import io.getstream.chat.android.client.ChatClient;
import it.piedpiper.unity.callback.CallBack;
import it.piedpiper.unity.databinding.FragmentEmailBinding;
import it.piedpiper.unity.model.User;
import it.piedpiper.unity.repository.IUserRepository;
import it.piedpiper.unity.repository.impl.UserRepository;
import it.piedpiper.unity.ui.preferences.PreferencesActivity;

public class EmailViewModel {
    private final PreferencesActivity activity;
    private final FragmentEmailBinding binding;

    private final IUserRepository userRepository;

    public EmailViewModel(PreferencesActivity activity, FragmentEmailBinding binding) {
        this.activity = activity;
        this.binding = binding;
        this.userRepository = new UserRepository(activity);
    }

    public void initUI() {
        binding.btnSave.setOnClickListener(c -> {
            if(binding.email.getText() != null && isValidEmail(binding.email.getText().toString())) {
                userRepository.readUserByID(Objects.requireNonNull(ChatClient.instance().getCurrentUser()).getId(), new CallBack() {
                    @Override
                    public void onSuccess(Object object) {
                        Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).updateEmail(binding.email.getText().toString());

                        User user = (User) object;
                        user.setEmail(binding.email.getText().toString().toLowerCase());

                        userRepository.updateUser(user.getId(), user, new CallBack() {
                            @Override
                            public void onSuccess(Object object) {
                                Handler handler = new Handler();
                                Runnable runnable = activity::finish;

                                handler.postDelayed(runnable, 1500);
                            }

                            @Override
                            public void onError(Object object) {

                            }
                        });
                    }

                    @Override
                    public void onError(Object object) {

                    }
                });
            }
        });
    }

    private boolean isValidEmail(CharSequence email) {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
}

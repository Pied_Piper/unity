package it.piedpiper.unity.viewmodel;

import android.app.Activity;
import android.app.Application;
import android.util.Log;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import it.piedpiper.unity.callback.CallBack;
import it.piedpiper.unity.model.User;
import it.piedpiper.unity.repository.IUserRepository;
import it.piedpiper.unity.repository.impl.UserRepository;

public class UserViewModel extends AndroidViewModel {
    private static final String TAG = "UserViewModel";
    private User user;
    public UserViewModel(Application application, Activity activity) {
        super(application);
        IUserRepository userRepository = new UserRepository(activity);
        userRepository.readUserDao(new CallBack() {
            @Override
            public void onSuccess(Object object) {
                user = (User) object;
            }

            @Override
            public void onError(Object object) {

            }
        });
    }

    public User getUser() {
        return user;
    }
}

package it.piedpiper.unity.viewmodel.preferences;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.widget.AppCompatButton;

import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import io.getstream.chat.android.client.ChatClient;
import io.getstream.chat.android.ui.avatar.AvatarView;
import it.piedpiper.unity.adapter.AdapterActions;
import it.piedpiper.unity.callback.CallBack;
import it.piedpiper.unity.databinding.FragmentProfileBinding;
import it.piedpiper.unity.model.University;
import it.piedpiper.unity.model.User;
import it.piedpiper.unity.repository.IStorageRepository;
import it.piedpiper.unity.repository.IUniversityRepository;
import it.piedpiper.unity.repository.IUserRepository;
import it.piedpiper.unity.repository.impl.StorageRepository;
import it.piedpiper.unity.repository.impl.UniversityRepository;
import it.piedpiper.unity.repository.impl.UserRepository;
import it.piedpiper.unity.ui.preferences.PreferencesActivity;
import it.piedpiper.unity.utils.Constant;

public class ProfileViewModel {
    private static final String TAG = "ProfileViewModel";
    private final PreferencesActivity activity;
    private final FragmentProfileBinding binding;
    private final IUserRepository userRepository;
    private final IUniversityRepository universityRepository;
    private final IStorageRepository storageRepository;

    private TextInputLayout mUsernameInputLayout, mUniversityInputLayout, mUniversityCourseInputLayout;
    private TextInputEditText mUsername;
    private AutoCompleteTextView mUniversity, mUniversityCourse;

    private FloatingActionButton mRemovePhoto;
    private AvatarView avatarView;
    private ImageView mUserPhotoImg;

    private AppCompatButton mSave;
    private AppCompatButton mLogout;

    private ActivityResultLauncher<Intent> launchActivity;

    private User user;

    private ArrayList<String> universities;
    private final String[] courses;

    private final HashMap<String, University> universitiesMap;

    public ProfileViewModel(PreferencesActivity activity, FragmentProfileBinding binding){
        this.activity = activity;
        this.binding = binding;

        this.userRepository = new UserRepository(activity);
        this.universityRepository = new UniversityRepository();
        this.storageRepository = new StorageRepository(activity);

        this.courses = Constant.getCourses(activity);

        this.universities = new ArrayList<>();
        this.universitiesMap = new HashMap<>();
    }

    public void initUI() {
        mUsernameInputLayout = binding.usernameInputLayout;
        mUsername = binding.username;

        mUniversityInputLayout = binding.universityInputLayout;
        mUniversity = binding.university;

        mUniversityCourseInputLayout = binding.universityCourseInputLayout;
        mUniversityCourse = binding.universityCourse;

        mRemovePhoto = binding.btnChangePhoto;

        mUserPhotoImg = binding.imageUser;

        mSave = binding.saveButton;
        mLogout = binding.logoutButton;

        avatarView = binding.avatarView;
        avatarView.setUserData(Objects.requireNonNull(ChatClient.instance().getCurrentUser()));

        initImageUser();
    }

    public void checkAll(){
        checkUniversity();
        checkUsername();
        checkUniversityCourse();
    }

    private void initImageUser() {
        userRepository.readUserDao(new CallBack() {
            @Override
            public void onSuccess(Object object) {
                user = (User) object;

                Log.d(TAG, "onSuccess: " + user.toString());

                binding.setUser(user);

                if(user.getPhotoUrl().isEmpty())
                    setVisible(mRemovePhoto, false);
            }

            @Override
            public void onError(Object object) {

            }
        });
    }

    private void setVisible(View view, boolean check){
        activity.runOnUiThread(() -> {
            if(check)
                view.setVisibility(View.VISIBLE);
            else
                view.setVisibility(View.GONE);
        });
    }


    public void setLaunchActivity() {
        launchActivity = activity.registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(), result -> {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        Intent data = result.getData();
                        assert data != null;
                        Uri uri = data.getData();
                        try {
                            Bitmap bitmap = MediaStore.Images.Media.getBitmap(activity.getContentResolver(), uri);
                            mRemovePhoto.setVisibility(View.VISIBLE);

                            avatarView.setVisibility(View.GONE);
                            mUserPhotoImg.setVisibility(View.VISIBLE);

                            mUserPhotoImg.setImageBitmap(Bitmap.createScaledBitmap(bitmap, 100, 100, false));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    private void checkUsername(){
        mUsername.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(mUsernameInputLayout.getError() != null)
                    mUsernameInputLayout.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void checkUniversity(){
        mUniversity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(mUniversityInputLayout.getError() != null)
                    mUniversityInputLayout.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void checkUniversityCourse(){
        mUniversityCourse.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(mUniversityCourseInputLayout.getError() != null)
                    mUniversityCourseInputLayout.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void setOnClickListeners(){
        AdapterActions.setAdapter(activity, mUniversityCourse, courses);

        onClickUniversity();
        onClickChangePhoto();
        onClickRemovePhoto();
        onClickSave();
        onClickLogout();
    }

    private void onClickUniversity() {
        universityRepository.fetchAllUniversities(new CallBack() {
            @Override
            public void onSuccess(Object object) {
                List<University> universityList = (List<University>) object;

                Log.d(TAG, "onSuccess: " + universityList);

                for(University university : universityList){
                    universities.add(university.getName());
                    universitiesMap.put(university.getName(), university);
                }

                AdapterActions.setAdapter(activity, mUniversity, universities);
            }

            @Override
            public void onError(Object object) {

            }
        });
    }

    private void onClickRemovePhoto() {
        mRemovePhoto.setOnClickListener(c -> {
            mUserPhotoImg.setImageDrawable(null);
            mUserPhotoImg.setVisibility(View.GONE);
            avatarView.setVisibility(View.VISIBLE);

            io.getstream.chat.android.client.models.User u = new io.getstream.chat.android.client.models.User();
            u.setName(user.getUsername());
            u.setImage("");
            avatarView.setUserData(u);

            mRemovePhoto.setVisibility(View.GONE);
        });
    }


    private void onClickLogout() {
        mLogout.setOnClickListener(c -> {
            userRepository.logout(new CallBack() {
                @Override
                public void onSuccess(Object object) {

                }

                @Override
                public void onError(Object object) {

                }
            });
        });
    }

    private void onClickChangePhoto(){
        avatarView.setOnClickListener(c -> {
            if(activity.checkPermission()) {
                ImagePicker.Companion.with(activity)
                        .crop()	    			//Crop image(Optional), Check Customization for more option
                        .compress(1024)//Final image size will be less than 1 MB(Optional)
                        .maxResultSize(1080, 1080)
                        .createIntent(intent -> {
                            launchActivity.launch(intent);
                            return null;
                        });
            }
            else
                activity.requestPermission();
        });

        mRemovePhoto.setOnClickListener(c -> {
            mUserPhotoImg.setImageDrawable(null);
            avatarView.setVisibility(View.VISIBLE);
            mUserPhotoImg.setVisibility(View.GONE);
            mRemovePhoto.setVisibility(View.GONE);
        });
    }


    private void onClickSave(){
        mSave.setOnClickListener(c -> {
            if(checkFields()){
                String id = ChatClient.instance().getCurrentUser().getId();
                if(!mUsername.getText().toString().isEmpty()) {
                    userRepository.readUserByUsername(mUsername.getText().toString(), new CallBack() {
                        @Override
                        public void onSuccess(Object object) {
                            if (object != null) {
                                mUsernameInputLayout.setError("Username not valid, please insert another username.");
                            } else {
                                Bundle args = new Bundle();
                                if (mUserPhotoImg.getDrawable() != null) {
                                    mUserPhotoImg.setDrawingCacheEnabled(true);
                                    mUserPhotoImg.buildDrawingCache();
                                    Bitmap bitmap = ((BitmapDrawable) mUserPhotoImg.getDrawable()).getBitmap();
                                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                                    byte[] data = baos.toByteArray();

                                    args.putByteArray("Image", data);

                                    storageRepository.putImage(binding.getUser().getUsername(), data, new CallBack() {
                                        @Override
                                        public void onSuccess(Object object) {
                                            String uri = ((Uri) object).toString();

                                            user.setPhotoUrl(uri);
                                            if (!mUsername.getText().toString().isEmpty())
                                                user.setUsername(mUsername.getText().toString());

                                            updateUser(user);
                                        }

                                        @Override
                                        public void onError(Object object) {

                                        }
                                    });
                                } else {
                                    if (!mUsername.getText().toString().isEmpty())
                                        user.setUsername(mUsername.getText().toString());

                                    updateUser(user);
                                }
                            }
                        }

                        @Override
                        public void onError(Object object) {
                            mUsernameInputLayout.setError("Insert a valid username!");
                        }
                    });
                }
                else {
                    updateUser(user);
                }
            }
        });
    }

    private boolean checkFields() {
        String username = mUsername.getText().toString();
        String university = mUniversity.getText().toString();
        String course = mUniversityCourse.getText().toString();

        if(!university.isEmpty() && isValidUniversity(university)){
            user.getUniversity().setName(university);
        }
        if(!course.isEmpty() && isValidCourse(course)){
            user.getUniversity().setCourse(course);
        }
        if(mUserPhotoImg.getDrawable() == null) {
            user.setPhoto(null);
            user.setPhotoUrl("");
        }
        return true;
    }

    private boolean isValidUniversity(String university) {
        for (String u : universities)
            if(u.equals(university))
                return true;
        return false;
    }

    private boolean isValidCourse(String course) {
        for (String c : courses)
            if(c.equals(course))
                return true;
        return false;
    }

    private void updateUser(User user) {
        userRepository.updateUser(user.getId(), user, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                binding.setUser((User) object);
                Log.d(TAG, "onSuccess: user updated");
            }

            @Override
            public void onError(Object object) {

            }
        });
    }


}

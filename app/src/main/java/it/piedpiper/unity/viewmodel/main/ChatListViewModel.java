package it.piedpiper.unity.viewmodel.main;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.SavedStateViewModelFactory;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.getstream.sdk.chat.utils.Utils;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.getstream.chat.android.client.ChatClient;
import io.getstream.chat.android.client.api.models.FilterObject;
import io.getstream.chat.android.client.api.models.QueryChannelRequest;
import io.getstream.chat.android.client.api.models.QueryChannelsRequest;
import io.getstream.chat.android.client.api.models.QuerySort;
import io.getstream.chat.android.client.call.Call;
import io.getstream.chat.android.client.models.Channel;
import io.getstream.chat.android.client.models.Filters;
import io.getstream.chat.android.client.models.User;
import io.getstream.chat.android.client.socket.SocketListener;
import io.getstream.chat.android.client.utils.Result;
import io.getstream.chat.android.offline.ChatDomain;
import io.getstream.chat.android.offline.querychannels.ChatEventHandlerFactory;
import io.getstream.chat.android.ui.channel.list.ChannelListView;
import io.getstream.chat.android.ui.channel.list.adapter.ChannelListItem;
import io.getstream.chat.android.ui.channel.list.viewmodel.ChannelListViewModel;
import io.getstream.chat.android.ui.channel.list.viewmodel.ChannelListViewModelBinding;
import io.getstream.chat.android.ui.channel.list.viewmodel.factory.ChannelListViewModelFactory;
import io.getstream.chat.android.ui.search.SearchInputView;
import io.getstream.chat.android.ui.search.list.SearchResultListView;
import io.getstream.chat.android.ui.search.list.viewmodel.SearchViewModel;
import io.getstream.chat.android.ui.search.list.viewmodel.SearchViewModelBinding;
import it.piedpiper.unity.R;
import it.piedpiper.unity.databinding.ChannelListLoadingViewBinding;
import it.piedpiper.unity.databinding.ChannelsEmptyViewBinding;
import it.piedpiper.unity.databinding.FragmentChatListBinding;

import it.piedpiper.unity.repository.IUserRepository;
import it.piedpiper.unity.repository.impl.UserRepository;
import it.piedpiper.unity.ui.main.MainActivity;
import it.piedpiper.unity.ui.main.chat.ChatActivity;
import it.piedpiper.unity.utils.dialog.ConfirmationDialogFragment;

public class ChatListViewModel {
    private static final String TAG = "ChatListViewModel";
    private final MainActivity activity;
    private final FragmentChatListBinding binding;

    private final String type;

    private final ChatClient client;
    private ChannelListViewModel channelsViewModel;
    private ChannelListView channelListView;
    private final User user;

    private SearchViewModel searchViewModel;
    private SearchResultListView searchResultListView;
    private SearchInputView searchInputView;

    private int n_channels;

    public ChatListViewModel(MainActivity activity, FragmentChatListBinding binding, String type) {
        this.activity = activity;
        this.binding = binding;
        this.type = type;
        this.client = ChatClient.instance();
        this.user = client.getCurrentUser();
    }

    public void initUI(){
        initChannelList();
    }

    private void initChannelList(){
        loadChannels();

        setupChannelList();

        onClickSearch();
        onClickChat();

        channelsViewModel.getState().observe(activity, state -> {
            if(n_channels != state.getChannels().size())
                loadChannels();
        });
        
        channelListView.setChannelLeaveClickListener(channel -> {
            channelsViewModel.leaveChannel(channel);
            loadChannels();
        });
    }

    private void getChannels(FilterObject filter) {
        client.queryChannels(new QueryChannelsRequest(filter, 0, 30, ChannelListViewModel.DEFAULT_SORT, 0, 0)).enqueue(result -> {
            n_channels = result.data().size();
        });
    }

    private void loadChannels() {
        Log.d(TAG, "initChannelList: " + type);
        Log.d(TAG, "initChannelList: User " + user.getId());

        FilterObject filter = Filters.and(
                Filters.eq("type", type),
                Filters.in("members", Collections.singletonList(user.getId()))
        );

        channelListView = binding.channelListView;

        ChannelListViewModelFactory factory = new ChannelListViewModelFactory(
                filter,
                ChannelListViewModel.DEFAULT_SORT,
                30
        );

        channelsViewModel = factory.create(ChannelListViewModel.class);

        ChannelListViewModelBinding.bind(channelsViewModel, channelListView, activity);
        getChannels(filter);
    }

    private void setupChannelList() {
        ChannelsEmptyViewBinding bindingEmptyView = ChannelsEmptyViewBinding.inflate(activity.getLayoutInflater());

        channelListView.setEmptyStateView(bindingEmptyView.getRoot(), new FrameLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT));


        channelListView.setChannelDeleteClickListener(channel -> {
            ConfirmationDialogFragment dialog = ConfirmationDialogFragment.newDeleteChannelInstance(activity);
            dialog.confirmClickListener = () -> channelsViewModel.deleteChannel(channel);

            dialog.show(activity.getSupportFragmentManager(), null);
        });
    }

    @SuppressLint("UnsafeOptInUsageError")
    private void onClickSearch(){
        //searchInputLayout = binding.searchViewInputLayout;
        searchInputView = binding.searchView;
        searchResultListView = binding.searchViewResults;

        /*searchInputLayout.setEndIconVisible(false);
        searchInputLayout.setEndIconOnClickListener(c -> {
            closeSearchList();
        });*/

        // Instantiate the ViewModel
        searchViewModel = new ViewModelProvider(activity).get(SearchViewModel.class);

        // Bind the ViewModel with SearchResultListView
        SearchViewModelBinding.bind(searchViewModel, searchResultListView, activity);
        searchInputView.setDebouncedInputChangedListener(query -> {
            if (query.isEmpty()) {
                channelListView.setVisibility(View.VISIBLE);
                searchResultListView.setVisibility(View.GONE);
            }
        });

        searchInputView.setSearchStartedListener(query -> {
            Utils.hideSoftKeyboard(searchInputView);
            searchViewModel.setQuery(query);

            if(query.isEmpty())
                channelListView.setVisibility(View.VISIBLE);
            else {
                channelListView.setVisibility(View.GONE);
                searchResultListView.setVisibility(View.VISIBLE);
            }
        });


        searchResultListView.setSearchResultSelectedListener(message -> {
            client.queryChannel(message.getChannelInfo().getType(), message.getChannelInfo().getId(), new QueryChannelRequest()).enqueue(result -> {
                activity.startActivity(ChatActivity.newIntent(activity, result.data()));
            });

        });
    }

    private void onClickChat(){
        channelListView.setChannelItemClickListener(
                channel -> activity.startActivity(ChatActivity.newIntent(activity, channel))
        );
    }
}

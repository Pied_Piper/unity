package it.piedpiper.unity.viewmodel.login;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;

import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import it.piedpiper.unity.callback.CallBack;
import it.piedpiper.unity.databinding.FragmentSetUserBinding;
import it.piedpiper.unity.repository.IUserRepository;
import it.piedpiper.unity.repository.impl.UserRepository;
import it.piedpiper.unity.ui.login.LoginActivity;

public class SetUserViewModel {
    private final FragmentSetUserBinding binding;
    private final LoginActivity activity;
    private final IUserRepository userRepository;

    private TextView mWelcomeTv;
    private TextView mUsernameTv;

    private ImageView mUserPhotoImg;
    private ImageButton mSelectPhoto;
    private Button mRemovePhoto;
    private TextInputLayout mUsernameInputLayout;
    private TextInputEditText mUsername;
    private Button mNextButton;

    private  ActivityResultLauncher<Intent> launchActivity;

    private boolean check;


    public SetUserViewModel(LoginActivity loginActivity, FragmentSetUserBinding binding) {
        this.binding = binding;
        this.activity = loginActivity;
        userRepository = new UserRepository(loginActivity);
    }

    public void initUI(){
        mWelcomeTv = binding.tvWelcome;
        mUsernameTv = binding.tvUsername;

        mUserPhotoImg = binding.imageUser;
        mSelectPhoto = binding.selectPhoto;
        mRemovePhoto = binding.btnChangePhoto;

        mUsernameInputLayout = binding.usernameInputLayout;
        mUsername = binding.username;

        mNextButton = binding.nextButton;
    }

    public void setLaunchActivity() {
        launchActivity = activity.registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(), result -> {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        Intent data = result.getData();
                        assert data != null;
                        Uri uri = data.getData();
                        try {
                            Bitmap bitmap = MediaStore.Images.Media.getBitmap(activity.getContentResolver(), uri);
                            mRemovePhoto.setVisibility(View.VISIBLE);
                            mSelectPhoto.setVisibility(View.GONE);
                            mUserPhotoImg.setImageBitmap(Bitmap.createScaledBitmap(bitmap, mUserPhotoImg.getWidth(), mUserPhotoImg.getHeight(), false));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    public void onTextChange(){
        mUsername.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(mUsernameInputLayout.getError() != null)
                    mUsernameInputLayout.setError(null);
                if(mUsername.getText().toString().length() == 0)
                    mUsernameTv.setText("");
                else {
                    mWelcomeTv.setText("Welcome,");
                    mUsernameTv.setText(mUsername.getText().toString() + " !");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void setOnClickListeners(){
        onClickChangePhoto();
        onClickNextBtn();
    }

    private void onClickChangePhoto(){
        mSelectPhoto.setOnClickListener(c -> {
            if(activity.checkPermission()) {
                ImagePicker.Companion.with(activity)
                        .crop()	    			//Crop image(Optional), Check Customization for more option
                        .compress(1024)//Final image size will be less than 1 MB(Optional)
                        .maxResultSize(1080, 1080)
                        .createIntent(intent -> {
                            launchActivity.launch(intent);
                            return null;
                        });
            }
            else
                activity.requestPermission();
        });

        mRemovePhoto.setOnClickListener(c -> {
            mUserPhotoImg.setImageDrawable(null);
            mSelectPhoto.setVisibility(View.VISIBLE);
            mRemovePhoto.setVisibility(View.GONE);
        });
    }

    private void onClickNextBtn(){
        binding.logout.setOnClickListener(c -> {
            userRepository.logout(new CallBack() {
                @Override
                public void onSuccess(Object object) {

                }

                @Override
                public void onError(Object object) {

                }
            });
        });

        mNextButton.setOnClickListener(c -> {
            String username = mUsername.getText().toString();
            updateUser(username);
        });
    }

    private void updateUser(String username) {
        if (username.isEmpty()) {
            mUsername.setError("Insert your username!");
        } else {
            userRepository.readUserByUsername(username, new CallBack() {
                @Override
                public void onSuccess(Object object) {
                    if(object != null) {
                        mUsernameInputLayout.setError("Username not valid, please insert another username.");
                    }
                    else{
                        Bundle args = new Bundle();
                        if(mUserPhotoImg.getDrawable() != null) {
                            mUserPhotoImg.setDrawingCacheEnabled(true);
                            mUserPhotoImg.buildDrawingCache();
                            Bitmap bitmap = ((BitmapDrawable) mUserPhotoImg.getDrawable()).getBitmap();
                            ByteArrayOutputStream baos = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                            byte[] data = baos.toByteArray();

                            args.putByteArray("Image", data);
                        }
                        args.putString("Username", username);
                        activity.replaceFragment("University", args);
                    }
                }

                @Override
                public void onError(Object object) {
                    mUsernameInputLayout.setError("Username not set, please insert your email.");
                }
            });
        }
    }
}

package it.piedpiper.unity.viewmodel.main;

import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.SimpleItemAnimator;

import com.getstream.sdk.chat.utils.Utils;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.slider.Slider;
import com.google.android.material.textfield.TextInputLayout;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.Objects;

import io.getstream.chat.android.client.ChatClient;
import it.piedpiper.unity.R;
import it.piedpiper.unity.adapter.AdapterActions;
import it.piedpiper.unity.adapter.LessonAdapter;
import it.piedpiper.unity.callback.CallBack;
import it.piedpiper.unity.databinding.FragmentLessonsBinding;
import it.piedpiper.unity.model.Lesson;
import it.piedpiper.unity.repository.ILessonRepository;
import it.piedpiper.unity.repository.impl.LessonRepository;
import it.piedpiper.unity.ui.main.MainActivity;
import it.piedpiper.unity.utils.Animations;
import it.piedpiper.unity.utils.Constant;

public class LessonsViewModel {
    private final FragmentLessonsBinding binding;
    private final MainActivity activity;
    private final ILessonRepository lessonRepository;

    private TextInputLayout mSubjectInputLayout, mPlaceInputLayout;
    private AutoCompleteTextView mSubject, mPlace;

    private TextView mPrice;

    private Slider mSlider;
    private Button mSearch;

    private TextView mNumberLessons;

    private BottomSheetBehavior sheetBehavior;
    private ImageView mFilterIcon;

    private ArrayList<String> subjectList, places;

    public LessonsViewModel(MainActivity activity, FragmentLessonsBinding binding){
        this.activity = activity;
        this.binding = binding;
        this.lessonRepository = new LessonRepository(activity);
    }

    public void initUI(){
        mSubjectInputLayout = binding.subjectInputLayout;
        mPlaceInputLayout = binding.placeInputLayout;
        mSubject = binding.subject;
        mPlace = binding.place;

        mPrice = binding.price;
        mSlider = binding.slider;
        mSearch = binding.searchBtn;

        mNumberLessons = binding.lessonsNumber;

        mFilterIcon = binding.filterIcon;

        subjectList = new ArrayList<>();
        places = new ArrayList<>();

        ((SimpleItemAnimator) Objects.requireNonNull(binding.recyclerView.getItemAnimator())).setSupportsChangeAnimations(false);

        getLessons();
    }

    private void getLessons(){
        lessonRepository.readAllLessons(new CallBack() {
            @Override
            public void onSuccess(Object object) {
                List<Lesson> lessons = (List<Lesson>) object;

                if(lessons == null)
                    lessons = new ArrayList<>();

                for(Lesson lesson : lessons){
                    if(!subjectList.contains(lesson.getSubject()))
                        subjectList.add(lesson.getSubject());

                    if(!places.contains(lesson.getPlace()))
                        places.add(lesson.getPlace());
                }

                AdapterActions.setAdapter(activity, mSubject, subjectList);
                AdapterActions.setAdapter(activity, mPlace, places);
            }

            @Override
            public void onError(Object object) {

            }
        });
    }

    public void setOnClickListeners(){
        mPlace.setOnClickListener(c -> {
            getLessons();
        });

        mSubject.setOnClickListener(c -> {
            getLessons();
        });


        mSearch.setOnClickListener(c -> {
            String subject = mSubject.getText().toString();
            String place = mPlace.getText().toString();
            String price = mPrice.getText().toString().substring(1);

            Utils.hideSoftKeyboard(activity);

            if(checkFields()){
                if (isPriceSetted()) {
                    lessonRepository.getLessonsBySubjectPlacePrice(subject, place, price, new CallBack() {
                        @Override
                        public void onSuccess(Object object) {
                            List<Lesson> lessons = (List<Lesson>) object;

                            if(lessons == null)
                                lessons = new ArrayList<>();

                            for(int i = 0; i < lessons.size(); i++){
                                if(Objects.requireNonNull(ChatClient.instance().getCurrentUser()).getId().equals(lessons.get(i).getUserID()))
                                    lessons.remove(lessons.get(i));
                            }

                            mNumberLessons.setText("" + lessons.size());

                            LessonAdapter lessonAdapter = new LessonAdapter(lessons, activity);
                            binding.setLessonAdapter(lessonAdapter);
                            toggleFilters(1500);
                        }

                        @Override
                        public void onError(Object object) {

                        }
                    });
                }
                else{
                    lessonRepository.getLessonsBySubjectPlace(subject, place, new CallBack() {
                        @Override
                        public void onSuccess(Object object) {
                            List<Lesson> lessons = (List<Lesson>) object;
                            mNumberLessons.setText(lessons.size());

                            LessonAdapter lessonAdapter = new LessonAdapter(lessons, activity);
                            binding.setLessonAdapter(lessonAdapter);
                            toggleFilters(1500);
                        }

                        @Override
                        public void onError(Object object) {

                        }
                    });
                }
            }
        });
    }

    public void setOnChangeTextListeners(){
        mSubject.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(mSubjectInputLayout.getError() != null)
                    mSubjectInputLayout.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mPlace.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(mPlaceInputLayout.getError() != null)
                    mPlaceInputLayout.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private boolean checkFields(){
        String subject = mSubject.getText().toString();
        String place = mPlace.getText().toString();

        if(subject.isEmpty()){
            mSubjectInputLayout.setError("Insert the lesson' s subject!");
            return false;
        }
        if(place.isEmpty()){
            mPlace.setError("Insert the lesson' s place!");
            return false;
        }
        else{
            boolean check = false;
            for(String p : places){
                if(mPlace.getText().toString().equals(p)) {
                    check = true;
                    break;
                }
            }
            if(!check) {
                mPlace.setError("Insert a valid lesson' s place!");
                return false;
            }
        }
        return true;
    }

    private boolean isPriceSetted(){
        return !mPrice.getText().toString().isEmpty();

    }

    public void setBottomSheet(){
        LinearLayoutCompat contentLayout = binding.contentLayout;
        sheetBehavior = BottomSheetBehavior.from(contentLayout);
        sheetBehavior.setFitToContents(false);
        sheetBehavior.setHideable(false);//prevents the boottom sheet from completely hiding off the screen
        sheetBehavior.setState(BottomSheetBehavior.STATE_DRAGGING);//initially state to fully expanded

        View filterIcon = binding.filterIcon;
        filterIcon.setOnClickListener(v -> toggleFilters());
    }

    private void toggleFilters(){
        Utils.hideSoftKeyboard(activity);
        if(sheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED){
            mFilterIcon.setImageResource(R.drawable.ic_baseline_filter_list_24);
            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
        else {
            sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            mFilterIcon.setImageResource(R.drawable.ic_baseline_filter_list_off_24);
        }
    }

    private void toggleFilters(int time){
        Utils.hideSoftKeyboard(activity);
        Handler handler = new Handler();

        Runnable runnable = () -> {
            if(sheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED){
                mFilterIcon.setImageResource(R.drawable.ic_baseline_filter_list_24);
                sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
            else {
                mFilterIcon.setImageResource(R.drawable.ic_baseline_filter_list_off_24);
                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        };

        handler.postDelayed(runnable, time);
    }

    public void setSlider(){
        NumberFormat formatINR = NumberFormat.getCurrencyInstance();
        formatINR.setMaximumFractionDigits(0);
        formatINR.setCurrency(Currency.getInstance("EUR"));

        mSlider.setLabelFormatter(formatINR::format);

        mSlider.addOnChangeListener((slider, value, fromUser) -> {
            mPrice.setText(formatINR.format(value) + " /h");
        });
    }
}

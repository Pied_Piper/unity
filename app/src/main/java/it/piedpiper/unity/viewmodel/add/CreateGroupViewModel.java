package it.piedpiper.unity.viewmodel.add;

import static it.piedpiper.unity.firebase.FirebaseConstants.GROUPS_COLLECTION;
import static it.piedpiper.unity.firebase.FirebaseDatabaseReference.DATABASE;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;

import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.firestore.CollectionReference;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import io.getstream.chat.android.client.ChatClient;
import io.getstream.chat.android.client.models.Channel;
import it.piedpiper.unity.callback.CallBack;
import it.piedpiper.unity.databinding.FragmentCreateGroupBinding;
import it.piedpiper.unity.model.Group;
import it.piedpiper.unity.model.User;
import it.piedpiper.unity.repository.IChannelRepository;
import it.piedpiper.unity.repository.IGroupRepository;
import it.piedpiper.unity.repository.IStorageRepository;
import it.piedpiper.unity.repository.IUserRepository;
import it.piedpiper.unity.repository.impl.ChannelRepository;
import it.piedpiper.unity.repository.impl.GroupRepository;
import it.piedpiper.unity.repository.impl.StorageRepository;
import it.piedpiper.unity.repository.impl.UserRepository;
import it.piedpiper.unity.ui.add.AddActivity;
import it.piedpiper.unity.utils.Constant;
import it.piedpiper.unity.viewmodel.UserViewModel;

public class CreateGroupViewModel {
    private static final String TAG = "CreateGroupViewModel";
    private final FragmentCreateGroupBinding binding;
    private final AddActivity activity;
    private final IGroupRepository groupRepository;
    private final IStorageRepository storageRepository;
    private final IChannelRepository channelRepository;
    private final IUserRepository userRepository;

    private String university;
    private String course;


    private TextInputLayout mNameInputLayout, mSubjectInputLayout;
    private TextInputEditText mName, mSubject;

    private ImageView mPhoto;
    private ImageButton mSelectPhoto;
    private FloatingActionButton mRemovePhoto;
    private Button mFinish;

    public CreateGroupViewModel(AddActivity activity, FragmentCreateGroupBinding binding) {
        this.activity = activity;
        this.binding = binding;
        this.groupRepository = new GroupRepository(activity);
        this.storageRepository = new StorageRepository(activity);
        this.channelRepository = new ChannelRepository();
        this.userRepository = new UserRepository(activity);
    }

    public void initUI() {
        mNameInputLayout = binding.groupNameInputLayout;
        mName = binding.groupName;
        mSubjectInputLayout = binding.subjectInputLayout;
        mSubject = binding.subject;

        mPhoto = binding.imageGroup;
        mSelectPhoto = binding.selectPhoto;
        mRemovePhoto = binding.btnChangePhoto;
        mFinish = binding.btnCreateGroup;

        getUser();
    }

    private void getUser() {
        userRepository.readUserDao(new CallBack() {
            @Override
            public void onSuccess(Object object) {
                User user = (User) object;
                university = user.getUserUniversity();
                course = user.getUserCourse();
            }

            @Override
            public void onError(Object object) {

            }
        });
    }

    public void setOnClickListeners() {
        onClickChangePhoto();
        onClickFinish();
    }

    private void onClickChangePhoto() {
        ActivityResultLauncher<Intent> launchActivity = activity.registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(), result -> {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        Intent data = result.getData();
                        assert data != null;
                        Uri uri = data.getData();
                        try {
                            Bitmap bitmap = MediaStore.Images.Media.getBitmap(activity.getContentResolver(), uri);

                            mSelectPhoto.setVisibility(View.GONE);
                            mRemovePhoto.setVisibility(View.VISIBLE);

                            mPhoto.setImageBitmap(Bitmap.createScaledBitmap(bitmap, mPhoto.getWidth(), mPhoto.getHeight(), false));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });

        mSelectPhoto.setOnClickListener(c -> {
            if (activity.checkPermission()) {
                ImagePicker.Companion.with(activity)
                        .crop()                    //Crop image(Optional), Check Customization for more option
                        .compress(1024)//Final image size will be less than 1 MB(Optional)
                        .maxResultSize(1080, 1080)
                        .createIntent(intent -> {
                            launchActivity.launch(intent);
                            return null;
                        });
            } else
                activity.requestPermission();
        });

        mRemovePhoto.setOnClickListener(c -> {
            mPhoto.setImageDrawable(null);
            mSelectPhoto.setVisibility(View.VISIBLE);
            mRemovePhoto.setVisibility(View.GONE);
        });
    }

    private void onClickFinish() {
        mFinish.setOnClickListener(c -> {
            if (checkFields()) {
                CollectionReference groupsCollectionReference = DATABASE.collection(GROUPS_COLLECTION);
                String id = groupsCollectionReference.document().getId();
                Log.d("TAG", "onClickFinish: " + id);


                if (mPhoto.getDrawable() != null) {
                    storageRepository.putImage(id, mPhoto, new CallBack() {
                        @Override
                        public void onSuccess(Object object) {
                            createGroup(object);
                        }

                        @Override
                        public void onError(Object object) {

                        }
                    });
                } else {
                    createGroup(null);
                }
            }
        });
    }

    private void createGroup(Object object) {
        String name = mName.getText().toString();
        String subject = mSubject.getText().toString();
        String photoUrl = "";

        if (object != null)
            photoUrl = ((Uri) object).toString();


        List<String> members = new ArrayList<>();

        //SharedPreferences sharedPref = activity.getSharedPreferences(Constant.SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
        //String userID = sharedPref.getString(Constant.USER_ID, null);
        members.add(Objects.requireNonNull(ChatClient.instance().getCurrentUser()).getId());
        Group group = new Group(name, university, course, subject, photoUrl, members);

        groupRepository.createGroup(group, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                Channel channel = new Channel();

                Map<String, Object> extraData = new HashMap<>();

                extraData.put("name", group.getName());
                extraData.put("createdAt", Calendar.getInstance().getTime());
                extraData.put("image", group.getPhotoUrl());
                extraData.put("members", group.getMembers());

                channel.setId(((Group) object).getId());
                channel.setType(Constant.GROUP.toLowerCase());
                channel.setExtraData(extraData);

                channelRepository.createChannel(channel, members, new CallBack() {
                    @Override
                    public void onSuccess(Object object) {
                        Runnable runnable = activity::finish;
                        Handler handler = new Handler();
                        handler.postDelayed(runnable, 1500);
                    }

                    @Override
                    public void onError(Object object) {

                    }
                });
            }

            @Override
            public void onError(Object object) {

            }
        });
    }


    private boolean checkFields() {
        if (mName.getText().toString().isEmpty()) {
            mNameInputLayout.setError("Insert a group' s name!");
            return false;
        }

        if (mSubject.getText().toString().isEmpty()) {
            mSubjectInputLayout.setError("Insert the group' s subject!");
            return false;
        }
        return true;
    }

    public void setNameListener() {
        mName.setOnClickListener(c -> {
            if (mNameInputLayout.getError() != null)
                mNameInputLayout.setError(null);
        });

        mName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mNameInputLayout.getError() != null)
                    mNameInputLayout.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
}

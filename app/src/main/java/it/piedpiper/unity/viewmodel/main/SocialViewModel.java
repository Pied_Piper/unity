package it.piedpiper.unity.viewmodel.main;

import androidx.fragment.app.FragmentManager;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabLayout;

import io.getstream.chat.android.client.ChatClient;
import io.getstream.chat.android.client.models.User;
import it.piedpiper.unity.adapter.ViewPagerChatAdapter;
import it.piedpiper.unity.databinding.FragmentSocialBinding;
import it.piedpiper.unity.repository.IUserRepository;
import it.piedpiper.unity.repository.impl.UserRepository;
import it.piedpiper.unity.ui.main.MainActivity;

public class SocialViewModel {
    private static final String TAG = "SocialViewModel";
    private final MainActivity activity;
    private final FragmentSocialBinding binding;
    private ViewPager2 viewPager2;

    private TabLayout mTabLayout;

    private int selected;


    public SocialViewModel(MainActivity activity, FragmentSocialBinding binding) {
        this.activity = activity;
        this.binding = binding;
    }

    public void initUI(){
        mTabLayout = binding.tabsLayout;
        viewPager2 = binding.viewPager;

        setTabItemListener();
        setViewPager();

        selected = 0;
    }

    public void initClient(){
        ChatClient client = ChatClient.instance();
        User user = client.getCurrentUser();
    }


    public void setViewPager() {
        ViewPagerChatAdapter viewPagerChatAdapter = new ViewPagerChatAdapter(activity);

        viewPager2.setAdapter(viewPagerChatAdapter);

        viewPager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                mTabLayout.selectTab(mTabLayout.getTabAt(position));
            }
        });

        viewPager2.setUserInputEnabled(false);

    }

    public void setTabItemListener(){
        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager2.setCurrentItem(tab.getPosition());

                selected = tab.getPosition();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
}

package it.piedpiper.unity.viewmodel.preferences;

import android.util.Log;

import androidx.appcompat.widget.Toolbar;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

import com.google.firebase.auth.FirebaseAuth;
import com.r0adkll.slidr.Slidr;
import com.r0adkll.slidr.model.SlidrInterface;

import java.util.Objects;

import it.piedpiper.unity.R;
import it.piedpiper.unity.databinding.ActivityPreferencesBinding;
import it.piedpiper.unity.ui.preferences.PreferencesActivity;
import it.piedpiper.unity.utils.Constant;

public class PreferencesActivityViewModel {
    private static final String TAG = "PreferencesActivityViewModel";
    private final ActivityPreferencesBinding binding;
    private final PreferencesActivity activity;

    private SlidrInterface slidr;

    //Toolbar
    private Toolbar toolbar;
    private NavHostFragment navHostFragment;
    private NavController navController;

    public PreferencesActivityViewModel(PreferencesActivity activity, ActivityPreferencesBinding binding){
        this.activity = activity;
        this.binding = binding;
    }

    public void initUI(){
        slidr = Slidr.attach(activity);

        navHostFragment = (NavHostFragment) activity.getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);
        assert navHostFragment != null;
        navController = navHostFragment.getNavController();

        //toolbar = findViewById(R.id.toolbar);
        toolbar = binding.toolbar;
        activity.setSupportActionBar(toolbar);
        Objects.requireNonNull(activity.getSupportActionBar()).setHomeButtonEnabled(true);
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_round_arrow_back_ios_new_24);

        toolbar.setNavigationOnClickListener(v -> {
            activity.finish();
        });

        loadFirstFragment();
    }


    //Load first fragment based on the extra string passed by the intent
    private void loadFirstFragment(){
        String fragmentName = activity.getIntent().getStringExtra(Constant.FRAGMENT_STRING);
        Log.d(TAG, "loadFirstFragment: " + fragmentName);
        switch (fragmentName){
            case Constant.PROFILE_STRING:
                navController.navigate(Constant.PROFILE);
                toolbar.setTitle("Profile");
                break;
            case Constant.EMAIL_STRING:
                navController.navigate(Constant.EMAIL);
                toolbar.setTitle("Email");
                break;
            case Constant.PASSWORD_STRING:
                navController.navigate(Constant.PASSWORD);
                toolbar.setTitle("Password");
                break;
            case Constant.SETTINGS_STRING:
                navController.navigate(Constant.SETTINGS);
                toolbar.setTitle("Settings");
                break;
            case Constant.EVENTS_STRING:
                navController.navigate(Constant.EVENTS);
                toolbar.setTitle("Event");
                break;
            case Constant.CALENDAR_STRING:
                navController.navigate(Constant.CALENDAR);
                toolbar.setTitle("Calendar");
                break;
            case Constant.ABOUT_STRING:
                navController.navigate(Constant.ABOUT);
                toolbar.setTitle("About");
                break;
            case Constant.HELP_STRING:
                navController.navigate(Constant.HELP);
                toolbar.setTitle("Help & Feedback");
                break;
            case Constant.CONTACTS_STRING:
                navController.navigate(Constant.CONTACTS);
                toolbar.setTitle("Contacts");
                break;
            case Constant.FAQ_STRING:
                navController.navigate(Constant.FAQ);
                toolbar.setTitle("FAQ");
                break;
        }
    }
}

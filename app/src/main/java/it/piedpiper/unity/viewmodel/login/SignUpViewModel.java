package it.piedpiper.unity.viewmodel.login;

import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.widget.Toast;

import androidx.navigation.Navigation;

import com.getstream.sdk.chat.utils.Utils;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseUser;

import it.piedpiper.unity.R;
import it.piedpiper.unity.callback.CallBack;
import it.piedpiper.unity.databinding.FragmentSignUpBinding;
import it.piedpiper.unity.firebase.FirebaseDatabaseReference;
import it.piedpiper.unity.model.User;
import it.piedpiper.unity.repository.IUserRepository;
import it.piedpiper.unity.repository.impl.UserRepository;
import it.piedpiper.unity.ui.login.LoginActivity;

public class SignUpViewModel {
    private static final String TAG = "SignUpViewModel";
    private final FragmentSignUpBinding binding;
    private final LoginActivity activity;
    private final IUserRepository userRepository;

    private TextInputLayout mEmailInputLayout;
    private TextInputLayout mPasswordInputLayout;
    private TextInputLayout mConfirmPasswordInputLayout;
    private TextInputEditText mEmail;
    private TextInputEditText mPassword;
    private TextInputEditText mConfirmPassword;

    public SignUpViewModel(LoginActivity loginActivity, FragmentSignUpBinding binding) {
        this.binding = binding;
        this.activity = loginActivity;
        userRepository = new UserRepository(loginActivity);
    }

    public void initUI() {
        mEmailInputLayout = binding.emailInputLayout;
        mPasswordInputLayout = binding.passwordInputLayout;
        mConfirmPasswordInputLayout = binding.confirmPasswordInputLayout;
        mEmail = binding.email;
        mPassword = binding.password;
        mConfirmPassword = binding.confirmPassword;
    }

    private boolean checkFields() {

        String email = mEmail.getText().toString().toLowerCase();
        String password = mPassword.getText().toString();
        String confirmPassword = mConfirmPassword.getText().toString();

        if(!isValidEmail(email)) {
            mEmailInputLayout.setErrorEnabled(true);
            mEmailInputLayout.setError("Insert a valid email!");
            return false;
        }

        //EMPTY FIELDS
        return !isEmptyFields(email, password, confirmPassword)
                && isValidEmail(email)
                && checkPassword(password, confirmPassword);
    }

    private boolean isEmptyFields(String email, String password, String confirmPassword) {
        if (TextUtils.isEmpty(email)) {
            mEmailInputLayout.setErrorEnabled(true);
            mEmailInputLayout.setError("Insert a valid email!");
            return true;
        }
        if (TextUtils.isEmpty(password)) {
            mPasswordInputLayout.setErrorEnabled(true);
            mPasswordInputLayout.setError("Insert a password!");
            return true;
        }
        if (TextUtils.isEmpty(confirmPassword)) {
            mConfirmPasswordInputLayout.setErrorEnabled(true);
            mConfirmPasswordInputLayout.setError("Re-insert the password!");
            return true;
        }
        return false;
    }

    private boolean isValidEmail(CharSequence email) {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private boolean checkPassword(String password, String confirmPassword){
        if(password.length() < 5){
            mPasswordInputLayout.setError("Password must be > 5");
            return false;
        }
        else
            return password.equals(confirmPassword);
    }

    public void onChangeCheckFields(){
        int checkIcon = androidx.mediarouter.R.drawable.ic_checked_checkbox;
        ColorStateList checkIconColor = ColorStateList.valueOf(activity.getResources().getColor(R.color.pastel_green_200, activity.getTheme()));
        Drawable visibilityIcon = mPasswordInputLayout.getEndIconDrawable();
        ColorStateList visibilityIconColor = ColorStateList.valueOf(activity.getResources().getColor(R.color.light_grey, activity.getTheme()));
        Drawable visibilityIconCp = mConfirmPasswordInputLayout.getEndIconDrawable();

        mEmailInputLayout.setEndIconVisible(false);
        mEmailInputLayout.setEndIconDrawable(checkIcon);
        mEmailInputLayout.setEndIconTintList(checkIconColor);

        mEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(mEmailInputLayout.getError() != null){
                    mEmailInputLayout.setErrorEnabled(false);
                    mEmailInputLayout.setError(null);
                }
                mEmailInputLayout.setEndIconVisible(isValidEmail(s));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(mPasswordInputLayout.getError() != null){
                    mPasswordInputLayout.setErrorEnabled(false);
                    mPasswordInputLayout.setError(null);
                }
                if(mPassword.getText().toString().length() <= 5) {
                    //mPasswordInputLayout.setError("Password must be > 5");
                    mPasswordInputLayout.setHelperText("Password must be greater than 5");
                }
                else{
                    mPasswordInputLayout.setHelperText(null);
                    if(mPassword.getText().toString().equals(mConfirmPassword.getText().toString())){
                        mPasswordInputLayout.setEndIconDrawable(checkIcon);
                        mConfirmPasswordInputLayout.setEndIconDrawable(checkIcon);
                        mPasswordInputLayout.setEndIconTintList(checkIconColor);
                        mConfirmPasswordInputLayout.setEndIconTintList(checkIconColor);
                    }
                    else{
                        mPasswordInputLayout.setEndIconDrawable(visibilityIcon);
                        mConfirmPasswordInputLayout.setEndIconDrawable(visibilityIconCp);
                        mPasswordInputLayout.setEndIconTintList(visibilityIconColor);
                        mConfirmPasswordInputLayout.setEndIconTintList(visibilityIconColor);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mConfirmPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(mConfirmPasswordInputLayout.getError() != null){
                    mConfirmPasswordInputLayout.setErrorEnabled(false);
                    mConfirmPasswordInputLayout.setError(null);
                }

                if(mPassword.getText().toString().length() > 5) {
                    if(mPassword.getText().toString().equals(mConfirmPassword.getText().toString())){
                        mPasswordInputLayout.setEndIconDrawable(checkIcon);
                        mConfirmPasswordInputLayout.setEndIconDrawable(checkIcon);
                        mPasswordInputLayout.setEndIconTintList(checkIconColor);
                        mConfirmPasswordInputLayout.setEndIconTintList(checkIconColor);
                    }
                    else{
                        mPasswordInputLayout.setEndIconDrawable(visibilityIcon);
                        mConfirmPasswordInputLayout.setEndIconDrawable(visibilityIconCp);
                        mPasswordInputLayout.setEndIconTintList(visibilityIconColor);
                        mConfirmPasswordInputLayout.setEndIconTintList(visibilityIconColor);
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    //DATABASE METHODS
    private void createUser(String email, String password) {
        userRepository.createUserWithEmail(email, password).observe(activity, authenticationResponse -> {
            if (authenticationResponse != null) {
                if (authenticationResponse.isSuccess()) {
                    createUserDb(email);
                } else {
                    binding.signup.setClickable(true);
                    activity.updateUIForFailure(authenticationResponse.getMessage());
                }
            }
        });
    }

    private void createUserDb(String email) {
        User user = new User();
        user.setEmail(email.toLowerCase());
        userRepository.createUser(user, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                /*userRepository.signInWithEmail(email, password, new CallBack() {
                    @Override
                    public void onSuccess(Object object) {
                        changeFragment();
                    }

                    @Override
                    public void onError(Object object) {

                    }
                });*/
                changeFragment();
            }

            @Override
            public void onError(Object object) {

            }
        });
    }


    private void changeFragment() {
        Log.d(TAG, "changeFragment: Verify");
        Handler handler = new Handler();
        handler.postDelayed(() -> activity.replaceFragment("Verify"), 3000);
    }

    private void setLoginListener() {
        binding.login.setOnClickListener(v -> {
            activity.replaceFragment("Login");
        });
    }

    private void setSignUpClickListener() {

        binding.signup.setOnClickListener(v -> {
            Utils.hideSoftKeyboard(binding.signup);
            binding.signup.setClickable(false);
            if (checkFields()) {
                createUser(mEmail.getText().toString(), mPassword.getText().toString());
            }
            else
                binding.signup.setClickable(true);
        });
    }

    public void setOnClickListeners(){
        setLoginListener();
        setSignUpClickListener();
    }
}

package it.piedpiper.unity.viewmodel.on_boarding;

import it.piedpiper.unity.R;
import it.piedpiper.unity.adapter.OnBoardingViewPagerAdapter;
import it.piedpiper.unity.databinding.ActivityOnBoardingBinding;
import it.piedpiper.unity.ui.on_boarding.OnBoardingActivity;

public class OnBoardingViewModel {
    private final ActivityOnBoardingBinding binding;
    private final OnBoardingActivity activity;

    public OnBoardingViewModel(OnBoardingActivity activity, ActivityOnBoardingBinding binding) {
        this.binding = binding;
        this.activity = activity;
    }

    public void initUI() {
        binding.pager.setAdapter(new OnBoardingViewPagerAdapter(activity.getSupportFragmentManager(), 1));
        binding.pager.setButtonDrawable(R.drawable.shape);
    }
}

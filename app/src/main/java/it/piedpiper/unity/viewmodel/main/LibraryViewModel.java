package it.piedpiper.unity.viewmodel.main;

import android.annotation.SuppressLint;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.getstream.sdk.chat.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import it.piedpiper.unity.R;
import it.piedpiper.unity.adapter.AdapterActions;
import it.piedpiper.unity.adapter.FileAdapter;
import it.piedpiper.unity.callback.CallBack;
import it.piedpiper.unity.databinding.FragmentLibraryBinding;
import it.piedpiper.unity.model.FilePdf;
import it.piedpiper.unity.repository.IStorageRepository;
import it.piedpiper.unity.repository.impl.StorageRepository;
import it.piedpiper.unity.ui.main.MainActivity;

public class LibraryViewModel {
    private static final String TAG = "LibraryViewModel";
    private MainActivity activity;
    private FragmentLibraryBinding binding;
    private IStorageRepository storageRepository;

    private ArrayList<String> categories;
    private List<FilePdf> files;

    private FileAdapter fileAdapter;

    private final Drawable icon;
    private final ColorDrawable background;


    public LibraryViewModel(MainActivity activity, FragmentLibraryBinding binding) {
        this.activity = activity;
        this.binding = binding;

        storageRepository = new StorageRepository(activity);

        categories = new ArrayList<>();

        icon = ResourcesCompat.getDrawable(activity.getResources(), R.drawable.ic_delete, activity.getTheme());
        background = new ColorDrawable(Color.RED);

        files = new ArrayList<>();
        fileAdapter = new FileAdapter(activity, files);
    }

    public void initUI(){
        setCloseIconVisible(false);

        binding.categoryInputLayout.setEndIconOnClickListener(c -> {
            binding.category.setText("");

            setCloseIconVisible(false);

            Utils.hideSoftKeyboard(binding.categoryInputLayout);
            initLibrary();
        });
    }

    public void setCloseIconVisible(boolean check){
        if(check){
            binding.categoryInputLayout.setEndIconCheckable(true);
            binding.categoryInputLayout.setEndIconVisible(true);
        }
        else{
            binding.categoryInputLayout.setEndIconCheckable(false);
            binding.categoryInputLayout.setEndIconVisible(false);
        }
    }

    public void initCategories() {
        initCategory();

        storageRepository.getAllFilesPDF(new CallBack() {
            @Override
            public void onSuccess(Object object) {
                List<FilePdf> filePdfList = (List<FilePdf>) object;

                if(filePdfList == null)
                    filePdfList = new ArrayList<>();

                for(FilePdf filePdf : filePdfList){
                    boolean check = false;
                    for(String category : categories){
                        if (filePdf.getCategory().equals(category)) {
                            check = true;
                            break;
                        }
                    }
                    if(!check)
                        categories.add(filePdf.getCategory());
                }

                AdapterActions.setAdapter(activity, binding.category, categories);
            }

            @Override
            public void onError(Object object) {

            }
        });
    }

    public void initLibrary(){
        binding.recyclerViewSearch.setVisibility(View.GONE);

        storageRepository.getAllFilesPDFDao(new CallBack() {
            @Override
            public void onSuccess(Object object) {
                files = (List<FilePdf>) object;
                Log.d(TAG, "onSuccess: " + object);

                if(files == null)
                    files = new ArrayList<>();

                fileAdapter = new FileAdapter(activity, files);

                binding.recyclerViewSearch.setVisibility(View.GONE);
                //fileAdapter = new FileAdapter(activity, files);
                setAdapter();
            }

            @Override
            public void onError(Object object) {

            }
        });
    }

    public boolean checkCategory(){
        for(String category : categories){
            if(binding.category.getText().toString().equals(category))
                return true;
        }
        return false;
    }

    private void initCategory() {
        binding.category.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!binding.categoryInputLayout.isEndIconVisible()) {
                    setCloseIconVisible(true);
                    files = new ArrayList<>();
                    fileAdapter = new FileAdapter(activity, files);
                    setAdapter();
                    //binding.setAdapter();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void onClickSearch(){
        binding.category.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                if(checkCategory()) {
                    Utils.hideSoftKeyboard(binding.category);
                    storageRepository.getFilesPDF(v.getText().toString(), new CallBack() {
                        @Override
                        public void onSuccess(Object object) {
                            List<FilePdf> filePdfList = (List<FilePdf>) object;

                            if (filePdfList == null)
                                filePdfList = new ArrayList<>();

                            FileAdapter fileAdapter = new FileAdapter(activity, filePdfList);
                            binding.recyclerViewSearch.setVisibility(View.VISIBLE);
                            binding.setAdapter(fileAdapter);
                        }

                        @Override
                        public void onError(Object object) {

                        }
                    });
                }
                return true;
            }
            return false;
        });
    }

    @SuppressLint("NotifyDataSetChanged")
    private void setAdapter() {
        binding.setAdapter(fileAdapter);
        //ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new SwipeToDeleteCallBack(activity, fileAdapter));

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {

            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getLayoutPosition();

                Log.d(TAG, "onSwiped:" + position);

                fileAdapter.deleteItem(position);
            }

            @Override
            public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);

                View itemView = viewHolder.itemView;
                int backgroundCornerOffset = 20; //so background is behind the rounded corners of itemView

                int iconMargin = (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
                int iconTop = itemView.getTop() + (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
                int iconBottom = iconTop + icon.getIntrinsicHeight();

                if (dX > 0) { // Swiping to the right
                    int iconLeft = itemView.getLeft() + iconMargin + icon.getIntrinsicWidth();
                    int iconRight = itemView.getLeft() + iconMargin;
                    icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);

                    background.setBounds(itemView.getLeft(), itemView.getTop(),
                            itemView.getLeft() + ((int) dX) + backgroundCornerOffset, itemView.getBottom());
                } else if (dX < 0) { // Swiping to the left
                    int iconLeft = itemView.getRight() - iconMargin - icon.getIntrinsicWidth();
                    int iconRight = itemView.getRight() - iconMargin;
                    icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);

                    background.setBounds(itemView.getRight() + ((int) dX) - backgroundCornerOffset,
                            itemView.getTop(), itemView.getRight(), itemView.getBottom());
                } else { // view is unSwiped
                    background.setBounds(0, 0, 0, 0);
                }

                background.draw(c);
                icon.draw(c);
            }

        });

        activity.runOnUiThread(() -> {
            itemTouchHelper.attachToRecyclerView(binding.recyclerView);
        });
    }
}

package it.piedpiper.unity.viewmodel.login;

import android.content.Intent;
import android.os.Handler;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import it.piedpiper.unity.databinding.FragmentVerifyEmailBinding;
import it.piedpiper.unity.firebase.FirebaseDatabaseReference;
import it.piedpiper.unity.ui.login.LoginActivity;
import it.piedpiper.unity.utils.Animations;
import it.piedpiper.unity.utils.Constant;

public class VerifyEmailViewModel {
    private final FragmentVerifyEmailBinding binding;
    private final LoginActivity activity;


    private TextView blackBackgroundFilter;
    private RelativeLayout emailVerifiedLayout;
    private TextView resendEmail;
    private Runnable runnable;

    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

    public VerifyEmailViewModel(LoginActivity activity, FragmentVerifyEmailBinding binding) {
        this.binding = binding;
        this.activity = activity;
    }

    public void initUI(){
        blackBackgroundFilter = binding.blackBackgroundFilter;
        emailVerifiedLayout = binding.emailVerifiedLayout;
        resendEmail = binding.resendEmail;

        binding.openEmailBtn.setOnClickListener(c -> {
            Intent intent = activity.getPackageManager().getLaunchIntentForPackage("com.google.android.gm");
            if(intent != null)
                activity.startActivity(intent);
        });
    }

    public void setActions(){
        resendEmail.setOnClickListener(c -> {
            user.sendEmailVerification();
        });

        checkEmailVerified();
    }

    private void checkEmailVerified(){
        Handler handler = new Handler();
        runnable = () -> {
            user.reload();
            if (user.isEmailVerified()){
                openDialog();
                handler.postDelayed(() -> {
                    Intent intent = new Intent(activity, LoginActivity.class);
                    intent.putExtra(Constant.FRAGMENT_STRING, Constant.USER_STRING);
                    activity.startActivity(intent);
                    activity.finish();
                }, 2000);
            }
            else
                handler.postDelayed(runnable, 1000);
        };

        handler.postDelayed(runnable, 1000);
    }

    private void openDialog(){
        Animations.alphaView(emailVerifiedLayout, 0, 1);
        emailVerifiedLayout.setVisibility(View.VISIBLE);
        Animations.alphaView(blackBackgroundFilter, 0, 1);
        blackBackgroundFilter.setVisibility(View.VISIBLE);
    }
}

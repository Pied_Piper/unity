package it.piedpiper.unity.viewmodel.add;

import static it.piedpiper.unity.firebase.FirebaseConstants.GROUPS_COLLECTION;
import static it.piedpiper.unity.firebase.FirebaseDatabaseReference.DATABASE;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;

import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.firestore.CollectionReference;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.getstream.chat.android.client.models.Channel;
import it.piedpiper.unity.adapter.AdapterActions;
import it.piedpiper.unity.callback.CallBack;
import it.piedpiper.unity.databinding.FragmentCreateAssociationBinding;
import it.piedpiper.unity.model.Association;
import it.piedpiper.unity.model.University;
import it.piedpiper.unity.model.User;
import it.piedpiper.unity.repository.IAssociationRepository;
import it.piedpiper.unity.repository.IChannelRepository;
import it.piedpiper.unity.repository.IStorageRepository;
import it.piedpiper.unity.repository.IUniversityRepository;
import it.piedpiper.unity.repository.IUserRepository;
import it.piedpiper.unity.repository.impl.AssociationRepository;
import it.piedpiper.unity.repository.impl.ChannelRepository;
import it.piedpiper.unity.repository.impl.StorageRepository;
import it.piedpiper.unity.repository.impl.UniversityRepository;
import it.piedpiper.unity.repository.impl.UserRepository;
import it.piedpiper.unity.ui.add.AddActivity;
import it.piedpiper.unity.utils.Constant;
import it.piedpiper.unity.viewmodel.UserViewModel;

public class CreateAssociationViewModel {
    private final FragmentCreateAssociationBinding binding;
    private final AddActivity activity;
    private IUniversityRepository universityRepository;
    private IAssociationRepository associationRepository;
    private final IStorageRepository storageRepository;
    private final IChannelRepository channelRepository;
    private final IUserRepository userRepository;


    private TextInputLayout mNameInputLayout, mDescriptionInputLayout, mUniversityInputLayout;
    private TextInputEditText mName, mDescription;
    private AutoCompleteTextView mUniversity;

    private ImageView mPhoto;
    private ImageButton mSelectPhoto;
    private FloatingActionButton mRemovePhoto;
    private Button mFinish;

    private String university;

    public CreateAssociationViewModel(AddActivity activity, FragmentCreateAssociationBinding binding){
        this.activity = activity;
        this.binding = binding;
        this.universityRepository = new UniversityRepository();
        this.associationRepository = new AssociationRepository(activity);
        this.storageRepository = new StorageRepository(activity);
        this.channelRepository = new ChannelRepository();
        this.userRepository = new UserRepository(activity);
    }

    public void initUI(){
        mNameInputLayout = binding.associationNameInputLayout;
        mName = binding.associationName;
        mDescriptionInputLayout = binding.descriptionInputLayout;
        mDescription = binding.description;
        //mUniversityInputLayout = binding.universityInputLayout;
        //mUniversity = binding.university;

        mPhoto = binding.image;
        mSelectPhoto = binding.selectPhoto;
        mRemovePhoto = binding.btnChangePhoto;
        mFinish = binding.btnCreateGroup;

        getUser();
    }

    private void getUser(){
        UserViewModel userViewModel = new UserViewModel(activity.getApplication(), activity);

        binding.setUserViewModel(userViewModel);

        userRepository.readUserDao(new CallBack() {
            @Override
            public void onSuccess(Object object) {
                User user = (User) object;
                university = user.getUserUniversity();
            }

            @Override
            public void onError(Object object) {

            }
        });
    }

    public void setOnClickListeners(){
        onClickChangePhoto();
        onClickFinish();
    }

    private void onClickUniversity() {
        ArrayList<String> universities = new ArrayList<>();

        mUniversity.setOnClickListener(c -> {
            if(mUniversityInputLayout.getError() != null)
                mUniversityInputLayout.setError(null);
        });

        universityRepository.readAllUniversities(new CallBack() {
            @Override
            public void onSuccess(Object universitiesQuery) {
                Log.d("TAG", "onSuccess: " + universitiesQuery.toString());
                for(University university : (ArrayList<University>) universitiesQuery){
                    universities.add(university.getName());
                }
                AdapterActions.setAdapter(activity, mUniversity, universities);
            }

            @Override
            public void onError(Object object) {

            }
        });
    }

    public void setOnChangeTextListeners(){
        mName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mNameInputLayout.getError() != null)
                    mNameInputLayout.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mDescription.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mDescriptionInputLayout.getError() != null)
                    mDescriptionInputLayout.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


    private void onClickChangePhoto(){
        ActivityResultLauncher<Intent> launchActivity = activity.registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(), result -> {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        Intent data = result.getData();
                        assert data != null;
                        Uri uri = data.getData();
                        try {
                            Bitmap bitmap = MediaStore.Images.Media.getBitmap(activity.getContentResolver(), uri);

                            mRemovePhoto.setVisibility(View.VISIBLE);
                            mSelectPhoto.setVisibility(View.GONE);

                            mPhoto.setImageBitmap(Bitmap.createScaledBitmap(bitmap, mPhoto.getWidth(), mPhoto.getHeight(), false));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });

        mSelectPhoto.setOnClickListener(c -> {
            if(activity.checkPermission()) {
                ImagePicker.Companion.with(activity)
                        .crop()	    			//Crop image(Optional), Check Customization for more option
                        .compress(1024)//Final image size will be less than 1 MB(Optional)
                        .maxResultSize(1080, 1080)
                        .createIntent(intent -> {
                            launchActivity.launch(intent);
                            return null;
                        });
            }
            else
                activity.requestPermission();
        });

        mRemovePhoto.setOnClickListener(c -> {
            mPhoto.setImageDrawable(null);
            mRemovePhoto.setVisibility(View.GONE);
            mSelectPhoto.setVisibility(View.VISIBLE);
        });
    }

    private void onClickFinish(){
        mFinish.setOnClickListener(c -> {
            if(checkFields()){
                CollectionReference groupsCollectionReference = DATABASE.collection(GROUPS_COLLECTION);
                String id = groupsCollectionReference.document().getId();
                Log.d("TAG", "onClickFinish: " + id);

                if(mPhoto.getDrawable() != null) {
                    storageRepository.putImage(id, mPhoto, new CallBack() {
                        @Override
                        public void onSuccess(Object object) {
                            createAssociation(object);
                        }

                        @Override
                        public void onError(Object object) {

                        }
                    });
                }
                else{
                    createAssociation(null);
                }
            }
        });
    }

    private void createAssociation(Object object){
        String name = mName.getText().toString();
        //String university = mUniversity.getText().toString();
        String description = mDescription.getText().toString();
        String photoUrl = "";
        if(object != null)
            photoUrl = ((Uri) object).toString();

        List<String> members = new ArrayList<>();

        SharedPreferences sharedPref = activity.getSharedPreferences(Constant.SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
        String userID = sharedPref.getString(Constant.USER_ID, null);
        members.add(userID);
        Association association = new Association(name, description, university, photoUrl, members);

        associationRepository.createAssociation(association, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                Channel channel = new Channel();

                List<String> members = association.getMembers();

                Map<String, Object> extraData = new HashMap<>();

                extraData.put("name", association.getName());
                extraData.put("createdAt", Calendar.getInstance().getTime());
                extraData.put("image", association.getPhotoUrl());
                extraData.put("members", members);

                channel.setId(association.getId());
                channel.setType(Constant.ASSOCIATION.toLowerCase());
                channel.setExtraData(extraData);

                channelRepository.createChannel(channel, members, new CallBack() {
                    @Override
                    public void onSuccess(Object object) {
                        Runnable runnable = activity::finish;
                        Handler handler = new Handler();
                        handler.postDelayed(runnable, 1500);
                    }

                    @Override
                    public void onError(Object object) {

                    }
                });
            }

            @Override
            public void onError(Object object) {

            }
        });
    }

    private boolean checkFields(){
        if(mName.getText().toString().isEmpty()) {
            mNameInputLayout.setError("Insert an association' s name!");
            return false;
        }
        if(mDescription.getText().toString().isEmpty()){
            mDescriptionInputLayout.setError("Insert an association' s description!");
            return false;
        }

        return true;
    }
}

package it.piedpiper.unity.viewmodel.preferences;

import android.app.DatePickerDialog;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.getstream.sdk.chat.utils.Utils;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.textfield.TextInputLayout;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import it.piedpiper.unity.R;
import it.piedpiper.unity.adapter.AdapterActions;
import it.piedpiper.unity.adapter.EventAdapter;
import it.piedpiper.unity.callback.CallBack;
import it.piedpiper.unity.databinding.FragmentEventBinding;
import it.piedpiper.unity.model.Event;
import it.piedpiper.unity.model.University;
import it.piedpiper.unity.repository.IEventRepository;
import it.piedpiper.unity.repository.IUniversityRepository;
import it.piedpiper.unity.repository.impl.EventRepository;
import it.piedpiper.unity.repository.impl.UniversityRepository;
import it.piedpiper.unity.ui.preferences.PreferencesActivity;
import it.piedpiper.unity.utils.Constant;

public class EventsViewModel {
    private static final String TAG = "EventsViewModel";
    private final FragmentEventBinding binding;
    private final PreferencesActivity activity;
    private ArrayList<String> events;
    private final IEventRepository eventRepository;
    private final IUniversityRepository universityRepository;


    private static final int AUTOCOMPLETE_REQUEST_CODE = 1;

    private ArrayList<String> universities;
    private ArrayList<String> places;

    private TextInputLayout mPlaceInputLayout, mUniversityInputLayout, mUniversityCourseInputLayout;

    private AutoCompleteTextView mUniversity, mUniversityCourse, mPlace, mDate;

    private Button mBtnDate, mSearch;

    private BottomSheetBehavior bottomSheetBehavior;
    private ImageView mFilterIcon;

    private TextView mNumber;

    public EventsViewModel (PreferencesActivity activity, FragmentEventBinding binding) {
        this.activity = activity;
        this.binding = binding;
        this.eventRepository = new EventRepository(activity);
        this.universityRepository = new UniversityRepository();
    }

    public void initUI(){
        mPlaceInputLayout = binding.placeInputLayout;
        mUniversityInputLayout = binding.universityInputLayout;
        mUniversityCourseInputLayout = binding.universityCourseInputLayout;

        mUniversity = binding.university;
        mUniversityCourse = binding.course;
        mPlace = binding.place;
        mDate = binding.date;

        mBtnDate = binding.btnDate;
        mSearch = binding.search;

        bottomSheetBehavior = BottomSheetBehavior.from(binding.contentLayout);
        mFilterIcon = binding.filterIcon;

        mNumber = binding.eventsNumber;
        AdapterActions.setAdapter(activity, mUniversityCourse, Constant.getCourses(activity));
    }

    public void setUniversityActions(){
        setPlaces();

        onClickUniversity();
        onClickDegreeCourse();

        checkUniversity();
    }

    private void setPlaces() {
        eventRepository.getAllEvents(new CallBack() {
            @Override
            public void onSuccess(Object object) {
                List<Event> events = (List<Event>) object;
                if(events == null)
                    events = new ArrayList<>();

                places = new ArrayList<>();

                for(Event event : events){
                    if(!places.contains(event.getPlace()))
                        places.add(event.getPlace());
                }

                AdapterActions.setAdapter(activity, mPlace, places);
            }

            @Override
            public void onError(Object object) {

            }
        });
    }

    private void onClickUniversity() {
        universities = new ArrayList<>();

        mUniversity.setOnClickListener(c -> {
            if(mUniversityInputLayout.getError() != null)
                mUniversityInputLayout.setError(null);
        });

        universityRepository.readAllUniversities(new CallBack() {
            @Override
            public void onSuccess(Object universitiesQuery) {
                Log.d("TAG", "onSuccess: " + universitiesQuery.toString());
                for(University university : (ArrayList<University>) universitiesQuery){
                    universities.add(university.getName());
                }
                AdapterActions.setAdapter(activity, mUniversity, universities);
            }

            @Override
            public void onError(Object object) {

            }
        });
    }

    private void onClickDegreeCourse(){
        mUniversityCourse.setOnClickListener(c -> {
            if(mUniversityCourseInputLayout.getError() != null)
                mUniversityCourseInputLayout.setError(null);
            Log.d("TAG", "onClickDegreeCourseCheck: " + mUniversityCourse.isPopupShowing());
            if(mUniversityCourse.isPopupShowing())
                mUniversityCourse.dismissDropDown();
            else
                mUniversityCourse.showDropDown();
        });
    }

    private void checkUniversity(){
        mUniversity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(mUniversityInputLayout.getError() != null)
                    mUniversityInputLayout.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void onClickDate(){
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(activity, R.style.DatePickerDialog,
                (datePicker, yearPicked, monthPicked, dayPicked) -> {
                    mDate.setText(dayPicked + "/" + (monthPicked + 1) + "/" + yearPicked);


                }, year, month, dayOfMonth);

        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());

        mBtnDate.setOnClickListener(c -> {

            if(!datePickerDialog.isShowing())
                activity.runOnUiThread(datePickerDialog::show);
        });
    }

    private boolean checkFields(){
        boolean checkUniversity = false;

        if(mUniversity.getText() != null)
            if(mUniversity.getText().toString().isEmpty())
            checkUniversity = true;
            else
                for(String university : universities) {
                    if (mUniversity.getText().toString().equals(university)) {
                        checkUniversity = true;
                        break;
                    }
                }

        if(!checkUniversity)
            mUniversityInputLayout.setError("Leave empty or insert a valid university");

        boolean checkCourse = false;

        if(mUniversityCourse.getText() != null)
            if(mUniversityCourse.getText().toString().isEmpty())
                checkCourse = true;
            else
                for(String course : Constant.getCourses(activity)) {
                    if (mUniversityCourse.getText() != null && mUniversityCourse.getText().toString().equals(course)) {
                        checkCourse = true;
                        break;
                    }
        }

        if(!checkCourse)
            mUniversityCourseInputLayout.setError("Leave empty or insert a valid course");

        boolean checkPlace = false;

        if(mPlace.getText() != null)
            if(mPlace.getText().toString().isEmpty())
                checkPlace = true;
            else
                for(String place : places) {
                    if (mPlace.getText() != null && mPlace.getText().toString().equals(place)) {
                        checkPlace = true;
                        break;
                    }
                }

        if(!checkPlace)
            mPlaceInputLayout.setError("Leave empty or insert a valid place");

        return checkUniversity && checkCourse && checkPlace;
    }

    public void setOnClickListeners() {
        binding.filterIcon.setOnClickListener(v -> {
            toggleFilters();
        });

        mSearch.setOnClickListener(c -> {
            if (checkFields()) {
                String university = mUniversity.getText().toString();
                String course = mUniversityCourse.getText().toString();
                String place = mPlace.getText().toString();
                String date = mDate.getText().toString();

                CallBack callBack = new CallBack() {
                    @Override
                    public void onSuccess(Object object) {
                        List<Event> events = (List<Event>) object;

                        if(events == null)
                            events = new ArrayList<>();

                        mNumber.setText(events.size() + "");
                        binding.setEventAdapter(new EventAdapter(events, activity));
                        toggleFilters(1500);

                    }

                    @Override
                    public void onError(Object object) {

                    }
                };

                try {
                    String check = checkFilters(university, course, place, date);
                    Log.d(TAG, "setOnClickListeners: " + check);
                    switch (check) {
                        case "UCPD":
                            eventRepository.getEventsByUniversityCoursePlaceDate(
                                    university, course, place, date, callBack);
                            break;
                        case "UCP":
                            eventRepository.getEventsByUniversityCoursePlace(
                                    university, course, place, callBack);
                            break;
                        case "UCD":
                            eventRepository.getEventsByUniversityCourseDate(
                                    university, course, date, callBack);
                            break;
                        case "UPD":
                            eventRepository.getEventsByUniversityPlaceDate(
                                    university, place, date, callBack);
                            break;
                        case "CPD":
                            eventRepository.getEventsByCoursePlaceDate(
                                    course, place, date, callBack);
                            break;
                        case "UC":
                            eventRepository.getEventsByUniversityCourse(university, course, callBack);
                            break;
                        case "UP":
                            eventRepository.getEventsByUniversityPlace(university, place, callBack);
                            break;
                        case "UD":
                            eventRepository.getEventsByUniversityDate(university, date, callBack);
                            break;
                        case "CP":
                            eventRepository.getEventsByCoursePlace(course, place, callBack);
                            break;
                        case "CD":
                            eventRepository.getEventsByCourseDate(course, date, callBack);
                        case "PD":
                            eventRepository.getEventsByPlaceDate(place, date, callBack);
                            break;
                        case "U":
                            eventRepository.getEventsByUniversity(university, callBack);
                            break;
                        case "C":
                            eventRepository.getEventsByCourse(course, callBack);
                            break;
                        case "P":
                            eventRepository.getEventsByPlace(place, callBack);
                            break;
                        case "D":
                            eventRepository.getEventsByDate(date, callBack);
                            break;
                        default:
                            Calendar todayCalendar = Calendar.getInstance();

                            String today = todayCalendar.get(Calendar.DAY_OF_MONTH) + "/" +
                                    (todayCalendar.get(Calendar.MONTH)+1) + "/" +
                                    todayCalendar.get(Calendar.YEAR);

                            eventRepository.getEventsFromDate(today, callBack);
                            break;
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
    }

        private String checkFilters(String university, String course, String place, String date) {
            //4 filters
            if(!university.isEmpty() && !course.isEmpty() && !place.isEmpty() && !date.isEmpty()){
                return "UCPD";
            }

            /* 3 FILTERS */
            //U C P
            else if(!university.isEmpty() && !course.isEmpty() && !place.isEmpty() && date.isEmpty()){
                return "UCP";
            }
            //U C D
            else if(!university.isEmpty() && !course.isEmpty() && place.isEmpty() && !date.isEmpty()){
                return "UCD";
            }
            //U P D
            else if(!university.isEmpty() && course.isEmpty() && !place.isEmpty() && !date.isEmpty()){
                return "UPD";
            }
            //U C P
            else if(university.isEmpty() && !course.isEmpty() && !place.isEmpty() && date.isEmpty()){
                return "CPD";
            }

            /* 2 FILTERS */
            // U C
            else if(!university.isEmpty() && !course.isEmpty() && place.isEmpty() && date.isEmpty()){
                return "UC";
            }
            //U P
            else if(!university.isEmpty() && course.isEmpty() && !place.isEmpty() && date.isEmpty()){
                return "UP";
            }
            //U D
            else if(!university.isEmpty() && course.isEmpty() && place.isEmpty() && !date.isEmpty()){
                return "UD";
            }
            //C P
            else if(university.isEmpty() && !course.isEmpty() && !place.isEmpty() && date.isEmpty()){
                return "CP";
            }
            //C D
            else if(university.isEmpty() && !course.isEmpty() && place.isEmpty() && !date.isEmpty()){
                return "CD";
            }
            //P D
            else if(university.isEmpty() && course.isEmpty() && !place.isEmpty() && !date.isEmpty()){
                return "PD";
            }

            /* 1 FILTER */
            //U
            else if(!university.isEmpty() && course.isEmpty() && place.isEmpty() && date.isEmpty()){
                return "U";
            }
            //C
            else if(university.isEmpty() && !course.isEmpty() && place.isEmpty() && date.isEmpty()){
                return "C";
            }
            //P
            else if(university.isEmpty() && course.isEmpty() && !place.isEmpty() && date.isEmpty()){
                return "P";
            }
            //D
            else if(university.isEmpty() && course.isEmpty() && place.isEmpty() && !date.isEmpty()){
                return "D";
            }
            return "";
    }


    private void toggleFilters(){
        Utils.hideSoftKeyboard(activity);
        if(bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED){
            mFilterIcon.setImageResource(R.drawable.ic_baseline_filter_list_24);
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
        else {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            mFilterIcon.setImageResource(R.drawable.ic_baseline_filter_list_off_24);
        }
    }

    private void toggleFilters(int time){
        Utils.hideSoftKeyboard(activity);
        Handler handler = new Handler();

        Runnable runnable = () -> {
            if(bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED){
                mFilterIcon.setImageResource(R.drawable.ic_baseline_filter_list_24);
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
            else {
                mFilterIcon.setImageResource(R.drawable.ic_baseline_filter_list_off_24);
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        };

        handler.postDelayed(runnable, time);
    }
}

package it.piedpiper.unity.viewmodel.login;

import android.os.Bundle;

import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.material.snackbar.Snackbar;

import it.piedpiper.unity.R;
import it.piedpiper.unity.databinding.ActivityLoginBinding;
import it.piedpiper.unity.repository.IUserRepository;
import it.piedpiper.unity.repository.impl.UserRepository;
import it.piedpiper.unity.ui.login.LoginActivity;
import it.piedpiper.unity.utils.Constant;

public class LoginActivityViewModel {
    private final ActivityLoginBinding binding;
    private final LoginActivity activity;
    private final IUserRepository userRepository;

    private NavController navController;

    public LoginActivityViewModel(LoginActivity loginActivity, ActivityLoginBinding binding) {
        this.binding = binding;
        this.activity = loginActivity;
        userRepository = new UserRepository(loginActivity);
    }

    public void initUI(){
        NavHostFragment navHostFragment = (NavHostFragment) activity.getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);
        assert navHostFragment != null;
        navController = navHostFragment.getNavController();

        String fragmentName = activity.getIntent().getStringExtra("Fragment");

        replaceFragment(fragmentName);
    }

    public void replaceFragment(String fragment){
        switch (fragment){
            case "Login":
                navController.navigate(Constant.LOGIN);
                break;
            case "SignUp":
                navController.navigate(Constant.SIGNUP);
                break;
            case "Verify":
                navController.navigate(Constant.VERIFY_EMAIL);
                break;
            case "ForgotPassword":
                navController.navigate(Constant.FORGOT_PASSWORD);
                break;
            case "User":
                navController.navigate(Constant.SET_USER);
                break;
            case "University":
                navController.navigate(Constant.SET_UNIVERSITY);
                break;
        }
    }

    public void replaceFragment(String fragment, Bundle args){
        switch (fragment){
            case "Login":
                navController.navigate(Constant.LOGIN, args);
                break;
            case "SignUp":
                navController.navigate(Constant.SIGNUP, args);
                break;
            case "Verify":
                navController.navigate(Constant.VERIFY_EMAIL, args);
                break;
            case "ForgotPassword":
                navController.navigate(Constant.FORGOT_PASSWORD, args);
                break;
            case "User":
                navController.navigate(Constant.SET_USER, args);
                break;
            case "University":
                navController.navigate(Constant.SET_UNIVERSITY, args);
                break;
        }
    }


}

package it.piedpiper.unity.viewmodel.preferences.profile;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;

import it.piedpiper.unity.R;
import it.piedpiper.unity.databinding.FragmentChangePasswordBinding;
import it.piedpiper.unity.ui.login.LoginActivity;
import it.piedpiper.unity.ui.preferences.PreferencesActivity;
import it.piedpiper.unity.utils.Constant;

public class ChangePasswordViewModel {
    private final PreferencesActivity activity;
    private final FragmentChangePasswordBinding binding;

    private TextInputLayout mPasswordInputLayout, mConfirmPasswordInputLayout;
    private TextInputEditText mPassword, mConfirmPassword;

    public ChangePasswordViewModel(PreferencesActivity activity, FragmentChangePasswordBinding binding) {
        this.activity = activity;
        this.binding = binding;
    }

    public void initUI() {
        mPasswordInputLayout = binding.passwordInputLayout;
        mConfirmPasswordInputLayout = binding.confirmPasswordInputLayout;
        mPassword = binding.password;
        mConfirmPassword = binding.confirmPassword;

        binding.btnSave.setOnClickListener(c -> {
            if(checkFields()) {
                FirebaseAuth.getInstance().getCurrentUser().updatePassword(binding.password.getText().toString());
                Intent intent = new Intent(activity, LoginActivity.class);
                intent.putExtra(Constant.FRAGMENT_STRING, Constant.LOGIN_STRING);
                activity.startActivity(intent);
                activity.finish();
            }
        });
    }

    private boolean checkFields() {
        String password = mPassword.getText().toString();
        String confirmPassword = mConfirmPassword.getText().toString();

        //EMPTY FIELDS
        return !isEmptyFields(password, confirmPassword)
                && checkPassword(password, confirmPassword);
    }

    private boolean isEmptyFields(String password, String confirmPassword) {
        if (TextUtils.isEmpty(password)) {
            mPasswordInputLayout.setErrorEnabled(true);
            mPasswordInputLayout.setError("Insert a password!");
            return true;
        }
        if (TextUtils.isEmpty(confirmPassword)) {
            mConfirmPasswordInputLayout.setErrorEnabled(true);
            mConfirmPasswordInputLayout.setError("Re-insert the password!");
            return true;
        }
        return false;
    }

    private boolean checkPassword(String password, String confirmPassword){
        if(password.length() < 5){
            mPasswordInputLayout.setError("Password must be > 5");
            return false;
        }
        else
            return password.equals(confirmPassword);
    }

    public void onChangeCheckFields(){
        int checkIcon = androidx.mediarouter.R.drawable.ic_checked_checkbox;
        ColorStateList checkIconColor = ColorStateList.valueOf(activity.getResources().getColor(R.color.pastel_green_200, activity.getTheme()));
        Drawable visibilityIcon = mPasswordInputLayout.getEndIconDrawable();
        ColorStateList visibilityIconColor = ColorStateList.valueOf(activity.getResources().getColor(R.color.light_grey, activity.getTheme()));
        Drawable visibilityIconCp = mConfirmPasswordInputLayout.getEndIconDrawable();

        mPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(mPasswordInputLayout.getError() != null){
                    mPasswordInputLayout.setErrorEnabled(false);
                    mPasswordInputLayout.setError(null);
                }
                if(mPassword.getText().toString().length() <= 5) {
                    //mPasswordInputLayout.setError("Password must be > 5");
                    mPasswordInputLayout.setHelperText("Password must be greater than 5");
                }
                else{
                    mPasswordInputLayout.setHelperText(null);
                    if(mPassword.getText().toString().equals(mConfirmPassword.getText().toString())){
                        mPasswordInputLayout.setEndIconDrawable(checkIcon);
                        mConfirmPasswordInputLayout.setEndIconDrawable(checkIcon);
                        mPasswordInputLayout.setEndIconTintList(checkIconColor);
                        mConfirmPasswordInputLayout.setEndIconTintList(checkIconColor);
                    }
                    else{
                        mPasswordInputLayout.setEndIconDrawable(visibilityIcon);
                        mConfirmPasswordInputLayout.setEndIconDrawable(visibilityIconCp);
                        mPasswordInputLayout.setEndIconTintList(visibilityIconColor);
                        mConfirmPasswordInputLayout.setEndIconTintList(visibilityIconColor);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mConfirmPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(mConfirmPasswordInputLayout.getError() != null){
                    mConfirmPasswordInputLayout.setErrorEnabled(false);
                    mConfirmPasswordInputLayout.setError(null);
                }

                if(mPassword.getText().toString().length() > 5) {
                    if(mPassword.getText().toString().equals(mConfirmPassword.getText().toString())){
                        mPasswordInputLayout.setEndIconDrawable(checkIcon);
                        mConfirmPasswordInputLayout.setEndIconDrawable(checkIcon);
                        mPasswordInputLayout.setEndIconTintList(checkIconColor);
                        mConfirmPasswordInputLayout.setEndIconTintList(checkIconColor);
                    }
                    else{
                        mPasswordInputLayout.setEndIconDrawable(visibilityIcon);
                        mConfirmPasswordInputLayout.setEndIconDrawable(visibilityIconCp);
                        mPasswordInputLayout.setEndIconTintList(visibilityIconColor);
                        mConfirmPasswordInputLayout.setEndIconTintList(visibilityIconColor);
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
}

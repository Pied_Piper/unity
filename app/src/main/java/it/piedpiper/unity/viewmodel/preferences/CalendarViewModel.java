package it.piedpiper.unity.viewmodel.preferences;

import android.annotation.SuppressLint;
import android.preference.PreferenceActivity;
import android.util.Log;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.LinearLayoutCompat;

import com.google.firebase.Timestamp;
import com.google.firebase.firestore.auth.User;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import io.getstream.chat.android.client.ChatClient;
import it.piedpiper.unity.adapter.CalendarAdapter;
import it.piedpiper.unity.adapter.CalendarEventAdapter;
import it.piedpiper.unity.callback.CallBack;
import it.piedpiper.unity.databinding.FragmentCalendarBinding;
import it.piedpiper.unity.model.Event;
import it.piedpiper.unity.repository.IEventRepository;
import it.piedpiper.unity.repository.IUniversityRepository;
import it.piedpiper.unity.repository.IUserRepository;
import it.piedpiper.unity.repository.impl.EventRepository;
import it.piedpiper.unity.ui.main.MainActivity;
import it.piedpiper.unity.ui.preferences.PreferencesActivity;

public class CalendarViewModel {
    private static final String TAG = "CalendarViewModel";
    private final PreferencesActivity activity;
    private final FragmentCalendarBinding binding;
    private final IEventRepository eventRepository;

    private ImageView previousMonth;
    private ImageView nextMonth;
    private TextView displayDate;
    private TextView weekDay;
    private TextView year;
    private GridView gridView;

    private ArrayList<Date> cells;
    private Calendar calendar;

    private Date selectedDate;
    private int monthBeginningCell;

    public CalendarViewModel(PreferencesActivity activity, FragmentCalendarBinding binding) {
        this.activity = activity;
        this.binding = binding;
        this.eventRepository = new EventRepository(activity);
    }


    public void initUI() {
        previousMonth = binding.calendarPreviousMonth;
        nextMonth = binding.calendarNextMonth;
        displayDate = binding.date;
        weekDay = binding.weekDay;
        year = binding.year;
        gridView = binding.calendarGridView;
        cells = new ArrayList<>();
        calendar = Calendar.getInstance();
        selectedDate = calendar.getTime();
        monthBeginningCell = calendar.get(Calendar.DAY_OF_WEEK) - 1;
    }

    public void initCalendar() throws ParseException {
        cells.clear();
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        monthBeginningCell = calendar.get(Calendar.DAY_OF_WEEK) - 1;
        // move calendar backwards to the beginning of the week
        calendar.add(Calendar.DAY_OF_MONTH, -monthBeginningCell);

        // fill cells
        int numberCells = calendar.getActualMaximum(Calendar.DAY_OF_MONTH) + monthBeginningCell;

        if (numberCells > 35)
            numberCells = 42;
        else
            numberCells = 35;

        while (cells.size() < numberCells) {
            cells.add(calendar.getTime());
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }
        calendar.setTime(selectedDate);

        setEvents();

        setCalendar();
    }

    private void setCalendar() throws ParseException {
        updateCalendar();
        setGridView();

        onClickCell();

        changeMonth();
    }

    @SuppressLint("SimpleDateFormat")
    private void updateCalendar(){
        // update title

        SimpleDateFormat sdf = new SimpleDateFormat("EEEE,d MMM,yyyy");

        String[] dateToday = sdf.format(calendar.getTime()).split(",");
        weekDay.setText(dateToday[0]);
        displayDate.setText(dateToday[1]);
        year.setText(dateToday[2]);

    }

    private void setGridView() throws ParseException {
        Date calendarTime = calendar.getTime();
        @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        String startDate = dateFormat.format(calendar.getTime());

        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        String endDate = dateFormat.format(calendar.getTime());

        calendar.setTime(calendarTime);

        Log.d(TAG, "getEvents: " + startDate + " " + endDate);

        eventRepository.getEventsByUserDate(Objects.requireNonNull(ChatClient.instance().getCurrentUser()).getId(), startDate, endDate, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                List<Event> eventList = (List<Event>) object;
                if (eventList == null)
                    eventList = new ArrayList<>();
                gridView.setAdapter(new CalendarAdapter(activity, cells, calendar, eventList));
            }

            @Override
            public void onError(Object object) {

            }
        });
    }

    private void onClickCell(){
        gridView.setOnItemClickListener((parent, view, position, id) -> {
            calendar.add(Calendar.DAY_OF_MONTH, (position - (monthBeginningCell - 1) - calendar.get(Calendar.DAY_OF_MONTH)));
            selectedDate = calendar.getTime();

            try {
                initCalendar();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        });
    }

    private void changeMonth(){
        previousMonth.setOnClickListener(c -> {
            calendar.add(Calendar.MONTH,-1);
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            selectedDate = calendar.getTime();
            try {
                initCalendar();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        });

        nextMonth.setOnClickListener(c -> {
            calendar.add(Calendar.MONTH,1);
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            selectedDate = calendar.getTime();
            try {
                initCalendar();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        });

    }


    private void getEvents(String date) throws ParseException {
        String userId = Objects.requireNonNull(ChatClient.instance().getCurrentUser()).getId();
        Log.d(TAG, "getEvents: " + date);

        eventRepository.getEventsByUserDate(userId, date, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                List<Event> events = (List<Event>) object;

                if(events == null)
                    events = new ArrayList<>();

                CalendarEventAdapter calendarEventAdapter = new CalendarEventAdapter(events, activity);

                binding.setEventAdapter(calendarEventAdapter);
            }

            @Override
            public void onError(Object object) {

            }
        });
    }

    private void setEvents() throws ParseException {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String date = dateFormat.format(selectedDate);

        getEvents(date);
    }
}

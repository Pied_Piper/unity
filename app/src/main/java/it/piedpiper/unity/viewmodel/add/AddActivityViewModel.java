package it.piedpiper.unity.viewmodel.add;

import static it.piedpiper.unity.utils.Constant.PERMISSION_REQUEST_CODE;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

import com.getstream.sdk.chat.utils.Utils;
import com.google.android.material.appbar.MaterialToolbar;
import com.r0adkll.slidr.Slidr;
import com.r0adkll.slidr.model.SlidrInterface;

import java.util.Objects;

import it.piedpiper.unity.R;
import it.piedpiper.unity.databinding.ActivityAddBinding;
import it.piedpiper.unity.ui.add.AddActivity;
import it.piedpiper.unity.utils.Animations;
import it.piedpiper.unity.utils.Constant;

public class AddActivityViewModel {
    private static final String TAG = "AddActivityViewModel";
    private ActivityAddBinding binding;
    private AddActivity activity;

    private SlidrInterface slidr;
    private NavController navController;


    private ActivityResultLauncher<Intent> launchActivity;
    private Uri uri;

    public Uri getUri(){
        return uri;
    }

    public AddActivityViewModel(AddActivity activity, ActivityAddBinding binding) {
        this.binding = binding;
        this.activity = activity;
    }

    public void initUI(){

        launchActivity = activity.registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(), result -> {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        Intent data = result.getData();
                        assert data != null;
                        uri = data.getData();
                    }
                });


        slidr = Slidr.attach(activity);

        NavHostFragment navHostFragment = (NavHostFragment) activity.getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);
        assert navHostFragment != null;
        navController = navHostFragment.getNavController();

        String fragmentName = activity.getIntent().getStringExtra(Constant.FRAGMENT_STRING);

        replaceFragment(fragmentName);

        binding.navHostFragment.setOnClickListener(c -> {
            Utils.hideSoftKeyboard(activity);
        });

        navHostFragment = (NavHostFragment) activity.getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);
        assert navHostFragment != null;
        navController = navHostFragment.getNavController();

        //toolbar = findViewById(R.id.toolbar);
        MaterialToolbar toolbar = binding.toolbar;
        activity.setSupportActionBar(toolbar);
        Objects.requireNonNull(activity.getSupportActionBar()).setHomeButtonEnabled(true);
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_round_arrow_back_ios_new_24);

        toolbar.setNavigationOnClickListener(v -> {
            activity.finish();
        });
    }

    public void replaceFragment(String fragment){
        switch (fragment){
            case Constant.GROUP:
                navController.navigate(Constant.ADD_GROUP);
                break;
            case Constant.ASSOCIATION:
                navController.navigate(Constant.ADD_ASSOCIATION);
                break;
            case Constant.LESSON:
                navController.navigate(Constant.ADD_LESSON);
                break;
            case Constant.FILE:
                navController.navigate(Constant.ADD_FILE);
                break;
        }
    }

    public void replaceFragment(String fragment, Bundle args){
        switch (fragment){
            case Constant.GROUP:
                navController.navigate(Constant.ADD_GROUP, args);
                break;
            case Constant.ASSOCIATION:
                navController.navigate(Constant.ADD_ASSOCIATION, args);
                break;
            case Constant.LESSON:
                navController.navigate(Constant.ADD_LESSON, args);
                break;
            case Constant.FILE:
                navController.navigate(Constant.ADD_FILE, args);
                break;
        }
    }

    public boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(activity, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return result == PackageManager.PERMISSION_GRANTED;
    }


    public void requestPermission() {
        activity.requestPermissions(new String[]{
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                PERMISSION_REQUEST_CODE);
    }

    public void pickPDF(){
        if(this.checkPermission()) {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("application/pdf");
            launchActivity.launch(intent);
        }
        else
            this.requestPermission();
    }
}

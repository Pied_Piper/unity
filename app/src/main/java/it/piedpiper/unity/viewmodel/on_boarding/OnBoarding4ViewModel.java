package it.piedpiper.unity.viewmodel.on_boarding;

import android.content.Intent;

import androidx.fragment.app.Fragment;

import it.piedpiper.unity.databinding.FragmentOnBoarding4Binding;
import it.piedpiper.unity.ui.login.LoginActivity;
import it.piedpiper.unity.ui.on_boarding.OnBoardingActivity;
import it.piedpiper.unity.utils.Constant;

public class OnBoarding4ViewModel {
    private final OnBoardingActivity activity;
    private final FragmentOnBoarding4Binding binding;

    public OnBoarding4ViewModel(OnBoardingActivity activity, FragmentOnBoarding4Binding binding){
        this.activity = activity;
        this.binding = binding;
    }

    public void setOnClickListeners() {
        binding.btnLogin.setOnClickListener(c -> {
            Intent intent = new Intent(activity, LoginActivity.class);
            intent.putExtra(Constant.FRAGMENT_STRING, Constant.LOGIN_STRING);
            activity.startActivity(intent);
        });

        binding.btnSignup.setOnClickListener(c -> {
            Intent intent = new Intent(activity, LoginActivity.class);
            intent.putExtra(Constant.FRAGMENT_STRING, Constant.SIGNUP_STRING);
            activity.startActivity(intent);
        });
    }
}

package it.piedpiper.unity.viewmodel.login;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageDecoder;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.lifecycle.LifecycleOwner;
import androidx.navigation.fragment.NavHostFragment;

import com.bumptech.glide.Glide;
import com.getstream.sdk.chat.utils.Utils;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.messaging.FirebaseMessaging;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import io.getstream.chat.android.client.ChatClient;
import io.getstream.chat.android.client.logger.ChatLogLevel;
import io.getstream.chat.android.client.models.Device;
import io.getstream.chat.android.client.models.PushProvider;
import io.getstream.chat.android.livedata.ChatDomain;
import it.piedpiper.unity.api.ChatApp;
import it.piedpiper.unity.callback.CallBack;
import it.piedpiper.unity.databinding.FragmentCreateAssociationBinding;
import it.piedpiper.unity.databinding.FragmentLoginBinding;
import it.piedpiper.unity.firebase.FirebaseDatabaseReference;
import it.piedpiper.unity.model.AuthenticationResponse;
import it.piedpiper.unity.model.User;
import it.piedpiper.unity.repository.IUserRepository;
import it.piedpiper.unity.repository.impl.UserRepository;
import it.piedpiper.unity.ui.login.LoginActivity;
import it.piedpiper.unity.ui.login.LoginFragment;
import it.piedpiper.unity.ui.main.MainActivity;
import it.piedpiper.unity.utils.Constant;
import it.piedpiper.unity.utils.Converter;
import it.piedpiper.unity.utils.SharedPreferencesProvider;

public class LoginViewModel {
    private static final String TAG = "LoginViewModel";
    private final FragmentLoginBinding binding;
    private final LoginActivity activity;
    private final LifecycleOwner lifecycleOwner;
    private final IUserRepository userRepository;

    private TextInputLayout mPasswordIL;
    private TextInputEditText mName;
    private TextInputEditText mPassword;

    private Button mLogin;
    private TextView mSignup;
    private TextView mForgotPassword;

    public LoginViewModel(LoginActivity activity, LifecycleOwner lifecycleOwner, FragmentLoginBinding binding) {
        this.binding = binding;
        this.activity = activity;
        this.lifecycleOwner = lifecycleOwner;
        this.userRepository = new UserRepository(activity);
    }

    public void initUI() {
        mName = binding.name;
        mPassword = binding.password;
        mPasswordIL = binding.passwordInputLayout;

        mLogin = binding.btnLogin;
        mSignup = binding.signup;
        mForgotPassword = binding.forgotPassword;
    }

    public void setOnClickListeners(){
        mLogin.setOnClickListener(v -> {
            Utils.hideSoftKeyboard(mLogin);
            loginUser();
        });

        mSignup.setOnClickListener(v -> {
            activity.replaceFragment("SignUp");
        });

        mForgotPassword.setOnClickListener(v -> {
            activity.replaceFragment("ForgotPassword");
        });

        mName.setOnClickListener(c -> {
            if(mName.getError() != null){
                mName.setError(null);
            }
            if(mPassword.getError() != null){
                mPasswordIL.setEndIconVisible(true);
                mPassword.setError(null);
            }
        });

        mPassword.setOnClickListener(c -> {
            if(mName.getError() != null){
                mName.setError(null);
            }
            if(mPassword.getError() != null){
                mPasswordIL.setEndIconVisible(true);
                mPassword.setError(null);
            }
        });
    }

    public void onChangeCheckFields(){
        mName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(mName.getError() != null){
                    mName.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(mPassword.getError() != null){
                    mPassword.setError(null);
                    mPasswordIL.setEndIconVisible(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    //EMPTY
    private void isEmptyFields(String name, String password) {
        if (TextUtils.isEmpty(name)) {
            mName.setError("Insert your username or email!");
        }
        if (TextUtils.isEmpty(password)) {
            mPassword.setError("Insert password!");
            mPasswordIL.setEndIconVisible(false);
        }
    }

    //USERNAME OR EMAIL
    public static boolean isEmail(String name) {
        return Patterns.EMAIL_ADDRESS.matcher(name).matches();
    }

    //DISPLAY LOGIN ERRORS
    private void setLoginErrors(){
        mName.setError("Insert correct email or password!");
        mPassword.setError("Insert correct username or password!");
        mPasswordIL.setEndIconVisible(false);
    }

    //Login Method
    private void loginUser(String email){
        String password = mPassword.getText().toString();

        userRepository.signInWithEmail(email.toLowerCase(), password, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                FirebaseAuth.getInstance().addAuthStateListener(firebaseAuth -> {
                    FirebaseUser user = firebaseAuth.getCurrentUser();
                    if (user != null) {
                        if(user.isEmailVerified())
                            activity.runOnUiThread(() -> isUserCreatedOnDB(user.getEmail()));
                            //isUserCreatedOnDB(user.getEmail());
                        else
                            activity.replaceFragment("Verify");
                    }
                });
            }

            @Override
            public void onError(Object object) {
                if(FirebaseAuth.getInstance().getCurrentUser() == null)
                    setLoginErrors();
            }
        });
    }

    private void isUserCreatedOnDB(String email){
        userRepository.readUserByEmail(email, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                User user = (User) object;
                Log.d(TAG, "onSuccess: " + user.toString());
                if(user.getUsername().isEmpty())
                    activity.replaceFragment("User");
                else{
                    SharedPreferencesProvider sharedPreferencesProvider = new SharedPreferencesProvider(activity.getApplication());
                    sharedPreferencesProvider.setUserId(user.getId());
                    sharedPreferencesProvider.setUserSetted(true);

                    user.setUserUniversity(user.getUniversity().getName());
                    user.setUserCourse(user.getUniversity().getCourse());
                    userRepository.createUserDao(user);

                    io.getstream.chat.android.client.models.User userStream = new io.getstream.chat.android.client.models.User();
                    userStream.setId(user.getId());
                    userStream.setName(user.getUsername());
                    userStream.setImage(user.getPhotoUrl());

                    ChatApp.connectUser(userStream);

                    FirebaseMessaging.getInstance().getToken()
                            .addOnCompleteListener(task -> {
                                if (!task.isSuccessful()) {
                                    Log.w(TAG, "Fetching FCM registration token failed", task.getException());
                                    return;
                                }

                                // Get new FCM registration token
                                String token = task.getResult();
                                Log.d(TAG, "onSuccess: " + token);

                                ChatClient.instance().addDevice(
                                        new Device(
                                                token,
                                                PushProvider.FIREBASE
                                        )
                                ).enqueue(r -> {
                                    if (r.isSuccess()) {
                                        Handler handler = new Handler();
                                        Runnable runnable = () -> {
                                            Intent intent = new Intent(activity, MainActivity.class);
                                            activity.startActivity(intent);
                                            activity.finish();
                                        };

                                        handler.postDelayed(runnable, 1000);
                                    } else {
                                        r.error();
                                    }
                                });
                            });

                    Toast.makeText(activity, "Login Successfull", Toast.LENGTH_SHORT).show();

                    /*Handler handler = new Handler();
                    Runnable runnable = () -> {
                        Intent intent = new Intent(activity, MainActivity.class);
                        activity.startActivity(intent);
                        activity.finish();
                    };

                    handler.postDelayed(runnable, 1000);*/
                }
            }

            @Override
            public void onError(Object object) {

            }
        });
    }

    private void loginUser() {
        String name = mName.getText().toString();
        String password = mPassword.getText().toString();

        //EMPTY FIELDS
        isEmptyFields(name, password);

        if(isEmail(name)){
            //QUERYING EMAIL FROM USERNAME
            /*userRepository.readUserByUsername(name, new CallBack() {
                @Override
                public void onSuccess(Object user) {
                    if(user != null){
                        loginUser(((User)user).getEmail());}
                    else
                        loginUser(name);
                }

                @Override
                public void onError(Object object) {

                }
            });*/
            loginUser(name);
        }
        else{
            mName.setError("Inser a valid email");
            //loginUser(name);
        }
    }
}

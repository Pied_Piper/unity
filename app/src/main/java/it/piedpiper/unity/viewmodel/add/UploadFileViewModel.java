package it.piedpiper.unity.viewmodel.add;

import static it.piedpiper.unity.utils.Constant.PERMISSION_REQUEST_CODE;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import it.piedpiper.unity.callback.CallBack;
import it.piedpiper.unity.databinding.FragmentUploadFileBinding;
import it.piedpiper.unity.repository.IStorageRepository;
import it.piedpiper.unity.repository.impl.StorageRepository;
import it.piedpiper.unity.ui.add.AddActivity;
import it.piedpiper.unity.ui.login.LoginActivity;
import it.piedpiper.unity.ui.main.MainActivity;
import it.piedpiper.unity.utils.Constant;

public class UploadFileViewModel {

    private static final String TAG = "UploadFileViewModel";
    private final FragmentUploadFileBinding binding;
    private final AddActivity activity;
    private final IStorageRepository storageRepository;

    private FloatingActionButton mFabChangeFile;
    private TextInputLayout mNameInputLayout, mCategoryInputLayout;
    private TextInputEditText mName, mCategory;
    private Button mBtnDone;

    private Uri uri;


    public UploadFileViewModel(AddActivity activity, FragmentUploadFileBinding binding){
        this.activity = activity;
        this.binding = binding;
        this.storageRepository = new StorageRepository(activity);
    }

    public void initUI(){
        mFabChangeFile = binding.btnChangeFile;
        mNameInputLayout = binding.fileNameInputLayout;
        mCategoryInputLayout = binding.categoryInputLayout;

        mName = binding.fileName;
        mCategory = binding.category;
        mBtnDone = binding.btnCreateFile;

    }

    public void onTextChangeListeners(){
        mName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(mNameInputLayout.getError() != null)
                    mNameInputLayout.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mCategory.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(mCategoryInputLayout.getError() != null)
                    mCategoryInputLayout.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void setOnClickListeners(){
        mFabChangeFile.setOnClickListener(c -> {
            activity.pickPDF();
        });

        mBtnDone.setOnClickListener(c -> {
            if(checkFields()) {
                uri = activity.getUri();
                addPdfToStorage(uri);
            }
        });
    }

    private boolean checkFields(){
        if(mName.getText().toString().isEmpty()){
            mNameInputLayout.setError("Insert file name *.pdf");
            return false;
        }
        if(mCategory.getText().toString().isEmpty()){
            mCategoryInputLayout.setError("Insert file category");
            return false;
        }
        return true;
    }

    private void addPdfToStorage(Uri uri){
        storageRepository.putFilePDF(mName.getText().toString(), mCategory.getText().toString(), uri, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                Log.d("TAG", "onSuccess: File uploaded on storage");

                Handler handler = new Handler();
                Runnable runnable = activity::finish;

                handler.postDelayed(runnable, 750);
            }

            @Override
            public void onError(Object object) {
                Log.d("TAG", "onSuccess: File not uploaded on storage");
            }
        });
    }

}

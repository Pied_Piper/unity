package it.piedpiper.unity.viewmodel.login;

import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import it.piedpiper.unity.callback.CallBack;
import it.piedpiper.unity.databinding.FragmentForgotPasswordBinding;
import it.piedpiper.unity.firebase.FirebaseDatabaseReference;
import it.piedpiper.unity.model.User;
import it.piedpiper.unity.repository.IUserRepository;
import it.piedpiper.unity.repository.impl.UserRepository;
import it.piedpiper.unity.ui.login.LoginActivity;

public class ForgotPasswordViewModel {
    private final FragmentForgotPasswordBinding binding;
    private final LoginActivity activity;
    private final IUserRepository userRepository;

    private ImageButton btnBack;
    private EditText mName;
    private EditText mPassword;
    private EditText mConfirmPassword;
    private Button mSendLink;

    private RelativeLayout popup;
    private Button mLogin;
    private TextView blackBackgroundFilter;

    public ForgotPasswordViewModel(LoginActivity activity, FragmentForgotPasswordBinding binding) {
        this.binding = binding;
        this.activity = activity;
        this.userRepository = new UserRepository(activity);
    }


    public void initUI(){
        btnBack = binding.btnBack;
        mName = binding.name;
        mSendLink = binding.sendLinkBtn;

        popup = binding.emailSent;
        blackBackgroundFilter = binding.blackBackgroundFilter;
        mLogin = binding.login;
    }

    public void setOnClickListeners(){
        btnBack.setOnClickListener(c -> {
            activity.getSupportFragmentManager().popBackStack();
        });

        mSendLink.setOnClickListener(c -> {
            String name = mName.getText().toString();

            if(isEmptyFields(name)){
                if(LoginViewModel.isEmail(name)) {
                    /*userRepository.readUserByEmail(name, new CallBack() {
                        @Override
                        public void onSuccess(Object object) {
                            User user = (User) object;
                            FirebaseDatabaseReference.AUTH.sendPasswordResetEmail(user.getEmail());
                        }

                        @Override
                        public void onError(Object object) {

                        }
                    });*/
                    FirebaseDatabaseReference.AUTH.sendPasswordResetEmail(name);
                }
                else
                    FirebaseDatabaseReference.AUTH.sendPasswordResetEmail(name);
            }
        });

        mLogin.setOnClickListener(c -> {
            activity.replaceFragment("Login");
        });
    }

    private boolean isEmptyFields(String name) {
        if (TextUtils.isEmpty(name)) {
            mName.setError("Insert your username or email!");
            return false;
        }
        return true;
    }
}

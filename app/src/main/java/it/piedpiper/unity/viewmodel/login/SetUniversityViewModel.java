package it.piedpiper.unity.viewmodel.login;

import android.content.Intent;
import android.net.Uri;
import android.text.Editable;
import android.text.TextWatcher;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import com.bumptech.glide.Glide;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import io.getstream.chat.android.client.ChatClient;
import io.getstream.chat.android.client.models.Device;
import io.getstream.chat.android.client.models.PushProvider;
import it.piedpiper.unity.R;
import it.piedpiper.unity.adapter.AdapterActions;
import it.piedpiper.unity.api.ChatApp;
import it.piedpiper.unity.callback.CallBack;
import it.piedpiper.unity.databinding.FragmentSetUniversityBinding;
import it.piedpiper.unity.model.Logo;
import it.piedpiper.unity.model.University;
import it.piedpiper.unity.model.User;
import it.piedpiper.unity.repository.ILogoRepository;
import it.piedpiper.unity.repository.IStorageRepository;
import it.piedpiper.unity.repository.IUniversityRepository;
import it.piedpiper.unity.repository.IUserRepository;
import it.piedpiper.unity.repository.impl.LogoRepository;
import it.piedpiper.unity.repository.impl.StorageRepository;
import it.piedpiper.unity.repository.impl.UniversityRepository;
import it.piedpiper.unity.repository.impl.UserRepository;
import it.piedpiper.unity.ui.login.LoginActivity;
import it.piedpiper.unity.ui.main.MainActivity;
import it.piedpiper.unity.utils.Animations;
import it.piedpiper.unity.utils.Constant;

public class SetUniversityViewModel {
    private static final String TAG = "SetUniversityViewModel";
    private final FragmentSetUniversityBinding binding;
    private final LoginActivity activity;
    private final IUserRepository userRepository;
    private final IUniversityRepository universityRepository;
    private final IStorageRepository storageRepository;
    private final ILogoRepository logoRepository;

    private final String username;
    private final byte[] data;

    private TextInputLayout mCountryInputLayout;
    private TextInputLayout mUniversityInputLayout;
    private TextInputLayout mUniversityCourseInputLayout;
    private AutoCompleteTextView mCountry;
    private AutoCompleteTextView mUniversity;
    private AutoCompleteTextView mUniversityCourse;
    private Button mButtonFinish;

    private ArrayList<String> universities;
    private final String[] courses;
    private final String[] countries;

    private HashMap<String, University> universitiesMap;

    public SetUniversityViewModel(LoginActivity activity, FragmentSetUniversityBinding binding, String username, byte[] data){
        this.activity = activity;
        this.binding = binding;
        userRepository = new UserRepository(activity);
        universityRepository = new UniversityRepository();
        storageRepository = new StorageRepository(activity);
        logoRepository = new LogoRepository();

        this.username = username;
        this.data = data;

        this.courses = Constant.getCourses(activity);
        this.countries = activity.getResources().getStringArray(R.array.country_array);

        universitiesMap = new HashMap<>();
    }

    public void initUI(){
        mCountryInputLayout = binding.countryInputLayout;
        mCountry = binding.country;
        mUniversityInputLayout = binding.universityInputLayout;
        mUniversityCourseInputLayout = binding.universityCourseInputLayout;
        mUniversity = binding.university;
        mUniversityCourse = binding.universityCourse;
        mButtonFinish = binding.btnFinish;

        AdapterActions.setAdapter(activity, mCountry, countries);
        AdapterActions.setAdapter(activity, mUniversityCourse, courses);
    }

    public void setUniversityActions(){
        onClickUniversity();
        onClickDegreeCourse();

        checkCountry();
        checkUniversity();
    }


    private void onClickUniversity() {
        universities = new ArrayList<>();

        mUniversity.setOnClickListener(c -> {
            if(mUniversityInputLayout.getError() != null)
                mUniversityInputLayout.setError(null);
        });
    }

    private void onClickDegreeCourse(){
        mUniversityCourse.setOnClickListener(c -> {
            if(mUniversityCourseInputLayout.getError() != null)
                mUniversityCourseInputLayout.setError(null);
        });
    }

    private void checkCountry(){
        mCountry.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(mCountryInputLayout.getError() != null)
                    mCountryInputLayout.setError(null);

                for(String country : countries){
                    if(country.equals(s.toString())) {

                        universityRepository.fetchUniversitiesCountry(country, new CallBack() {
                            @Override
                            public void onSuccess(Object object) {
                                List<University> universityList = (List<University>) object;

                                Log.d(TAG, "onSuccess: " + universityList);

                                for(University university: universityList){
                                    universities.add(university.getName());
                                    universitiesMap.put(university.getName(), university);
                                }

                                AdapterActions.setAdapter(activity, mUniversity, universities);
                            }

                            @Override
                            public void onError(Object object) {

                            }
                        });

                        break;
                    }
                    else {
                        if(universities.size() != 0) {
                            AdapterActions.setAdapter(activity, mUniversity, new ArrayList<>());
                            mUniversity.setText(null);
                            universities = new ArrayList<>();
                            universitiesMap = new HashMap<>();
                        }
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void checkUniversity(){
        mUniversity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(mUniversityInputLayout.getError() != null)
                    mUniversityInputLayout.setError(null);

                TransitionManager.beginDelayedTransition((ViewGroup) binding.getRoot());
                for(String university : universities){
                    if(university.equals(s.toString())) {
                        Animations.alphaView(mUniversityCourseInputLayout, 0, 1);
                        mUniversityCourseInputLayout.setVisibility(View.VISIBLE);

                        String webPage = Objects.requireNonNull(universitiesMap.get(university)).getWeb_pages().get(0);
                        Log.d(TAG, "onTextChanged WebPage: " + webPage);

                        if(webPage != null) {
                            logoRepository.getLogo(webPage, new CallBack() {
                                @Override
                                public void onSuccess(Object object) {
                                    Logo logo = (Logo) object;

                                    if(!activity.isDestroyed())
                                        Glide.with(activity)
                                                .load(logo.getLogo_url())
                                                .centerCrop()
                                                .into(binding.logo);
                                }

                                @Override
                                public void onError(Object object) {

                                }
                            });
                        }

                        break;
                    }
                    else if(mUniversityCourseInputLayout.getVisibility() == View.VISIBLE) {
                        Animations.alphaView(mUniversityCourseInputLayout, 1, 0);
                        AdapterActions.setAdapter(activity, mUniversityCourse, new ArrayList<>());
                        mUniversityCourse.setText(null);
                        mUniversityCourseInputLayout.setVisibility(View.GONE);

                        binding.logo.setImageDrawable(null);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void onClickFinish(){
        mButtonFinish.setOnClickListener(c -> {
            updateUser();
        });
    }

    private boolean isEmptyFields(){
        if(mCountry.getText() == null) {
            mCountryInputLayout.setError("Insert your country");
            return true;
        }
        if(mUniversity.getText() == null) {
            mUniversityInputLayout.setError("Insert your university");
            return true;
        }
        if(mUniversityCourse.getText() == null){
            mUniversityCourseInputLayout.setError("Insert your degree course");
            return true;
        }
        return false;
    }

    private boolean checkFields(){
        boolean checkCountry = false;
        for(String country : countries){
            if (mCountry.getText().toString().equals(country)) {
                checkCountry = true;
                break;
            }
        }
        if(!checkCountry)
            mCountryInputLayout.setError("Insert a valid country!");

        boolean checkUniversity = false;
        for(String university : universities){
            if (mUniversity.getText().toString().equals(university)) {
                checkUniversity = true;
                break;
            }
        }
        if(!checkUniversity)
            mUniversityInputLayout.setError("Insert a valid university!");

        boolean checkCourse = false;
        for(String course : courses){
            if (mUniversityCourse.getText().toString().equals(course)) {
                checkCourse = true;
                break;
            }
        }

        if(!checkCourse)
            mUniversityCourseInputLayout.setError("Insert a valid degree course!");

        return checkCountry & checkUniversity & checkCourse;
    }

    private void updateUser(){
        String university = mUniversity.getText().toString();
        String course = mUniversityCourse.getText().toString();

        if(!isEmptyFields()){
            if(checkFields())
                universityRepository.getUniversityByName(university, new CallBack() {
                    @Override
                    public void onSuccess(Object object) {
                        if(object == null){
                            universityRepository.createUniversity(universitiesMap.get(university), new CallBack() {
                                @Override
                                public void onSuccess(Object object) {
                                    Log.d(TAG, "onSuccess: university created");
                                }

                                @Override
                                public void onError(Object object) {

                                }
                            });
                        }
                        updateUser(university, course);

                    }

                    @Override
                    public void onError(Object object) {

                    }
                });
        }
    }

    private void updateUser(String university, String course){
        Log.d(TAG, "updateUser: " + FirebaseAuth.getInstance().getCurrentUser().getEmail());

        userRepository.readUserByEmail(FirebaseAuth.getInstance().getCurrentUser().getEmail(), new CallBack() {
            @Override
            public void onSuccess(Object object) {
                User doc = (User) object;

                Log.d(TAG, "onSuccess: " + doc.toString());
                String userID = doc.getId();

                if(data != null) {
                    storageRepository.putImage(username, data, new CallBack() {
                        @Override
                        public void onSuccess(Object object) {
                            String uri = ((Uri) object).toString();

                            User user = new User(userID, username, doc.getEmail(), university, course, uri, data);
                            connectUser(user);
                        }
                        @Override
                        public void onError(Object object) {

                        }
                    });
                }
                else{
                    User user = new User(userID, username, doc.getEmail(), university, course, "", null);
                    connectUser(user);
                }
            }

            @Override
            public void onError(Object object) {

            }
        });
    }

    private void connectUser(User user){
        userRepository.updateUser(user.getId(), user, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                userRepository.createUserDao(user);

                ChatApp.initClient(activity);
                ChatClient client = ChatClient.instance();

                Log.d(TAG, "onSuccess: photo-url" + user.getPhotoUrl());

                io.getstream.chat.android.client.models.User userStream = new io.getstream.chat.android.client.models.User();
                userStream.setId(user.getId());
                userStream.setName(user.getUsername());
                userStream.setImage(user.getPhotoUrl());


                client.updateUser(userStream).enqueue(result -> {
                    ChatApp.connectUser(userStream);

                    FirebaseMessaging.getInstance().getToken()
                            .addOnCompleteListener(task -> {
                                if (!task.isSuccessful()) {
                                    Log.w(TAG, "Fetching FCM registration token failed", task.getException());
                                    return;
                                }

                                // Get new FCM registration token
                                String token = task.getResult();
                                Log.d(TAG, "onSuccess: " + token);

                                client.addDevice(
                                        new Device(
                                                token,
                                                PushProvider.FIREBASE
                                        )
                                ).enqueue(r -> {
                                    if (r.isSuccess()) {
                                        activity.startActivity(new Intent(activity, MainActivity.class));
                                        activity.finish();
                                    } else {
                                        r.error();
                                    }
                                });
                            });
                });
            }

            @Override
            public void onError(Object object) {

            }
        });
    }
}

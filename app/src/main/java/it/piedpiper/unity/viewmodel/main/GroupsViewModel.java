package it.piedpiper.unity.viewmodel.main;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.getstream.sdk.chat.utils.Utils;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.List;

import io.getstream.chat.android.client.ChatClient;
import it.piedpiper.unity.R;
import it.piedpiper.unity.adapter.AdapterActions;
import it.piedpiper.unity.adapter.AssociationAdapter;
import it.piedpiper.unity.adapter.GroupAdapter;
import it.piedpiper.unity.callback.CallBack;
import it.piedpiper.unity.databinding.FragmentGroupsBinding;
import it.piedpiper.unity.model.Association;
import it.piedpiper.unity.model.Group;
import it.piedpiper.unity.model.University;
import it.piedpiper.unity.repository.IAssociationRepository;
import it.piedpiper.unity.repository.IGroupRepository;
import it.piedpiper.unity.repository.IUniversityRepository;
import it.piedpiper.unity.repository.impl.AssociationRepository;
import it.piedpiper.unity.repository.impl.GroupRepository;
import it.piedpiper.unity.repository.impl.UniversityRepository;
import it.piedpiper.unity.ui.main.MainActivity;
import it.piedpiper.unity.utils.Animations;
import it.piedpiper.unity.utils.Constant;
import it.piedpiper.unity.viewmodel.UserViewModel;

public class GroupsViewModel {
    private static final String TAG = "GroupsViewModel";
    private final FragmentGroupsBinding binding;
    private final MainActivity activity;
    private ArrayList<String> universities;
    private final IUniversityRepository universityRepository;
    private final IGroupRepository groupRepository;
    private final IAssociationRepository IAssociationRepository;


    private TabLayout mTabs;
    private TextInputLayout mUniversityInputLayout, mUniversityCourseInputLayout, mSubjectInputLayout;
    private AutoCompleteTextView mUniversity, mUniversityCourse;
    private RelativeLayout mCourseLayout;
    private TextInputEditText mSubject;
    private BottomSheetBehavior bottomSheetBehavior;
    private ImageView mFilterIcon;

    private TextView mNumber;
    private TextView mGroupAssociationTv;

    private Button mSearch;

    private RecyclerView recyclerView;

    public GroupsViewModel(MainActivity activity, FragmentGroupsBinding binding){
        this.activity = activity;
        this.binding = binding;
        this.universityRepository = new UniversityRepository();
        this.groupRepository = new GroupRepository(activity);
        this.IAssociationRepository = new AssociationRepository(activity);
    }

    public void initUI(){
        mTabs = binding.tabsLayout;
        mUniversityInputLayout = binding.universityInputLayout;
        mUniversity = binding.university;
        mUniversityCourseInputLayout = binding.universityCourseInputLayout;
        mUniversityCourse = binding.universityCourse;
        mSubjectInputLayout = binding.subjectInputLayout;
        mSubject = binding.subject;
        LinearLayoutCompat mHeaderBottomSheet = binding.headerBottomsheet;
        LinearLayoutCompat mContentLayout = binding.contentLayout;
        bottomSheetBehavior = BottomSheetBehavior.from(mContentLayout);
        mFilterIcon = binding.filterIcon;

        mCourseLayout = binding.courseLayout;

        mNumber = binding.groupsNumber;
        mGroupAssociationTv = binding.groupsAssociationTv;

        mSearch = binding.searchBtn;

        recyclerView = binding.recyclerView;
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(activity);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());


        getUser();
    }

    private void getUser(){
        UserViewModel userViewModel = new UserViewModel(activity.getApplication(), activity);
        binding.setUserViewModel(userViewModel);
    }

    private void textChangeListener() {
        mUniversity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(mUniversityInputLayout.getError() != null)
                    mUniversityInputLayout.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mUniversityCourse.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(mUniversityCourseInputLayout.getError() != null)
                    mUniversityCourseInputLayout.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mSubject.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(mSubjectInputLayout.getError() != null)
                    mSubjectInputLayout.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void onClickTab(){
        mTabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Utils.hideSoftKeyboard(activity);
                if (tab.getPosition() == 1) {
                    if (mCourseLayout.getVisibility() == View.VISIBLE) {
                        TransitionManager.beginDelayedTransition((ViewGroup) binding.getRoot());
                        Animations.alphaView(mCourseLayout, 1, 0);
                        mCourseLayout.setVisibility(View.GONE);
                        mGroupAssociationTv.setText("associations");
                    }
                }
            }

            @SuppressLint("SetTextI18n")
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                if (tab.getPosition() == 1)
                    if (mCourseLayout.getVisibility() == View.GONE) {
                        TransitionManager.beginDelayedTransition((ViewGroup) binding.getRoot());
                        Animations.alphaView(mCourseLayout, 0, 1);
                        mCourseLayout.setVisibility(View.VISIBLE);
                        mGroupAssociationTv.setText("groups");
                    }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    public void onClickSearch(){
        String id = ChatClient.instance().getCurrentUser().getId();
        mSearch.setOnClickListener(c -> {
            String university = mUniversity.getText().toString();
            String universityCourse = mUniversityCourse.getText().toString();
            String subject = mSubject.getText().toString();

            Utils.hideSoftKeyboard(activity);

            switch(mTabs.getSelectedTabPosition()){
                case 0:
                    if(checkFieldsGroup()){
                        groupRepository.getGroupsByUniversityCourseSubject(university, universityCourse, subject, new CallBack() {
                            @SuppressLint("SetTextI18n")
                            @Override
                            public void onSuccess(Object object) {
                                List<Group> groups = (List<Group>) object;
                                if(groups == null)
                                    groups = new ArrayList<>();


                                groups.removeIf(group -> group.getMembers().contains(id));
                                GroupAdapter adapter = new GroupAdapter(groups, activity);

                                mNumber.setText(groups.size() + "");
                                recyclerView.setAdapter(adapter);
                                toggleFilters(1500);
                            }

                            @Override
                            public void onError(Object object) {

                            }
                        });
                    }
                    break;
                case 1:
                    if(checkFieldsAssociation()){
                        IAssociationRepository.getAssociationByUniversity(university, new CallBack() {
                            @SuppressLint("SetTextI18n")
                            @Override
                            public void onSuccess(Object object) {
                                List<Association> associations = (List<Association>) object;
                                if(associations == null)
                                    associations = new ArrayList<>();

                                associations.removeIf(association -> association.getMembers().contains(id));

                                AssociationAdapter adapter = new AssociationAdapter(associations, activity);

                                mNumber.setText(associations.size() + "");
                                recyclerView.setAdapter(adapter);
                                toggleFilters(1500);
                            }

                            @Override
                            public void onError(Object object) {

                            }
                        });
                    }
                    break;
            }

        });
    }

    private boolean checkFieldsGroup(){
        String university = mUniversity.getText().toString();
        String course = mUniversityCourse.getText().toString();
        String subject = mSubject.getText().toString();

        boolean checkUniversity = false;
        for(String u : universities){
            if (u.equals(university)) {
                checkUniversity = true;
                break;
            }
        }
        if(!checkUniversity)
            mUniversityInputLayout.setError("Insert a valid university!");

        boolean checkCourse = false;
        for(String c : Constant.getCourses(activity)){
            if (c.equals(course)) {
                checkCourse = true;
                break;
            }
        }

        if(!checkCourse)
            mUniversityCourseInputLayout.setError("Insert a valid degree course!");

        if(subject.isEmpty()){
            mSubjectInputLayout.setError("Insert the group' s subject of study!");
            return false;
        }
        return true;
    }

    private boolean checkFieldsAssociation(){
        String university = mUniversity.getText().toString();

        boolean checkUniversity = false;
        for(String u : universities){
            if (u.equals(university)) {
                checkUniversity = true;
                break;
            }
        }
        if(!checkUniversity)
            mUniversityInputLayout.setError("Insert a valid university!");

        return checkUniversity;
    }

    public void setBottomSheet(){
        LinearLayoutCompat contentLayout = binding.contentLayout;
        bottomSheetBehavior = BottomSheetBehavior.from(contentLayout);
        bottomSheetBehavior.setFitToContents(false);
        bottomSheetBehavior.setHideable(false);//prevents the boottom sheet from completely hiding off the screen
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_DRAGGING);//initially state to fully expanded

        View filterIcon = binding.filterIcon;
        filterIcon.setOnClickListener(v -> {
            toggleFilters();
        });
    }

    private void toggleFilters(){
        Utils.hideSoftKeyboard(activity);
        if(bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED){
            mFilterIcon.setImageResource(R.drawable.ic_baseline_filter_list_24);
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
        else {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            mFilterIcon.setImageResource(R.drawable.ic_baseline_filter_list_off_24);
        }
    }

    private void toggleFilters(int time){
        Utils.hideSoftKeyboard(activity);
        Handler handler = new Handler();

        Runnable runnable = () -> {
            if(bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED){
                mFilterIcon.setImageResource(R.drawable.ic_baseline_filter_list_24);
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
            else {
                mFilterIcon.setImageResource(R.drawable.ic_baseline_filter_list_off_24);
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        };

        handler.postDelayed(runnable, time);
    }

    public void setUniversityActions(){
        AdapterActions.setAdapter(activity, mUniversityCourse, Constant.getCourses(activity));

        onClickUniversity();
        onClickDegreeCourse();

        checkUniversity();
    }

    private void onClickUniversity() {
        universities = new ArrayList<>();

        mUniversity.setOnClickListener(c -> {
            if(mUniversityInputLayout.getError() != null)
                mUniversityInputLayout.setError(null);
        });

        universityRepository.readAllUniversities(new CallBack() {
            @Override
            public void onSuccess(Object universitiesQuery) {
                Log.d("TAG", "onSuccess: " + universitiesQuery.toString());
                for(University university : (ArrayList<University>) universitiesQuery){
                    universities.add(university.getName());
                }
                AdapterActions.setAdapter(activity, mUniversity, universities);
            }

            @Override
            public void onError(Object object) {

            }
        });
    }

    private void onClickDegreeCourse(){
        mUniversityCourse.setOnClickListener(c -> {

            if(mUniversityCourseInputLayout.getError() != null)
                mUniversityCourseInputLayout.setError(null);
            Log.d("TAG", "onClickDegreeCourse: " + mUniversityCourse.isPopupShowing());
            if(!mUniversityCourse.isPopupShowing())
                mUniversityCourse.dismissDropDown();
            else{
                if (mUniversity.getText() != null) {
                    Log.d("TAG", "onClickDegreeCourse: " + mUniversity.getText().toString());
                }
            }
        });
    }

    private void checkUniversity(){
        mUniversity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(mUniversityInputLayout.getError() != null)
                    mUniversityInputLayout.setError(null);

                mUniversityCourse.setText(null);

                Log.d("TAG", "onTextChanged: " + s);

                /*for(String university : universities) {
                    if (university.equals(s.toString())) {
                        universityRepository.getUniversityByName(mUniversity.getText().toString(), new CallBack() {
                            @Override
                            public void onSuccess(Object university) {
                                Log.d("TAG", "onSuccess university selected: " + ((University) university).getName());
                                setAdapter(mUniversityCourse, ((University) university).getCourses());
                            }

                            @Override
                            public void onError(Object object) {

                            }
                        });
                        break;
                    }
                }*/
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

}

package it.piedpiper.unity.viewmodel.main;

import static it.piedpiper.unity.utils.Constant.PERMISSION_REQUEST_CODE;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.Px;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModel;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.NavOptions;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.NavigationUI;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;

import com.getstream.sdk.chat.utils.Utils;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.firestore.ListenerRegistration;

import java.util.Arrays;
import java.util.Collections;
import java.util.Objects;

import io.getstream.chat.android.client.ChatClient;
import io.getstream.chat.android.client.api.models.FilterObject;
import io.getstream.chat.android.client.api.models.QueryUsersRequest;
import io.getstream.chat.android.client.models.Filters;
import io.getstream.chat.android.ui.ChatUI;
import io.getstream.chat.android.ui.avatar.AvatarBitmapFactory;
import io.getstream.chat.android.ui.avatar.AvatarStyle;
import io.getstream.chat.android.ui.channel.list.header.ChannelListHeaderView;
import io.getstream.chat.android.ui.channel.list.header.viewmodel.ChannelListHeaderViewModel;
import io.getstream.chat.android.ui.channel.list.header.viewmodel.ChannelListHeaderViewModelBinding;
import it.piedpiper.unity.R;
import it.piedpiper.unity.api.ChatApp;
import it.piedpiper.unity.callback.CallBack;
import it.piedpiper.unity.callback.FirebaseCallBack;
import it.piedpiper.unity.database.UserRoomDatabase;
import it.piedpiper.unity.databinding.ActivityMainBinding;
import it.piedpiper.unity.databinding.HeaderNavigationDrawerBinding;
import it.piedpiper.unity.firebase.FirebaseDatabaseReference;
import it.piedpiper.unity.model.User;
import it.piedpiper.unity.repository.IUserRepository;
import it.piedpiper.unity.repository.impl.UserRepository;
import it.piedpiper.unity.ui.add.AddActivity;
import it.piedpiper.unity.ui.login.LoginActivity;
import it.piedpiper.unity.ui.main.MainActivity;
import it.piedpiper.unity.ui.preferences.PreferencesActivity;
import it.piedpiper.unity.utils.Animations;
import it.piedpiper.unity.utils.Constant;
import it.piedpiper.unity.utils.NetworkConnection;
import it.piedpiper.unity.utils.SharedPreferencesProvider;


public class MainActivityViewModel {
    private static final String TAG = "MainActivityViewModel";
    private final ActivityMainBinding binding;
    private final MainActivity activity;
    private final IUserRepository userRepository;
    private ListenerRegistration listenerRegistration;

    private CoordinatorLayout filterLayout;
    private TextView textViewBackground;

    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private NavController navController;


    private FloatingActionButton mFabAdd;
    private ExtendedFloatingActionButton mFabCreateGroup, mFabCreateAssociation, mFabCreateLesson, mFabUploadFile;

    private boolean isFabMenuOpen = false;

    private Animation fabOpen, fabClose, fabRotateOpen, fabRotateClose;

    private ChannelListHeaderView channelListHeaderView;



    public MainActivityViewModel(MainActivity mainActivity, ActivityMainBinding activityMainBinding) {
        this.binding = activityMainBinding;
        this.activity = mainActivity;
        userRepository = new UserRepository(mainActivity);
    }

    /*public void checkFragment(String fragment){
        if(fragment.equals(Constant.FILE))
            navController.navigate(R.id.library);
    }*/


    public void setActionBar() {
        //Toolbar tb = binding.toolbar;

        //activity.setSupportActionBar(tb);

        //setNavigationDrawer(tb, user);
        channelListHeaderView = binding.channelToolbar;
        channelListHeaderView.showOfflineTitle();

        ChannelListHeaderViewModelBinding.bind(new ChannelListHeaderViewModel(), channelListHeaderView, activity);

        drawerLayout = binding.drawerLayout;
        navigationView = binding.navigationView;
        NavHostFragment navHostFragment = (NavHostFragment) activity.getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);

        assert navHostFragment != null;
        navController = navHostFragment.getNavController();


        //connectUser();
        View navHeader = binding.navigationView.getHeaderView(0);
        HeaderNavigationDrawerBinding headerBinding = HeaderNavigationDrawerBinding.bind(navHeader);

        channelListHeaderView.setUser(Objects.requireNonNull(ChatClient.instance().getCurrentUser()));
        Log.d(TAG, "setActionBar: " + ChatClient.instance().getCurrentUser());
        channelListHeaderView.showOnlineTitle();
        channelListHeaderView.setOnlineTitle("UniTy");
        channelListHeaderView.setOnUserAvatarClickListener(() -> {
            binding.drawerLayout.openDrawer(GravityCompat.START);
            setNavHeader(headerBinding);
        });

        setNavHeader(headerBinding);
        setFragmentsNavigation();
        //setNavHeader();
    }

    public void reloadChannelListHeaderView() {
        channelListHeaderView.setUser(Objects.requireNonNull(ChatClient.instance().getCurrentUser()));
    }

    /*public void connectUser(){
        /*userRepository.readUserDao(new CallBack() {
            @Override
            public void onSuccess(Object object) {
                it.piedpiper.unity.model.User tmp = (it.piedpiper.unity.model.User) object;

                    }
                });
            }

            @Override
            public void onError(Object object) {

            }
        });

        SharedPreferences sharedPref = activity.getSharedPreferences(Constant.SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
        String userID = sharedPref.getString(Constant.USER_ID, null);

        FilterObject filter = Filters.eq("id", userID);
        QueryUsersRequest request = new QueryUsersRequest(filter, 0, 1);

        ChatClient.instance().queryUsers(request).enqueue(result -> {
            if(result.isSuccess()){
                ChatClient.instance().connectUser(result.data().get(0), userID).enqueue();

                activity.runOnUiThread(this::setActionBar);
            }
        });
    }*/

    public void setFabs(){
        textViewBackground = binding.textViewBlackBackground;
        mFabAdd = binding.fabAdd;
        mFabCreateGroup = binding.fabCreateGroup;
        mFabCreateAssociation = binding.fabCreateAssociation;
        mFabCreateLesson = binding.fabCreateLesson;
        mFabUploadFile = binding.fabUploadFile;

        filterLayout = binding.layoutFilter;

        fabOpen = AnimationUtils.loadAnimation(activity.getApplicationContext(), R.anim.fab_open);
        fabClose = AnimationUtils.loadAnimation(activity.getApplicationContext(), R.anim.fab_close);
        fabRotateOpen = AnimationUtils.loadAnimation(activity.getApplicationContext(), R.anim.fab_rotate_open);
        fabRotateClose = AnimationUtils.loadAnimation(activity.getApplicationContext(), R.anim.fab_rotate_close);

        mFabAdd.setBackgroundTintList(ColorStateList.valueOf(activity.getResources().getColor(R.color.pastel_blue_200, activity.getTheme())));

        //FloatingActionButton methods
        mFabAdd.setOnClickListener(v -> {
            if(!isFabMenuOpen){
                openFabs();
            }
            else{
                closeFabs();
            }
        });

        textViewBackground.setOnClickListener(v -> {
            if(isFabMenuOpen) {
                closeFabs();
            }
        });

        binding.navHostFragment.setOnClickListener(c -> {
            Utils.hideSoftKeyboard(activity);
        });
    }

    private void openFabs(){
        isFabMenuOpen = true;

        filterLayout.setVisibility(View.VISIBLE);
        Animations.alphaView(textViewBackground, 0, 1);

        //Animations.alphaView(bottomNavigationView, 1, 0);
        binding.bottomNavigationView.animate().translationY(binding.bottomNavigationView.getHeight());

        mFabAdd.startAnimation(fabRotateOpen);
        mFabAdd.setBackgroundTintList(ColorStateList.valueOf(activity.getResources().getColor(R.color.pastel_red_500, activity.getTheme())));

        mFabCreateGroup.startAnimation(fabOpen);
        mFabCreateGroup.animate().translationX(-activity.getResources().getDimension(R.dimen.standard_125));
        mFabCreateGroup.animate().translationY(-activity.getResources().getDimension(R.dimen.standard_75));
        mFabCreateGroup.setClickable(true);
        mFabCreateGroup.setVisibility(View.VISIBLE);

        mFabCreateAssociation.startAnimation(fabOpen);
        mFabCreateAssociation.animate().translationX(-activity.getResources().getDimension(R.dimen.standard_100));
        mFabCreateAssociation.animate().translationY(-activity.getResources().getDimension(R.dimen.standard_150));
        mFabCreateAssociation.setClickable(true);
        mFabCreateAssociation.setVisibility(View.VISIBLE);

        mFabCreateLesson.startAnimation(fabOpen);
        mFabCreateLesson.animate().translationX(activity.getResources().getDimension(R.dimen.standard_100));
        mFabCreateLesson.animate().translationY(-activity.getResources().getDimension(R.dimen.standard_150));
        mFabCreateLesson.setClickable(true);
        mFabCreateLesson.setVisibility(View.VISIBLE);

        mFabUploadFile.startAnimation(fabOpen);
        mFabUploadFile.animate().translationX(activity.getResources().getDimension(R.dimen.standard_125));
        mFabUploadFile.animate().translationY(-activity.getResources().getDimension(R.dimen.standard_75));
        mFabUploadFile.setClickable(true);
        mFabUploadFile.setVisibility(View.VISIBLE);
    }

    private void closeFabs(){
        isFabMenuOpen = false;

        Animations.alphaView(textViewBackground, 1, 0);

        filterLayout.setVisibility(View.INVISIBLE);

        //Animations.alphaView(bottomNavigationView, 0, 1);
        binding.bottomNavigationView.animate().translationY(0);

        mFabAdd.startAnimation(fabRotateClose);
        mFabAdd.setBackgroundTintList(ColorStateList.valueOf(activity.getResources().getColor(R.color.pastel_blue_200, activity.getTheme())));

        mFabCreateGroup.startAnimation(fabClose);
        mFabCreateGroup.animate().translationX(0);
        mFabCreateGroup.animate().translationY(activity.getResources().getDimension(R.dimen.standard_75));
        mFabCreateGroup.setClickable(false);

        mFabCreateAssociation.startAnimation(fabClose);
        mFabCreateAssociation.animate().translationX(0);
        mFabCreateAssociation.animate().translationY(activity.getResources().getDimension(R.dimen.standard_150));
        mFabCreateAssociation.setClickable(false);

        mFabCreateLesson.startAnimation(fabClose);
        mFabCreateLesson.animate().translationX(0);
        mFabCreateLesson.animate().translationY(activity.getResources().getDimension(R.dimen.standard_150));
        mFabCreateLesson.setClickable(false);

        mFabUploadFile.startAnimation(fabClose);
        mFabUploadFile.animate().translationX(0);
        mFabUploadFile.animate().translationY(activity.getResources().getDimension(R.dimen.standard_75));
        mFabUploadFile.setClickable(false);
    }

    private void setNavigationDrawer(Toolbar tb, User user) {
        DrawerLayout drawerLayout = binding.drawerLayout;
        NavigationView navigationView = binding.navigationView;

        NavHostFragment navHostFragment = (NavHostFragment) activity.getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);

        assert navHostFragment != null;
        navController = navHostFragment.getNavController();


        //Navigation Drawer Toggle on/off methods
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(
                activity,
                drawerLayout,
                tb,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close
        );


        actionBarDrawerToggle.syncState();

        Objects.requireNonNull(activity.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        binding.setUser(user);

        //setNavHeader(user);
        setFragmentsNavigation();
    }

    private void setNavHeader(HeaderNavigationDrawerBinding headerBinding) {
        headerBinding.setUser(ChatClient.instance().getCurrentUser());

        headerBinding.avatarView.setUserData(Objects.requireNonNull(ChatClient.instance().getCurrentUser()));
    }

    public void getUserDao() {
        if (listenerRegistration != null)
            listenerRegistration.remove();

        userRepository.readUserDao(new CallBack() {
            @Override
            public void onSuccess(Object object) {
                User user = (User) object;

                //activity.runOnUiThread(() -> setActionBar(user));
                String userID = user.getId();
                //getUser(userID);
            }

            @Override
            public void onError(Object object) {

            }
        });
    }

    public void getUser(){
        assert FirebaseDatabaseReference.USER != null;
        listenerRegistration = userRepository.readUser(FirebaseDatabaseReference.USER.getEmail(), new FirebaseCallBack() {
            @Override
            public void onChildAdded(Object object) {
                if (object != null) {
                    User user = (User) object;
                    setActionBar();
                }
            }

            @Override
            public void onChildChanged(Object object) {
                if (object != null) {
                    User user = (User) object;
                    setActionBar();
                }
            }

            @Override
            public void onChildRemoved(Object object) {
                if (object != null) {
                }
            }

            @Override
            public void onCancelled(Object object) {
                if (object != null) {
                }
            }
         });
    }

    public void setBottomBar() {
        BottomNavigationView bottomNavigationView = binding.bottomNavigationView;
        bottomNavigationView.setBackground(null);

        NavHostFragment navHostFragment = (NavHostFragment) activity.getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);

        assert navHostFragment != null;
        NavController navController = navHostFragment.getNavController();

        NavigationUI.setupWithNavController(bottomNavigationView, navController);
    }

    private void setFragmentsNavigation() {
        navigationView.setNavigationItemSelectedListener(item -> {
            Intent intent;
            switch (item.getItemId()) {
                case Constant.PROFILE:
                    intent = new Intent(activity, PreferencesActivity.class);
                    intent.putExtra(Constant.FRAGMENT_STRING, Constant.PROFILE_STRING);
                    activity.startActivity(intent);
                    break;
                case Constant.SETTINGS:
                    intent = new Intent(activity, PreferencesActivity.class);
                    intent.putExtra(Constant.FRAGMENT_STRING, Constant.SETTINGS_STRING);
                    activity.startActivity(intent);
                    break;
                case Constant.EVENTS:
                    intent = new Intent(activity, PreferencesActivity.class);
                    intent.putExtra(Constant.FRAGMENT_STRING, Constant.EVENTS_STRING);
                    activity.startActivity(intent);
                    break;
                case Constant.CALENDAR:
                    intent = new Intent(activity, PreferencesActivity.class);
                    intent.putExtra(Constant.FRAGMENT_STRING, Constant.CALENDAR_STRING);
                    activity.startActivity(intent);
                    break;
                case Constant.ABOUT:
                    intent = new Intent(activity, PreferencesActivity.class);
                    intent.putExtra(Constant.FRAGMENT_STRING, Constant.ABOUT_STRING);
                    activity.startActivity(intent);
                    break;
                case Constant.HELP:
                    intent = new Intent(activity, PreferencesActivity.class);
                    intent.putExtra(Constant.FRAGMENT_STRING, Constant.HELP_STRING);
                    activity.startActivity(intent);
                    break;
                case Constant.LOGOUT:
                    userRepository.logout(new CallBack() {
                        @Override
                        public void onSuccess(Object object) {

                        }

                        @Override
                        public void onError(Object object) {

                        }
                    });
                    break;
            }
            Utils.hideSoftKeyboard(activity);
            drawerLayout.closeDrawer(GravityCompat.START);
            return true;
        });
    }

    public void setFabsOnClickListeners(){
        onClickCreateGroup();
        onClickCreateAssociation();
        onClickCreateLesson();
        onClickUploadFile();
    }

    private void onClickCreateGroup(){
        mFabCreateGroup.setOnClickListener(c -> {
            closeFabs(Constant.GROUP);
        });
    }

    private void onClickCreateAssociation(){
        mFabCreateAssociation.setOnClickListener(c -> {
            closeFabs(Constant.ASSOCIATION);
        });
    }

    private void onClickCreateLesson(){
        mFabCreateLesson.setOnClickListener(c -> {
            closeFabs(Constant.LESSON);
        });
    }

    private void onClickUploadFile(){
        mFabUploadFile.setOnClickListener(c -> {
            closeFabs(Constant.FILE);
        });
    }

    private void closeFabs(String string) {
        activity.setCheck(true);

        Runnable runnable = this::closeFabs;
        Thread thread = new Thread(runnable);
        thread.start();

        Intent intent = new Intent(activity, AddActivity.class);
        intent.putExtra(Constant.FRAGMENT_STRING, string);
        activity.startActivity(intent);
    }

    private void removeListener() {
        if (listenerRegistration != null)
            listenerRegistration.remove();
        Glide.get(activity).clearMemory();//clear memory
    }

    public void setDrawerItemNotChecked(){
        for (int i = 0; i < navigationView.getMenu().size(); i++) {
            navigationView.getMenu().getItem(i).setChecked(false);
        }
    }

    public void checkDestination(){
        navController.addOnDestinationChangedListener((navController, navDestination, bundle) -> {
            if(navDestination.getId() == R.id.library){
                if(!checkPermission()) {
                    requestPermission();
                }
            }
        });
    }

    public boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(activity, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return result == PackageManager.PERMISSION_GRANTED;
    }


    public void requestPermission() {
        activity.requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
    }

}

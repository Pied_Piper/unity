package it.piedpiper.unity.viewmodel.main.chat;

import android.telecom.Call;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;

import com.getstream.sdk.chat.utils.Utils;
import com.getstream.sdk.chat.viewmodel.MessageInputViewModel;
import com.getstream.sdk.chat.viewmodel.messages.MessageListViewModel;
import com.getstream.sdk.chat.viewmodel.messages.MessageListViewModel.Mode.*;
import com.vanniktech.emoji.EmojiPopup;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import io.getstream.chat.android.client.ChatClient;
import io.getstream.chat.android.client.models.Attachment;
import io.getstream.chat.android.client.models.Message;
import io.getstream.chat.android.client.models.User;
import io.getstream.chat.android.livedata.ChatDomain;
import io.getstream.chat.android.livedata.controller.ChannelController;
import io.getstream.chat.android.ui.message.input.MessageInputView;
import io.getstream.chat.android.ui.message.input.viewmodel.MessageInputViewModelBinding;
import io.getstream.chat.android.ui.message.list.header.MessageListHeaderView;
import io.getstream.chat.android.ui.message.list.header.viewmodel.MessageListHeaderViewModel;
import io.getstream.chat.android.ui.message.list.header.viewmodel.MessageListHeaderViewModelBinding;
import io.getstream.chat.android.ui.message.list.viewmodel.MessageListViewModelBinding;
import io.getstream.chat.android.ui.message.list.viewmodel.factory.MessageListViewModelFactory;
import it.piedpiper.unity.R;
import it.piedpiper.unity.callback.CallBack;
import it.piedpiper.unity.databinding.ActivityChatBinding;
import it.piedpiper.unity.databinding.CreateGroupEventDialogBinding;
import it.piedpiper.unity.factory.EventAttachmentViewFactory;
import it.piedpiper.unity.model.Event;
import it.piedpiper.unity.repository.IEventRepository;
import it.piedpiper.unity.repository.impl.EventRepository;
import it.piedpiper.unity.ui.main.chat.ChatActivity;
import it.piedpiper.unity.utils.Constant;
import it.piedpiper.unity.utils.dialog.CreateEventGroupDialog;

public class ChatViewModel {
    private static final String TAG = "ChatViewModel";
    private final ChatActivity activity;
    private final ActivityChatBinding binding;


    private final IEventRepository eventRepository;

    private MessageListViewModel messageListViewModel;
    private MessageListHeaderViewModel messageListHeaderViewModel;
    private MessageInputViewModel messageInputViewModel;
    private String type;
    private String cid;

    public ChatViewModel(ChatActivity activity, ActivityChatBinding binding) {
        this.activity = activity;
        this.binding = binding;
        this.eventRepository = new EventRepository(activity);
    }

    public void initUI(){
        cid = activity.getIntent().getStringExtra(ChatActivity.CID_KEY);
        type = activity.getIntent().getStringExtra("type");
        if (cid == null) {
            throw new IllegalStateException("Specifying a channel id is required when starting ChannelActivity");
        }

        // Step 1 - Create three separate ViewModels for the views so it's easy
        //          to customize them individually
        MessageListViewModelFactory factory = new MessageListViewModelFactory(cid);

        //ViewModelProvider provider = new ViewModelProvider(activity.getViewModelStore(), f);
        messageListHeaderViewModel = factory.create(MessageListHeaderViewModel.class);
        messageListViewModel = factory.create(MessageListViewModel.class);
        messageInputViewModel = factory.create(MessageInputViewModel.class);

        MessageListHeaderViewModelBinding.bind(messageListHeaderViewModel, binding.messageListHeaderView, activity);
        MessageListViewModelBinding.bind(messageListViewModel, binding.messageListView, activity);
        MessageInputViewModelBinding.bind(messageInputViewModel, binding.messageInputView, activity);

        setSendIcon();

        setObservers();

        onBackPress();

    }

    private void setObservers(){
        // Step 3 - Let both message list header and message input know when we open a thread
        messageListViewModel.getMode().observe(activity, mode -> {
            if (mode instanceof MessageListViewModel.Mode.Thread) {
                Message parentMessage = ((MessageListViewModel.Mode.Thread) mode).getParentMessage();
                messageListHeaderViewModel.setActiveThread(parentMessage);
                messageInputViewModel.setActiveThread(parentMessage);
            } else if (mode instanceof Normal) {
                messageListHeaderViewModel.resetThread();
                messageInputViewModel.resetThread();
            }
        });

        // Step 4 - Let the message input know when we are editing a message
        binding.messageListView.setMessageEditHandler(messageInputViewModel::postMessageToEdit);

        // Step 5 - Handle navigate up state
        messageListViewModel.getState().observe(activity, state -> {
            if (state instanceof MessageListViewModel.State.NavigateUp) {
                activity.finish();
            }
        });
    }

    private void onBackPress(){
        MessageListHeaderView.OnClickListener backHandler = () -> {
            messageListViewModel.onEvent(MessageListViewModel.Event.BackButtonPressed.INSTANCE);
        };
        binding.messageListHeaderView.setBackButtonClickListener(backHandler);
        activity.getOnBackPressedDispatcher().addCallback(activity, new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                backHandler.onClick();
            }
        });
    }

    private void setSendIcon(){
        binding.messageInputView.setSendMessageButtonEnabledDrawable(activity.getDrawable(R.drawable.ic_round_send_24));
        binding.messageInputView.setSendMessageButtonDisabledDrawable(activity.getDrawable(R.drawable.ic_round_send_24));

        //if(type.equals(Constant.GROUP.toLowerCase()) || type.equals(Constant.ASSOCIATION.toLowerCase())){
        binding.messageListView.setAttachmentViewFactory(new EventAttachmentViewFactory(activity, this));

        binding.messageInputView.setSendMessageButtonDisabledDrawable(activity.getDrawable(R.drawable.ic_round_add_24));

        binding.messageInputView.setOnSendButtonClickListener(() -> {
            Utils.hideSoftKeyboard(binding.messageInputView);
        });

        binding.createEvent.setVisibility(View.VISIBLE);

        binding.createEvent.setOnClickListener(c -> {
            CreateEventGroupDialog dialog = CreateEventGroupDialog.newCreateEvent(activity, type, cid);
            dialog.show(activity.getSupportFragmentManager(), null);
        });
    }

    public void checkIsTyping(){
        binding.messageInputView.setTypingListener(new MessageInputView.TypingListener() {
            @Override
            public void onKeystroke() {
                ChatClient.instance().channel(activity.getIntent().getStringExtra(ChatActivity.CID_KEY)).keystroke().enqueue();

                //if(type.equals(Constant.GROUP.toLowerCase()) || type.equals(Constant.ASSOCIATION.toLowerCase())) {
                if (binding.createEvent.getVisibility() == View.VISIBLE)
                    binding.createEvent.setVisibility(View.GONE);
                //}
            }

            @Override
            public void onStopTyping() {
                ChatClient.instance().channel(activity.getIntent().getStringExtra(ChatActivity.CID_KEY)).stopTyping().enqueue();

                //if(type.equals(Constant.GROUP.toLowerCase()) || type.equals(Constant.ASSOCIATION.toLowerCase())) {
                binding.createEvent.setVisibility(View.VISIBLE);
                //}
            }
        });
    }

    public void updateEventMembers(String id, String newMember) {
        eventRepository.getEventById(id, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                Event event = (Event) object;
                boolean check = false;

                for(int i = 0; i < event.getMembers().size(); i++){
                    if(event.getMembers().get(i).equals(newMember)) {
                        event.getMembers().remove(i);
                        i = event.getMembers().size();
                        check = true;
                    }
                }

                if(!check)
                    event.getMembers().add(newMember);

                eventRepository.updateEvent(id, event.getMap(), new CallBack() {
                    @Override
                    public void onSuccess(Object object) {
                        Log.d(TAG, "onSuccess: Members list updated");
                    }

                    @Override
                    public void onError(Object object) {

                    }
                });
            }

            @Override
            public void onError(Object object) {

            }
        });
    }

    public void getEventById(String id, CallBack callBack){
        eventRepository.getEventById(id, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                Event event = (Event) object;
                callBack.onSuccess(event);
            }

            @Override
            public void onError(Object object) {

            }
        });
    }


}

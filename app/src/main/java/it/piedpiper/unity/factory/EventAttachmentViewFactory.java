package it.piedpiper.unity.factory;

import android.annotation.SuppressLint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.getstream.sdk.chat.adapter.MessageListItem;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Objects;

import io.getstream.chat.android.client.ChatClient;
import io.getstream.chat.android.client.models.Attachment;
import io.getstream.chat.android.ui.message.list.MessageListItemStyle;
import io.getstream.chat.android.ui.message.list.adapter.MessageListListenerContainer;
import io.getstream.chat.android.ui.message.list.adapter.viewholder.attachment.AttachmentViewFactory;
import it.piedpiper.unity.callback.CallBack;
import it.piedpiper.unity.databinding.AttachmentEventGroupBinding;
import it.piedpiper.unity.model.Event;
import it.piedpiper.unity.ui.main.chat.ChatActivity;
import it.piedpiper.unity.utils.dialog.ConfirmationDialogFragment;
import it.piedpiper.unity.viewmodel.main.chat.ChatViewModel;


//quando creo evento --> targhetta dell'evento
//factory: pattern ustao come creatore per quella view
public class EventAttachmentViewFactory extends AttachmentViewFactory {
    private static final String TAG = "EventAttachmentViewFactory";
    private final ChatActivity activity;
    private final ChatViewModel viewModel;

    public EventAttachmentViewFactory(ChatActivity activity, ChatViewModel chatViewModel) {
        this.activity = activity;
        this.viewModel = chatViewModel;
    }

    @NonNull
    @Override
    @SuppressLint("SimpleDateFormat")
    public View createAttachmentView(@NonNull MessageListItem.MessageItem data, @Nullable MessageListListenerContainer listeners, @NonNull MessageListItemStyle style, @NonNull ViewGroup parent) {
        Attachment attachment = null;
        for(Attachment a : data.getMessage().getAttachments()){  //considero tipi messaggio che mando --> faccio getType (diverso da getType che ritorna gruppo, associaozme, lezione
            if(Objects.equals(a.getType(), "appointment"))  //--> appointemnt: guardare createeventgroupdialog
                attachment = a; //se il tipo è appointemnet allora lo associo ad a
        }

        if(attachment != null){
            Map<String, Object> appointment = attachment.getExtraData();  //ritorna una map

            String id = appointment.get("id").toString();
            String subject = appointment.get("subject").toString();
            String place = appointment.get("place").toString();
            String calendar = appointment.get("date").toString();

            return createEventView(parent, calendar, id, subject, place);
        }

        return super.createAttachmentView(data, listeners, style, parent);
    }

    @SuppressLint("SimpleDateFormat")
    private View createEventView(ViewGroup parent, String calendar, String id, String subject, String place){
        AttachmentEventGroupBinding binding =
                AttachmentEventGroupBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);

        String day = calendar.substring(0, 10);
        String time = calendar.substring(11);

        binding.date.setText(day);
        binding.time.setText(time);

        binding.place.setText(place);
        binding.argument.setText(subject);

        binding.cardView.setOnClickListener(c -> {
            viewModel.getEventById(id, new CallBack() {
                @Override
                public void onSuccess(Object object) {
                    Event event = (Event) object;

                    boolean check = false;

                    //controllo per verificar ese sono gia dentro il gruppo (esce leave oppure join)
                    for(String userId : event.getMembers()){
                        if(userId.equals(Objects.requireNonNull(ChatClient.instance().getCurrentUser()).getId())) {
                            check = true;  //se userId = mio Id
                            break;
                        }
                    }

                    ConfirmationDialogFragment dialog;

                    if(check)
                        dialog = ConfirmationDialogFragment.newLeaveEvent(activity);

                    else
                        dialog = ConfirmationDialogFragment.newJoinEvent(activity);

                    dialog.confirmClickListener = () -> {
                        viewModel.updateEventMembers(id, Objects.requireNonNull(ChatClient.instance().getCurrentUser()).getId());
                    };

                    dialog.show(activity.getSupportFragmentManager(), null);
                }

                @Override
                public void onError(Object object) {

                }
            });
        });

        return binding.getRoot();
    }

}

package it.piedpiper.unity.callback;

public abstract class FirebaseCallBack {


    //per essere in costante aggiornamento quando cambuani dati --> posso usarli per non eseguire ogni volta una query
    public abstract void onChildAdded(Object object);

    public abstract void onChildChanged(Object object);

    public abstract void onChildRemoved(Object object);

    public abstract void onCancelled(Object object);
}

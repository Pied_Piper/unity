package it.piedpiper.unity.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import it.piedpiper.unity.model.FilePdf;

@Dao
public interface FileDao {
    @Query("SELECT * FROM FilePdf WHERE id=:id")
    FilePdf getFile(String id); //id=:id") si riferisce  a questo String id

    //se c'e conflitto  ignore e vai comunue
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(FilePdf filePdf);

    @Query("DELETE FROM FilePdf WHERE id=:id")
    void delete(String id);

    @Query("DELETE FROM FilePdf")
    void deleteAll();

    @Query("SELECT * FROM FilePdf")
    List<FilePdf> getAll();
}

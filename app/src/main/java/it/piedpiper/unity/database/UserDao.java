package it.piedpiper.unity.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import it.piedpiper.unity.model.User;

@Dao
public interface UserDao  {
    @Query("SELECT * FROM user")
    LiveData<User> getUserLiveData();

    @Query("SELECT * FROM user")
    User getUser();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(User user);

    @Query("DELETE FROM user WHERE id=:id")
    void delete(String id);

    @Query("DELETE FROM user")
    void deleteAll();
}

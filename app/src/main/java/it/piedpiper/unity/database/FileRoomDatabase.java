package it.piedpiper.unity.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import it.piedpiper.unity.model.FilePdf;
import it.piedpiper.unity.utils.Constant;

//estende databse --> dabatase contiene entita di tipo filePd.class

//per ottenere istanza di file dao --> metodo getDatabase
@Database(entities = {FilePdf.class}, version = 1)
public abstract class FileRoomDatabase extends RoomDatabase {
    public abstract FileDao fileDao();

    private static volatile FileRoomDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 4;
    public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    public static FileRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {  //in questo caso creo istanza
            synchronized (FileRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            FileRoomDatabase.class, Constant.FILE_DATABASE) //nome del databse
                            .build(); //creazione databse memorizzato in ISTANCE
                }
            }
        }
        return INSTANCE;
    }
}

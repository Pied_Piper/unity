package it.piedpiper.unity.repository;

import android.net.Uri;
import android.widget.ImageView;

import it.piedpiper.unity.callback.CallBack;
import it.piedpiper.unity.model.FilePdf;

public interface IStorageRepository {
    void putImage(String id, ImageView mUserPhotoImg, CallBack callBack);
    void putImage(String id, byte[] data, CallBack callBack);
    void putFilePDF(String name, String category, Uri uri,  CallBack callBack);

    void getAllFilesPDF(CallBack callBack);
    void getFilesPDF(String category, CallBack callBack);

    void downloadPDF(FilePdf filePdf, CallBack callBack);

    void deleteFilePDF(String id, String storageUrl, CallBack callBack);

    //DAO
    void getAllFilesPDFDao(CallBack callBack);
    void getFilePDFDao(String id, CallBack callBack);
    void deleteFilePDFDao(String id, CallBack callBack);
    void deleteAllFilesPDFDao(CallBack callBack);
}

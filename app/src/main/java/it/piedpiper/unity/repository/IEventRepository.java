package it.piedpiper.unity.repository;

import java.text.ParseException;
import java.util.HashMap;

import it.piedpiper.unity.callback.CallBack;
import it.piedpiper.unity.model.Event;

public interface IEventRepository {
    //void createEventGroup(Event event, CallBack callBack);
    void createEvent(Event event, CallBack callBack);

    void getAllEvents(CallBack callBack);

    void getEventsByUserDate(String userId, String date, CallBack callBack) throws ParseException;

    void getEventsByUserDate(String userId, String startDate, String endDate, CallBack callBack) throws ParseException;

    void updateEvent(String id, HashMap<String, Object> map, CallBack callBack);

    void getEventById(String id, CallBack callBack);

    //1 parameter
    void getEventsByUniversity(String university, CallBack callBack);
    void getEventsByCourse(String course, CallBack callBack);
    void getEventsByPlace(String place, CallBack callBack);
    void getEventsByDate(String date, CallBack callBack) throws ParseException;
    void getEventsFromDate(String date, CallBack callBack) throws ParseException;



    //2 parameters
    void getEventsByUniversityCourse(String universsity, String course, CallBack callBack);
    void getEventsByUniversityPlace(String university, String place, CallBack callBack);
    void getEventsByUniversityDate(String university, String date, CallBack callBack) throws ParseException;
    void getEventsByCoursePlace(String course, String place, CallBack callBack);
    void getEventsByCourseDate(String course, String date, CallBack callBack) throws ParseException;
    void getEventsByPlaceDate(String place, String date, CallBack callBack) throws ParseException;



    //3 parameters
    void getEventsByUniversityCoursePlace(String university, String course, String place, CallBack callBack);
    void getEventsByUniversityCourseDate(String university, String course, String date, CallBack callBack) throws ParseException;
    void getEventsByUniversityPlaceDate(String university, String place, String date, CallBack callBack) throws ParseException;
    void getEventsByCoursePlaceDate(String course, String place, String date, CallBack callBack) throws ParseException;


    //4 parameters
    void getEventsByUniversityCoursePlaceDate(String university, String course, String place, String date, CallBack callBack) throws ParseException;


    void readAllEvents(CallBack callBack);
}

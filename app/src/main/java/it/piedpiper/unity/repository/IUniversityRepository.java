package it.piedpiper.unity.repository;


import it.piedpiper.unity.callback.CallBack;
import it.piedpiper.unity.model.University;

public interface IUniversityRepository {
    enum JsonParser {
        JSON_READER,
        JSON_OBJECT_ARRAY,
        GSON,
        JSON_ERROR
    };

    void createUniversity(University university, CallBack callBack);
    void fetchUniversitiesName(String name, CallBack callBack);
    void fetchUniversitiesCountry(String country, CallBack callBack);
    void fetchUniversities(String name, String country, CallBack callBack);
    void fetchAllUniversities(CallBack callBack);

    void getUniversityByName(String name, CallBack callBack);
    void readAllUniversities(CallBack callBack);
}

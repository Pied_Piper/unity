package it.piedpiper.unity.repository.impl;

import static it.piedpiper.unity.firebase.FirebaseConstants.ASSOCIATIONS_COLLECTION;
import static it.piedpiper.unity.firebase.FirebaseDatabaseReference.DATABASE;
import static it.piedpiper.unity.utils.Constant.FAIL;

import android.app.Activity;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import it.piedpiper.unity.callback.CallBack;
import it.piedpiper.unity.firebase.FirebaseRepository;
import it.piedpiper.unity.model.Association;
import it.piedpiper.unity.repository.IAssociationRepository;
import it.piedpiper.unity.utils.Utility;
import it.piedpiper.unity.utils.dialog.LoadingDialog;
import it.piedpiper.unity.utils.dialog.ProgressDialogClass;


//gardare firebaserepository
//implemneta interfaccia dei metodi --> tutti dichiarati dnetro la classe
public class AssociationRepository extends FirebaseRepository implements IAssociationRepository {
    private final CollectionReference associationCollectionReference;
    private final ProgressDialogClass progressDialog;
    private final LoadingDialog loadingDialog;

    public AssociationRepository(Activity activity) {
        this.progressDialog = new ProgressDialogClass(activity);
        this.loadingDialog = new LoadingDialog(activity);
        this.associationCollectionReference = DATABASE.collection(ASSOCIATIONS_COLLECTION);
    }

    @Override
    public void createAssociation(Association association, CallBack callBack) {
        if (association != null) {
            String id =  associationCollectionReference.document().getId();  //creazioen id
            association.setId(id); //setto id alla associazione
            loadingDialog.start();
            DocumentReference documentReference = associationCollectionReference.document(id);
            fireStoreCreate(documentReference, association.getMap(), new CallBack() {  //in firestorecrete posso passare qualsiasi tipo di oggetto
                @Override
                public void onSuccess(Object object) {
                    loadingDialog.stop();
                    callBack.onSuccess(association);
                }

                @Override
                public void onError(Object object) {
                    //loadingDialog.stop();
                    callBack.onError(object);
                }
            });
        } else {
            callBack.onError(FAIL);
        }
    }

    @Override
    public void updateMembers(String associationID, Association association, CallBack callBack){
        if (!Utility.isEmptyOrNull(associationID)) {
            DocumentReference documentReference = associationCollectionReference.document(associationID);
            fireStoreUpdate(documentReference, association.getMap(), new CallBack() {
                @Override
                public void onSuccess(Object object) {
                    callBack.onSuccess(association);
                }

                @Override
                public void onError(Object object) {
                    callBack.onError(object);
                }
            });
        } else {
            callBack.onError(FAIL);
        }
    }

    @Override
    public void getAssociationByUniversity(String university, CallBack callBack) {
        progressDialog.showDialog();
        Query query = associationCollectionReference.whereEqualTo("university", university);
        readQueryDocuments(query, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                if (object != null) {
                    callBack.onSuccess(getDataFromQuerySnapshot(object));
                } else
                    callBack.onSuccess(null);
                progressDialog.dismissDialog();
            }

            @Override
            public void onError(Object object) {
                progressDialog.dismissDialog();
                callBack.onError(object);
            }
        });
    }

    @Override
    public void getAssociationById(String id, CallBack callBack) {
        DocumentReference documentReference = associationCollectionReference.document(id);
        readDocument(documentReference, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                DocumentSnapshot document = (DocumentSnapshot) object;
                if (document.exists()) {
                    Association association = document.toObject(Association.class);
                    callBack.onSuccess(association);
                } else {
                    callBack.onSuccess(null);
                }
            }

            @Override
            public void onError(Object object) {
                callBack.onError(object);
            }
        });
    }

    public List<Association> getDataFromQuerySnapshot(Object object) {
        List<Association> associations = new ArrayList<>();
        QuerySnapshot queryDocumentSnapshots = (QuerySnapshot) object;
        for (DocumentSnapshot snapshot : queryDocumentSnapshots) {
            Association association = snapshot.toObject(Association.class);
            associations.add(association);
        }
        return associations;
    }
}
///quando ho insimee di gruppi eseguo getQueryfrom...
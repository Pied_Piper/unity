package it.piedpiper.unity.repository;

import it.piedpiper.unity.callback.CallBack;

public interface ILogoRepository {
    void getLogo(String url, CallBack callBack);
}

package it.piedpiper.unity.repository.impl;

import static it.piedpiper.unity.firebase.FirebaseConstants.EVENTS_COLLECTION;
import static it.piedpiper.unity.firebase.FirebaseDatabaseReference.DATABASE;
import static it.piedpiper.unity.utils.Constant.FAIL;
import static it.piedpiper.unity.utils.Constant.SUCCESS;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.icu.text.SimpleDateFormat;
import android.util.Log;
import android.widget.ProgressBar;

import com.google.firebase.Timestamp;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import it.piedpiper.unity.callback.CallBack;
import it.piedpiper.unity.firebase.FirebaseRepository;
import it.piedpiper.unity.model.Event;
import it.piedpiper.unity.repository.IEventRepository;
import it.piedpiper.unity.utils.Utility;
import it.piedpiper.unity.utils.dialog.ProgressDialogClass;

public class EventRepository extends FirebaseRepository implements IEventRepository {

    private static final String TAG = "EventRepository";
    private final CollectionReference eventsCollectionReference;
    private final ProgressDialogClass progressDialog;

    public EventRepository(Activity activity) {
        eventsCollectionReference = DATABASE.collection(EVENTS_COLLECTION);
        progressDialog = new ProgressDialogClass(activity);
    }

    @Override
    public void createEvent(Event event, CallBack callBack) {
        if (event != null) {
            //loadingDialog.start();
            String pushId = eventsCollectionReference.document().getId();
            event.setId(pushId);
            DocumentReference documentReference = eventsCollectionReference.document(pushId);
            fireStoreCreate(documentReference, event, new CallBack() {
                @Override
                public void onSuccess(Object object) {
                    //loadingDialog.stop();
                    callBack.onSuccess(event);

                }

                @Override
                public void onError(Object object) {
                    //loadingDialog.stop();
                    callBack.onError(object);
                }
            });
        } else {
            callBack.onError(FAIL);
        }
    }

    @Override
    public void getAllEvents(CallBack callBack) {
        Query query = eventsCollectionReference.orderBy("date");
        readQueryDocuments(query, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                if (object != null) {
                    callBack.onSuccess(getDataFromQuerySnapshot(object));
                } else
                    callBack.onSuccess(null);
            }

            @Override
            public void onError(Object object) {
                callBack.onError(object);
            }
        });
    }

    @Override
    public void getEventsByUserDate(String userId, String date, CallBack callBack) throws ParseException {
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
        Timestamp startDate = new Timestamp(sdf.parse(date + " 00:00:00"));
        Timestamp endDate = new Timestamp(sdf.parse(date + " 23:59:59"));

        Log.d(TAG, "getEventsByUserDate: " + startDate.toDate());

        Query query = eventsCollectionReference
                .whereArrayContains("members", userId)
                .whereGreaterThanOrEqualTo("date", startDate)
                .whereLessThanOrEqualTo("date", endDate)
                .orderBy("date");
        readQueryDocuments(query, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                if (object != null) {
                    callBack.onSuccess(getDataFromQuerySnapshot(object));
                } else
                    callBack.onSuccess(null);
            }

            @Override
            public void onError(Object object) {
                callBack.onError(object);
            }
        });
    }

    @Override
    public void getEventsByUserDate(String userId, String startDate, String endDate, CallBack callBack) throws ParseException {
        //progressDialog.showDialog();
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");

        Timestamp s = new Timestamp(sdf.parse(startDate + " 00:00:00"));
        Timestamp e = new Timestamp(sdf.parse(endDate + " 23:59:59"));

        Log.d(TAG, "getEventsByUserDate: " + startDate + " " + s.toDate());
        Log.d(TAG, "getEventsByUserDate: " + endDate + " " + e.toDate());

        Query query = eventsCollectionReference
                .whereArrayContains("members", userId)
                .whereGreaterThanOrEqualTo("date", s)
                .whereLessThanOrEqualTo("date", e)
                .orderBy("date");
        readQueryDocuments(query, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                if (object != null) {
                    callBack.onSuccess(getDataFromQuerySnapshot(object));
                } else
                    callBack.onSuccess(null);
                //progressDialog.dismissDialog();
            }

            @Override
            public void onError(Object object) {
                callBack.onError(object);
            }
        });
    }



    @Override
    public void updateEvent(String id, HashMap<String, Object> map, CallBack callBack) {
        if (!Utility.isEmptyOrNull(id)) {
            DocumentReference documentReference = eventsCollectionReference.document(id);
            fireStoreUpdate(documentReference, map, new CallBack() {
                @Override
                public void onSuccess(Object object) {
                    callBack.onSuccess(SUCCESS);
                }

                @Override
                public void onError(Object object) {
                    callBack.onError(object);
                }
            });
        } else {
            callBack.onError(FAIL);
        }
    }

    @Override
    public void getEventById(String id, CallBack callBack) {
        DocumentReference document = eventsCollectionReference.document(id);
        readDocument(document, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                DocumentSnapshot document = (DocumentSnapshot) object;
                if (document.exists()) {
                    Event event = document.toObject(Event.class);
                    callBack.onSuccess(event);
                } else {
                    callBack.onSuccess(null);
                }
            }

            @Override
            public void onError(Object object) {

            }
        });
    }

    @Override
    public void getEventsByUniversity(String university, CallBack callBack) {
        progressDialog.showDialog();
        Query query = eventsCollectionReference.whereEqualTo("university", university).orderBy("date");
        readQueryDocuments(query, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                if (object != null) {
                    callBack.onSuccess(getDataFromQuerySnapshot(object));
                } else
                    callBack.onSuccess(null);

                progressDialog.dismissDialog();
            }

            @Override
            public void onError(Object object) {
                callBack.onError(object);
            }
        });
    }


    @Override
    public void getEventsByCourse(String course, CallBack callBack) {
        progressDialog.showDialog();
        Query query = eventsCollectionReference.whereEqualTo("course", course).orderBy("date");
        readQueryDocuments(query, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                if (object != null) {
                    callBack.onSuccess(getDataFromQuerySnapshot(object));
                } else
                    callBack.onSuccess(null);

                progressDialog.dismissDialog();
            }

            @Override
            public void onError(Object object) {
                callBack.onError(object);
            }
        });
    }

    @Override
    public void getEventsByPlace(String place, CallBack callBack) {
        progressDialog.showDialog();
        Query query = eventsCollectionReference.whereEqualTo("place", place).orderBy("date");
        readQueryDocuments(query, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                if (object != null) {
                    callBack.onSuccess(getDataFromQuerySnapshot(object));
                } else
                    callBack.onSuccess(null);

                progressDialog.dismissDialog();
            }

            @Override
            public void onError(Object object) {
                callBack.onError(object);
            }
        });
    }

    @Override
    public void getEventsByDate(String date, CallBack callBack) throws ParseException {
        progressDialog.showDialog();
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
        Timestamp startDate = new Timestamp(sdf.parse(date + " 00:00:00"));
        Timestamp endDate = new Timestamp(sdf.parse(date + " 23:59:59"));

        Query query = eventsCollectionReference
                .whereGreaterThanOrEqualTo("date", startDate)
                .whereLessThanOrEqualTo("date", endDate)
                .orderBy("date");
        readQueryDocuments(query, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                if (object != null) {
                    callBack.onSuccess(getDataFromQuerySnapshot(object));
                } else
                    callBack.onSuccess(null);

                progressDialog.dismissDialog();
            }

            @Override
            public void onError(Object object) {
                callBack.onError(object);
            }
        });
    }

    @Override
    public void getEventsFromDate(String date, CallBack callBack) throws ParseException {
        progressDialog.showDialog();
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
        Timestamp startDate = new Timestamp(sdf.parse(date + " 00:00:00"));

        Query query = eventsCollectionReference
                .whereGreaterThanOrEqualTo("date", startDate)
                .orderBy("date");
        readQueryDocuments(query, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                if (object != null) {
                    callBack.onSuccess(getDataFromQuerySnapshot(object));
                } else
                    callBack.onSuccess(null);

                progressDialog.dismissDialog();
            }

            @Override
            public void onError(Object object) {
                callBack.onError(object);
            }
        });
    }


    @Override
    public void getEventsByUniversityCourse(String university, String course, CallBack callBack) {
        progressDialog.showDialog();
        Query query = eventsCollectionReference
                .whereEqualTo("university", university)
                .whereEqualTo("course", course)
                .orderBy("date");
        readQueryDocuments(query, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                if (object != null) {
                    callBack.onSuccess(getDataFromQuerySnapshot(object));
                } else
                    callBack.onSuccess(null);
                progressDialog.dismissDialog();
            }

            @Override
            public void onError(Object object) {
                callBack.onError(object);
            }
        });
    }

    @Override
    public void getEventsByUniversityPlace(String university, String place, CallBack callBack) {
        progressDialog.showDialog();
        Query query = eventsCollectionReference
                .whereEqualTo("university", university)
                .whereEqualTo("place", place)
                .orderBy("date");
        readQueryDocuments(query, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                if (object != null) {
                    callBack.onSuccess(getDataFromQuerySnapshot(object));
                } else
                    callBack.onSuccess(null);
                progressDialog.dismissDialog();
            }

            @Override
            public void onError(Object object) {
                callBack.onError(object);
            }
        });
    }

    @Override
    public void getEventsByUniversityDate(String university, String date, CallBack callBack) throws ParseException {
        progressDialog.showDialog();
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
        Timestamp startDate = new Timestamp(sdf.parse(date + " 00:00:00"));
        Timestamp endDate = new Timestamp(sdf.parse(date + " 23:59:59"));

        Query query = eventsCollectionReference
                .whereEqualTo("university", university)
                .whereGreaterThanOrEqualTo("date", startDate)
                .whereLessThanOrEqualTo("date", endDate)
                .orderBy("date");
        readQueryDocuments(query, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                if (object != null) {
                    callBack.onSuccess(getDataFromQuerySnapshot(object));
                } else
                    callBack.onSuccess(null);
                progressDialog.dismissDialog();
            }

            @Override
            public void onError(Object object) {
                callBack.onError(object);
            }
        });
    }

    @Override
    public void getEventsByCoursePlace(String course, String place, CallBack callBack) {
        progressDialog.showDialog();
        Query query = eventsCollectionReference.whereEqualTo("course", course).orderBy("date");
        readQueryDocuments(query, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                if (object != null) {
                    callBack.onSuccess(getDataFromQuerySnapshot(object));
                } else
                    callBack.onSuccess(null);
                progressDialog.dismissDialog();
            }

            @Override
            public void onError(Object object) {
                callBack.onError(object);
            }
        });
    }

    @Override
    public void getEventsByCourseDate(String course, String date, CallBack callBack) throws ParseException {
        progressDialog.showDialog();
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
        Timestamp startDate = new Timestamp(sdf.parse(date + " 00:00:00"));
        Timestamp endDate = new Timestamp(sdf.parse(date + " 23:59:59"));

        Query query = eventsCollectionReference
                .whereEqualTo("course", course)
                .whereGreaterThanOrEqualTo("date", startDate)
                .whereLessThanOrEqualTo("date", endDate)
                .orderBy("date");
        readQueryDocuments(query, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                if (object != null) {
                    callBack.onSuccess(getDataFromQuerySnapshot(object));
                } else
                    callBack.onSuccess(null);
                progressDialog.dismissDialog();
            }

            @Override
            public void onError(Object object) {
                callBack.onError(object);
            }
        });
    }

    @Override
    public void getEventsByPlaceDate(String place, String date, CallBack callBack) throws ParseException {
        progressDialog.showDialog();
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
        Timestamp startDate = new Timestamp(sdf.parse(date + " 00:00:00"));
        Timestamp endDate = new Timestamp(sdf.parse(date + " 23:59:59"));

        Query query = eventsCollectionReference
                .whereEqualTo("place", place)
                .whereGreaterThanOrEqualTo("date", startDate)
                .whereLessThanOrEqualTo("date", endDate)
                .orderBy("date");
        readQueryDocuments(query, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                if (object != null) {
                    callBack.onSuccess(getDataFromQuerySnapshot(object));
                } else
                    callBack.onSuccess(null);
                progressDialog.dismissDialog();
            }

            @Override
            public void onError(Object object) {
                callBack.onError(object);
            }
        });
    }

    @Override
    public void getEventsByUniversityCoursePlace(String university, String course, String place, CallBack callBack) {
        progressDialog.showDialog();
        Query query = eventsCollectionReference
                .whereEqualTo("university", university)
                .whereEqualTo("course", course)
                .whereEqualTo("place", place)
                .orderBy("date");
        readQueryDocuments(query, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                if (object != null) {
                    callBack.onSuccess(getDataFromQuerySnapshot(object));
                } else
                    callBack.onSuccess(null);
                progressDialog.dismissDialog();
            }

            @Override
            public void onError(Object object) {
                callBack.onError(object);
            }
        });
    }

    @Override
    public void getEventsByUniversityCourseDate(String university, String course, String date, CallBack callBack) throws ParseException {
        progressDialog.showDialog();
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
        Timestamp startDate = new Timestamp(sdf.parse(date + " 00:00:00"));
        Timestamp endDate = new Timestamp(sdf.parse(date + " 23:59:59"));

        Query query = eventsCollectionReference
                .whereEqualTo("university", university)
                .whereEqualTo("course", course)
                .whereGreaterThanOrEqualTo("date", startDate)
                .whereLessThanOrEqualTo("date", endDate)
                .orderBy("date");

        readQueryDocuments(query, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                if (object != null) {
                    callBack.onSuccess(getDataFromQuerySnapshot(object));
                } else
                    callBack.onSuccess(null);
                progressDialog.dismissDialog();
            }

            @Override
            public void onError(Object object) {
                callBack.onError(object);
            }
        });
    }

    @Override
    public void getEventsByUniversityPlaceDate(String university, String place, String date, CallBack callBack) throws ParseException {
        progressDialog.showDialog();
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
        Timestamp startDate = new Timestamp(sdf.parse(date + " 00:00:00"));
        Timestamp endDate = new Timestamp(sdf.parse(date + " 23:59:59"));

        Query query = eventsCollectionReference
                .whereEqualTo("university", university)
                .whereEqualTo("place", place)
                .whereGreaterThanOrEqualTo("date", startDate)
                .whereLessThanOrEqualTo("date", endDate)
                .orderBy("date");
        readQueryDocuments(query, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                if (object != null) {
                    callBack.onSuccess(getDataFromQuerySnapshot(object));
                } else
                    callBack.onSuccess(null);
                progressDialog.dismissDialog();
            }

            @Override
            public void onError(Object object) {
                callBack.onError(object);
            }
        });
    }

    @Override
    public void getEventsByCoursePlaceDate(String course, String place, String date, CallBack callBack) throws ParseException {
        progressDialog.showDialog();
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
        Timestamp startDate = new Timestamp(sdf.parse(date + " 00:00:00"));
        Timestamp endDate = new Timestamp(sdf.parse(date + " 23:59:59"));

        Query query = eventsCollectionReference
                .whereEqualTo("course", course)
                .whereEqualTo("place", place)
                .whereGreaterThanOrEqualTo("date", startDate)
                .whereLessThanOrEqualTo("date", endDate)
                .orderBy("date");
        readQueryDocuments(query, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                if (object != null) {
                    callBack.onSuccess(getDataFromQuerySnapshot(object));
                } else
                    callBack.onSuccess(null);
                progressDialog.dismissDialog();
            }

            @Override
            public void onError(Object object) {
                callBack.onError(object);
            }
        });
    }



    @Override
    public void getEventsByUniversityCoursePlaceDate(String university, String course, String place, String date, CallBack callBack) throws ParseException {
        progressDialog.showDialog();
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
        Timestamp startDate = new Timestamp(sdf.parse(date + " 00:00:00"));
        Timestamp endDate = new Timestamp(sdf.parse(date + " 23:59:59"));

        Query query = eventsCollectionReference
                .whereEqualTo("university", university)
                .whereEqualTo("course", course)
                .whereEqualTo("place", place)
                .whereGreaterThanOrEqualTo("date", startDate)
                .whereLessThanOrEqualTo("date", endDate)
                .orderBy("date");
        readQueryDocuments(query, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                if (object != null) {
                    callBack.onSuccess(getDataFromQuerySnapshot(object));
                } else
                    callBack.onSuccess(null);
                progressDialog.dismissDialog();
            }

            @Override
            public void onError(Object object) {
                callBack.onError(object);
            }
        });
    }

    @Override
    public void readAllEvents(CallBack callBack) {
        Query query = eventsCollectionReference.orderBy("date");
        readQueryDocuments(query, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                if (object != null) {
                    callBack.onSuccess(getDataFromQuerySnapshot(object));
                } else
                    callBack.onSuccess(null);
            }

            @Override
            public void onError(Object object) {
                callBack.onError(object);
            }
        });
    }

    public List<Event> getDataFromQuerySnapshot(Object object) {
        List<Event> events = new ArrayList<>();
        QuerySnapshot queryDocumentSnapshots = (QuerySnapshot) object;
        for (DocumentSnapshot snapshot : queryDocumentSnapshots) {
            Event event = snapshot.toObject(Event.class);
            events.add(event);
        }
        return events;
    }


}

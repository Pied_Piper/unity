package it.piedpiper.unity.repository.impl;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import io.getstream.chat.android.client.ChatClient;
import it.piedpiper.unity.R;
import it.piedpiper.unity.api.ChatApp;
import it.piedpiper.unity.callback.CallBack;
import it.piedpiper.unity.callback.FirebaseCallBack;
import it.piedpiper.unity.database.FileRoomDatabase;
import it.piedpiper.unity.database.UserDao;
import it.piedpiper.unity.database.UserRoomDatabase;
import it.piedpiper.unity.firebase.FirebaseDatabaseReference;
import it.piedpiper.unity.firebase.FirebaseRepository;
import it.piedpiper.unity.model.AuthenticationResponse;
import it.piedpiper.unity.model.FilePdf;
import it.piedpiper.unity.model.User;
import it.piedpiper.unity.repository.IStorageRepository;
import it.piedpiper.unity.repository.IUserRepository;
import it.piedpiper.unity.ui.login.LoginActivity;
import it.piedpiper.unity.utils.Constant;
import it.piedpiper.unity.utils.Converter;
import it.piedpiper.unity.utils.dialog.ProgressDialogClass;
import it.piedpiper.unity.utils.SharedPreferencesProvider;
import it.piedpiper.unity.utils.Utility;

import static it.piedpiper.unity.utils.Constant.FAIL;
import static it.piedpiper.unity.utils.Constant.SUCCESS;
import static it.piedpiper.unity.firebase.FirebaseConstants.USERS_COLLECTION;
import static it.piedpiper.unity.firebase.FirebaseDatabaseReference.DATABASE;

import androidx.core.content.ContextCompat;
import androidx.lifecycle.MutableLiveData;

public class UserRepository extends FirebaseRepository implements IUserRepository {

    private static final String TAG = "UserRepository";

    private final ProgressDialogClass progressDialog;
    private final Activity activity;
    private final CollectionReference usersCollectionReference;
    private final SharedPreferencesProvider mSharedPreferencesProvider;
    private final MutableLiveData<AuthenticationResponse> mAuthenticationResponseLiveData;
    private final UserDao mUserDao;

    private final IStorageRepository storageRepository;

    private final FirebaseAuth mAuth;

    public UserRepository(Activity activity) {
        this.activity = activity;
        this.progressDialog = new ProgressDialogClass(activity);
        usersCollectionReference = DATABASE.collection(USERS_COLLECTION);
        mAuth = FirebaseDatabaseReference.AUTH;
        mSharedPreferencesProvider = new SharedPreferencesProvider(activity.getApplication());
        mAuthenticationResponseLiveData = new MutableLiveData<>();
        UserRoomDatabase db = UserRoomDatabase.getDatabase(activity);
        mUserDao = db.userDao();

        storageRepository = new StorageRepository(activity);
    }

    @Override
    public void signInWithEmail(String email, String password, CallBack callBack) {
        progressDialog.showDialog();
        if (email != null && !email.isEmpty() && password != null && !password.isEmpty())  {
            mAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(ContextCompat.getMainExecutor(activity), task -> {
                        AuthenticationResponse authenticationResponse;
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithEmail:success");
                            authenticationResponse = new AuthenticationResponse();
                            FirebaseUser user = mAuth.getCurrentUser();
                            assert user != null;
                            authenticationResponse.setSuccess(true);
                            mSharedPreferencesProvider.setAuthenticationToken(user.getIdToken(false).getResult().getToken());

                            callBack.onSuccess(SUCCESS);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.d(TAG, "signInWithEmail:failure", task.getException());
                            authenticationResponse = new AuthenticationResponse();
                            authenticationResponse.setSuccess(false);
                            progressDialog.dismissDialog();
                            if (task.getException() != null) {
                                authenticationResponse.setMessage(task.getException().getLocalizedMessage());
                            } else {
                                authenticationResponse.setMessage(activity.getString(R.string.registration_failure));
                            }
                        }
                        //mAuthenticationResponseLiveData.postValue(authenticationResponse);
                        callBack.onError(FAIL);
                    });
        }
    }

    @Override
    public MutableLiveData<AuthenticationResponse> createUserWithEmail(String email, String password) {
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(ContextCompat.getMainExecutor(activity), task -> {
                    AuthenticationResponse authenticationResponse = new AuthenticationResponse();
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "createUserWithEmail:success");
                        FirebaseUser user = mAuth.getCurrentUser();
                        assert user != null;
                        user.sendEmailVerification();
                        authenticationResponse.setSuccess(true);
                        mSharedPreferencesProvider.setAuthenticationToken(user.getIdToken(false).getResult().getToken());
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.d(TAG, "createUserWithEmail:failure", task.getException());
                        authenticationResponse.setSuccess(false);
                        if (task.getException() != null) {
                            authenticationResponse.setMessage(task.getException().getLocalizedMessage());
                        } else {
                            authenticationResponse.setMessage(activity.getApplication().getString(R.string.registration_failure));
                        }
                    }
                    mAuthenticationResponseLiveData.postValue(authenticationResponse);
                });
        return mAuthenticationResponseLiveData;
    }

    @Override
    public void logout(CallBack callBack) {
        mAuth.signOut();

        storageRepository.getAllFilesPDFDao(new CallBack() {
            @Override
            public void onSuccess(Object object) {
                List<FilePdf> files = (List<FilePdf>) object;
                if(files != null){
                    for(FilePdf file : files){
                        File f = new File(file.getStorageUrl());
                        if(f.delete())
                            Log.d(TAG, "onSuccess: file deleted");
                    }
                }
            }

            @Override
            public void onError(Object object) {
                callBack.onError(FAIL);
            }
        });

        UserRoomDatabase.databaseWriteExecutor.execute(mUserDao::deleteAll);
        FileRoomDatabase.databaseWriteExecutor.execute(() -> FileRoomDatabase.getDatabase(activity).fileDao().deleteAll());
        SharedPreferences sharedPref = activity.getSharedPreferences(Constant.SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.clear().apply();

        ChatApp.disconnectUser();

        Intent intent = new Intent(activity, LoginActivity.class);
        intent.putExtra(Constant.FRAGMENT_STRING, Constant.LOGIN_STRING);
        activity.startActivity(intent);
        activity.finish();

        callBack.onSuccess(SUCCESS);
    }

    @Override
    public ListenerRegistration readUser(String email, FirebaseCallBack callBack) {
        Query query = usersCollectionReference.whereEqualTo("email", email).limit(1);
        return readQueryDocumentsByChildEventListener(query, new FirebaseCallBack() {
            @Override
            public void onChildAdded(Object object) {
                if (object != null) {
                    DocumentSnapshot document = (DocumentSnapshot) object;
                    if (document.exists()) {
                        User user = document.toObject(User.class);
                        callBack.onChildAdded(user);
                    } else {
                        callBack.onChildAdded(null);
                    }
                } else {
                    callBack.onChildAdded(null);
                }
            }

            @Override
            public void onChildChanged(Object object) {
                if (object != null) {
                    DocumentSnapshot document = (DocumentSnapshot) object;
                    if (document.exists()) {
                        User user = document.toObject(User.class);
                        callBack.onChildChanged(user);
                    } else {
                        callBack.onChildChanged(null);
                    }
                } else {
                    callBack.onChildChanged(null);
                }
            }

            @Override
            public void onChildRemoved(Object object) {
                if (object != null) {
                    DocumentSnapshot document = (DocumentSnapshot) object;
                    if (document.exists()) {
                        User user = document.toObject(User.class);
                        callBack.onChildRemoved(user);
                    } else {
                        callBack.onChildRemoved(null);
                    }
                } else {
                    callBack.onChildRemoved(null);
                }
            }

            @Override
            public void onCancelled(Object object) {
                callBack.onCancelled(object);
            }
        });
    }

    @Override
    public void readUserDao(CallBack callBack) {
        UserRoomDatabase.databaseWriteExecutor.execute(() -> { //uso thread
            User user = mUserDao.getUser();  //controllare mUserDao

            if(user != null)
                callBack.onSuccess(user);
            else
                callBack.onError(null);
        });
    }

    @Override
    public void createUserDao(User user) {
        if(progressDialog.isShowing())
            progressDialog.dismissDialog();

        UserRoomDatabase.databaseWriteExecutor.execute(() -> {
            if(!user.getPhotoUrl().isEmpty())
                user.setPhoto(Converter.getByteArrayImage(user.getPhotoUrl()));  //memorizzo foto come array di bite
            mUserDao.insert(user);
        });
    }

    @Override
    public void deleteUserDao(String id) {
        UserRoomDatabase.databaseWriteExecutor.execute(() -> mUserDao.delete(id));
    }

    @Override
    public void createUser(User user, CallBack callBack) {
        String pushId = usersCollectionReference.document().getId();
        if (user != null && !Utility.isEmptyOrNull(pushId)) {
            progressDialog.showDialog();
            user.setId(pushId);
            DocumentReference documentReference = usersCollectionReference.document(pushId);
            fireStoreCreate(documentReference, user, new CallBack() {
                @Override
                public void onSuccess(Object object) {
                    progressDialog.dismissDialog();
                    mSharedPreferencesProvider.setUserId(user.getId());
                    callBack.onSuccess(SUCCESS);
                }

                @Override
                public void onError(Object object) {
                    progressDialog.dismissDialog();
                    callBack.onError(object);
                }
            });
        } else {
            callBack.onError(FAIL);
        }
    }

    @Override
    public void updateUser(String userID, User user, CallBack callBack) {
        if (!Utility.isEmptyOrNull(userID)) {
            progressDialog.showDialog();
            DocumentReference documentReference = usersCollectionReference.document(userID);
            fireStoreUpdate(documentReference, user.getMap(), new CallBack() {
                @Override
                public void onSuccess(Object object) {
                    progressDialog.dismissDialog();
                    mSharedPreferencesProvider.setUserSetted(true);
                    mSharedPreferencesProvider.setUserId(userID);

                    if (ChatClient.instance().getCurrentUser() != null){
                        deleteUserDao(userID);
                        createUserDao(user);
                        Objects.requireNonNull(ChatClient.instance().getCurrentUser()).setImage(user.getPhotoUrl());
                        Objects.requireNonNull(ChatClient.instance().getCurrentUser()).setName(user.getUsername());
                    }

                    callBack.onSuccess(user);
                }

                @Override
                public void onError(Object object) {
                    progressDialog.dismissDialog();
                    callBack.onError(object);
                }
            });
        } else {
            callBack.onError(FAIL);
        }
    }

    @Override
    public void deleteUser(String userID, CallBack callBack) {
        if (!Utility.isEmptyOrNull(userID)) {
            progressDialog.showDialog();
            DocumentReference documentReference = usersCollectionReference.document(userID);
            fireStoreDelete(documentReference, new CallBack() {
                @Override
                public void onSuccess(Object object) {
                    progressDialog.dismissDialog();
                    callBack.onSuccess(SUCCESS);
                }

                @Override
                public void onError(Object object) {
                    progressDialog.dismissDialog();
                    callBack.onError(object);
                }
            });
        } else {
            callBack.onError(FAIL);
        }
    }

    @Override
    public void readUserByID(String userID, CallBack callBack) {
        if (!Utility.isEmptyOrNull(userID)) {
            //progressDialog.showDialog();
            DocumentReference documentReference = usersCollectionReference.document(userID);
            readDocument(documentReference, new CallBack() {
                @Override
                public void onSuccess(Object object) {
                    if (object != null) {
                        DocumentSnapshot document = (DocumentSnapshot) object;
                        if (document.exists()) {
                            User user = document.toObject(User.class);
                            callBack.onSuccess(user);
                        } else {
                            callBack.onSuccess(null);
                        }
                    } else
                        callBack.onSuccess(null);
                    //progressDialog.dismissDialog();
                }

                @Override
                public void onError(Object object) {
                    //progressDialog.dismissDialog();
                    callBack.onError(object);
                }
            });
        } else {
            callBack.onError(FAIL);
        }
    }

    @Override
    public void readUserByEmail(String email, CallBack callBack) {
        if (!Utility.isEmptyOrNull(email)) {
            //progressDialog.showDialog();
            Log.d(TAG, "readUserByEmail: " + email.toLowerCase());
            Query query = usersCollectionReference.whereEqualTo("email", email.toLowerCase()).limit(1);
            readDocument(query, new CallBack() {
                @Override
                public void onSuccess(Object object) {
                    if (object != null) {
                        DocumentSnapshot document = (DocumentSnapshot) object;
                        if (document.exists()) {
                            User user = document.toObject(User.class);
                            callBack.onSuccess(user);
                        } else {
                            callBack.onSuccess(null);
                        }
                    } else
                        callBack.onSuccess(null);
                    //progressDialog.dismissDialog();
                }

                @Override
                public void onError(Object object) {
                    //progressDialog.dismissDialog();
                    callBack.onError(object);
                }
            });
        } else {
            callBack.onError(FAIL);
        }
    }

    @Override
    public void readUserByUsername(String username, CallBack callBack) {
        if (!Utility.isEmptyOrNull(username)) {
            //progressDialog.showDialog();
            Query query = usersCollectionReference.whereEqualTo("username", username).limit(1);
            readDocument(query, new CallBack() {
                @Override
                public void onSuccess(Object object) {
                    if (object != null) {
                        DocumentSnapshot document = (DocumentSnapshot) object;
                        if (document.exists()) {
                            User user = document.toObject(User.class);
                            callBack.onSuccess(user);
                        } else {
                            callBack.onSuccess(null);
                        }
                    } else {
                        callBack.onSuccess(null);
                    }
                    //progressDialog.dismissDialog();
                }

                @Override
                public void onError(Object object) {
                    Log.d("TAG", "onSuccess: " + object);
                    callBack.onError(object);
                }
            });
        } else {
            callBack.onError(FAIL);
        }
    }

    @Override
    public void readAllUsersBySingleValueEvent(final CallBack callBack) {
        progressDialog.showDialog();
        //get all employees order by employee name
        Query query = usersCollectionReference.orderBy("username");
        readQueryDocuments(query, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                if (object != null) {
                    callBack.onSuccess(getDataFromQuerySnapshot(object));
                } else
                    callBack.onSuccess(null);
                progressDialog.dismissDialog();
            }

            @Override
            public void onError(Object object) {
                progressDialog.dismissDialog();
                callBack.onError(object);
            }
        });
    }

    @Override
    public ListenerRegistration readAllUsersByDataChangeEvent(CallBack callBack) {
        progressDialog.showDialog();
        //get all employees order by employee name
        Query query = usersCollectionReference.orderBy("username");
        return readQueryDocumentsByListener(query, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                if (object != null) {
                    callBack.onSuccess(getDataFromQuerySnapshot(object));
                } else
                    callBack.onSuccess(null);
                progressDialog.dismissDialog();
            }

            @Override
            public void onError(Object object) {
                progressDialog.dismissDialog();
                callBack.onError(object);
            }
        });
    }

    @Override
    public ListenerRegistration readAllUsersByChildEvent(FirebaseCallBack firebaseCallBack) {
        progressDialog.showDialog();
        //get all employees order by created date time
        Query query = usersCollectionReference.orderBy("username");
        return readQueryDocumentsByChildEventListener(query, new FirebaseCallBack() {
            @Override
            public void onChildAdded(Object object) {
                if (object != null) {
                    DocumentSnapshot document = (DocumentSnapshot) object;
                    if (document.exists()) {
                        User user = document.toObject(User.class);
                        firebaseCallBack.onChildAdded(user);
                    } else {
                        firebaseCallBack.onChildAdded(null);
                    }
                } else {
                    firebaseCallBack.onChildAdded(null);
                }
                progressDialog.dismissDialog();
            }

            @Override
            public void onChildChanged(Object object) {
                if (object != null) {
                    DocumentSnapshot document = (DocumentSnapshot) object;
                    if (document.exists()) {
                        User user = document.toObject(User.class);
                        firebaseCallBack.onChildChanged(user);
                    } else {
                        firebaseCallBack.onChildChanged(null);
                    }
                } else {
                    firebaseCallBack.onChildChanged(null);
                }
            }

            @Override
            public void onChildRemoved(Object object) {
                if (object != null) {
                    DocumentSnapshot document = (DocumentSnapshot) object;
                    if (document.exists()) {
                        User user = document.toObject(User.class);
                        firebaseCallBack.onChildRemoved(user);
                    } else {
                        firebaseCallBack.onChildRemoved(null);
                    }
                } else {
                    firebaseCallBack.onChildRemoved(null);
                }
            }

            @Override
            public void onCancelled(Object object) {
                firebaseCallBack.onCancelled(object);
                progressDialog.dismissDialog();
            }
        });
    }

    public List<User> getDataFromQuerySnapshot(Object object) {
        List<User> users = new ArrayList<>();
        QuerySnapshot queryDocumentSnapshots = (QuerySnapshot) object;
        for (DocumentSnapshot snapshot : queryDocumentSnapshots) {
            User user = snapshot.toObject(User.class);
            users.add(user);
        }
        return users;
    }
}

package it.piedpiper.unity.repository;

import it.piedpiper.unity.callback.CallBack;
import it.piedpiper.unity.model.Lesson;

public interface ILessonRepository {

    void createLesson(Lesson lesson, CallBack callBack);

    void getLessonById(String id, CallBack callBack);

    //2 parameters
    void getLessonsBySubjectPlace(String subject, String place, CallBack callBack);

    //3 parameters
    void getLessonsBySubjectPlacePrice(String subject, String place, String price, CallBack callBack);

    void readAllLessons(CallBack callBack);
}

package it.piedpiper.unity.repository.impl;

import static it.piedpiper.unity.firebase.FirebaseConstants.FILES_COLLECTION;
import static it.piedpiper.unity.firebase.FirebaseDatabaseReference.DATABASE;
import static it.piedpiper.unity.utils.Constant.SUCCESS;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.widget.ImageView;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import it.piedpiper.unity.callback.CallBack;
import it.piedpiper.unity.database.FileDao;
import it.piedpiper.unity.database.FileRoomDatabase;
import it.piedpiper.unity.firebase.FirebaseDatabaseReference;
import it.piedpiper.unity.firebase.FirebaseRepository;
import it.piedpiper.unity.model.FilePdf;
import it.piedpiper.unity.repository.IStorageRepository;
import it.piedpiper.unity.utils.UriUtils;
import it.piedpiper.unity.utils.dialog.LoadingDialog;
import it.piedpiper.unity.utils.dialog.ProgressDialogClass;

public class StorageRepository extends FirebaseRepository implements IStorageRepository {

    private static final String TAG = "StorageRepository";
    private final Activity activity;
    private final CollectionReference filesCollectionReference;
    private final LoadingDialog loadingDialog;
    private final ProgressDialogClass progressDialog;

    private final FileDao fileDao;

    private final StorageReference storageRef;
    private StorageReference reference;

    public StorageRepository(Activity activity) {
        this.activity = activity;
        this.filesCollectionReference = DATABASE.collection(FILES_COLLECTION);
        FirebaseStorage storage = FirebaseDatabaseReference.STORAGE;
        this.storageRef = storage.getReference();

        fileDao = FileRoomDatabase.getDatabase(activity).fileDao();

        loadingDialog = new LoadingDialog(activity);
        progressDialog = new ProgressDialogClass(activity);

        File sdCardRoot = new File(Environment.getExternalStorageDirectory(), "UniTy");

        if (!sdCardRoot.exists()) {
            sdCardRoot.mkdirs();
        }
    }

    private void insertFileDao(FilePdf filePdf){
        FileRoomDatabase.databaseWriteExecutor.execute(() -> {
            fileDao.insert(filePdf);
            Log.d(TAG, "insertFileDao: file inserted in dao " + filePdf.getStorageUrl());
        });
    }

    @Override
    public void putImage(String id, ImageView mUserPhotoImg, CallBack callBack) {
        reference = storageRef.child("images/" + id  + ".jpg");

        mUserPhotoImg.setDrawingCacheEnabled(true);
        mUserPhotoImg.buildDrawingCache();
        Bitmap bitmap = ((BitmapDrawable) mUserPhotoImg.getDrawable()).getBitmap();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();

        storageCreate(reference, data, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                reference.getDownloadUrl().addOnSuccessListener(callBack::onSuccess);
            }

            @Override
            public void onError(Object object) {
                callBack.onSuccess(object);
            }
        });
    }

    @Override
    public void putImage(String id, byte[] data, CallBack callBack) {
        reference = storageRef.child("images/" + id  + ".jpg");

        storageCreate(reference, data, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                reference.getDownloadUrl().addOnSuccessListener(callBack::onSuccess);
            }

            @Override
            public void onError(Object object) {
                callBack.onSuccess(object);
            }
        });
    }

    @Override
    public void putFilePDF(String name, String category, Uri uri, CallBack callBack) {
        loadingDialog.start();
        String id = filesCollectionReference.document().getId();
        reference = storageRef.child("files/" + id  + ".pdf");
        storageCreate(reference, uri, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                reference.getDownloadUrl().addOnSuccessListener(url -> {
                    DocumentReference documentReference = filesCollectionReference.document(id);
                    FilePdf filePdf = new FilePdf(name, url.toString(), category);
                    filePdf.setId(id);
                    fireStoreCreate(documentReference, filePdf, new CallBack() {
                        @Override
                        public void onSuccess(Object object) {
                            //filePdf.setStorageUrl(UriUtils.getPathFromUri(activity, uri));
                            //Log.d(TAG, "onSuccess: " + filePdf.getStorageUrl());
                            //insertFileDao(filePdf);
                            loadingDialog.stop(filePdf.getName() + ".pdf\n" + "uploaded!");
                            downloadPDF(filePdf, callBack);
                            callBack.onSuccess(SUCCESS);
                        }

                        @Override
                        public void onError(Object object) {
                            callBack.onError(object);
                        }
                    });
                });
            }

            @Override
            public void onError(Object object) {

            }
        });
    }

    @Override
    public void getAllFilesPDF(CallBack callBack) {
        readQueryDocuments(filesCollectionReference, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                if (object != null) {
                    callBack.onSuccess(getDataFromQuerySnapshot(object));
                } else {
                    callBack.onError(null);
                }
            }

            @Override
            public void onError(Object object) {
                callBack.onError(object);
            }
        });
    }

    @Override
    public void getFilesPDF(String category, CallBack callBack) {
        progressDialog.showDialog();
        Query query = filesCollectionReference
                .whereEqualTo("category", category);
        readQueryDocuments(query, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                if (object != null) {
                    progressDialog.dismissDialog();
                    callBack.onSuccess(getDataFromQuerySnapshot(object));
                } else {
                    progressDialog.dismissDialog();
                    callBack.onError(null);
                }
            }

            @Override
            public void onError(Object object) {
                callBack.onError(object);
            }
        });
    }

    @Override
    public void downloadPDF(FilePdf filePdf, CallBack callBack) {
        Uri uri = Uri.parse(filePdf.getUri());
        Log.d(TAG, "downloadPDF: " + uri.toString());
        DownloadManager.Request request = new DownloadManager.Request(uri);
        request.setDestinationInExternalFilesDir(activity, "UniTy", filePdf.getName() + ".pdf");
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE);
        request.allowScanningByMediaScanner();
        DownloadManager manager = (DownloadManager) activity.getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);

        filePdf.setStorageUrl(activity.getExternalFilesDir("UniTy") + "/" + filePdf.getName() + ".pdf");
        Log.d(TAG, "downloadPDF: " + filePdf.getStorageUrl());

        BroadcastReceiver onComplete = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d(TAG, "onReceive: download of file " + filePdf.getName() + " completed");
                insertFileDao(filePdf);
                callBack.onSuccess(filePdf);
            }
        };

        onComplete.onReceive(activity, null);
}

    @Override
    public void getAllFilesPDFDao(CallBack callBack) {
        FileRoomDatabase.databaseWriteExecutor.execute(() -> callBack.onSuccess(fileDao.getAll()));
    }

    @Override
    public void getFilePDFDao(String id, CallBack callBack) {
        FileRoomDatabase.databaseWriteExecutor.execute(() -> {
            FilePdf file = fileDao.getFile(id);
            if(file != null)
                callBack.onSuccess(fileDao.getFile(id));
            else
                callBack.onError(null);
        });
    }

    @Override
    public void deleteFilePDFDao(String id, CallBack callBack) {
        FileRoomDatabase.databaseWriteExecutor.execute(() -> {
            fileDao.delete(id);

            callBack.onSuccess(SUCCESS);
        });
    }

    @Override
    public void deleteAllFilesPDFDao(CallBack callBack) {
        FileRoomDatabase.databaseWriteExecutor.execute(() -> {
            fileDao.deleteAll();

            callBack.onSuccess(SUCCESS);
        });
    }

    @Override
    public void deleteFilePDF(String id, String storageUrl, CallBack callBack) {
        File file = new File(storageUrl);

        if(file.delete())
            deleteFilePDFDao(id, callBack);
    }

    public List<FilePdf> getDataFromQuerySnapshot(Object object) {
        List<FilePdf> filePdfs = new ArrayList<>();
        QuerySnapshot queryDocumentSnapshots = (QuerySnapshot) object;
        for (DocumentSnapshot snapshot : queryDocumentSnapshots) {
            FilePdf filePdf = snapshot.toObject(FilePdf.class);
            filePdfs.add(filePdf);
        }
        return filePdfs;
    }
}

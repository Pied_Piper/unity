package it.piedpiper.unity.repository.impl;

import android.util.Log;

import java.util.List;

import io.getstream.chat.android.client.ChatClient;
import io.getstream.chat.android.client.call.Call;
import io.getstream.chat.android.client.channel.ChannelClient;
import io.getstream.chat.android.client.models.Channel;
import io.getstream.chat.android.client.models.User;
import it.piedpiper.unity.callback.CallBack;
import it.piedpiper.unity.repository.IChannelRepository;

public class ChannelRepository implements IChannelRepository {
    private final ChatClient chatClient;
    private static final String TAG = "ChannelRepository";
    public ChannelRepository(){
        chatClient = ChatClient.instance();
    }

    @Override
    public void createChannel(Channel channel, List<String> members, CallBack callBack) {
        ChannelClient channelClient = chatClient.channel(channel.getType(), channel.getId());
        channelClient.create(members, channel.getExtraData()).enqueue(result -> {
            if (result.isSuccess()) {
                Log.d(TAG, "createChannel: channel created");
                Channel channelStream = result.data();
                callBack.onSuccess(channelStream);
            } else {
                Log.d(TAG, "createChannel: channel not created" + result.error());
            }
        });

    }

    @Override
    public void deleteChannel(Channel channel) {
        Call<Channel> call = chatClient.deleteChannel(channel.getType(), channel.getId());
        call.enqueue();
    }

    @Override
    public void joinChannel(Channel channel, String user) {
        ChannelClient channelClient = chatClient.channel(channel.getCid());

        Call<Channel> call = channelClient.addMembers(user);
        call.enqueue();
    }

    @Override
    public void leaveChannel(Channel channel, User user) {
        ChannelClient channelClient = chatClient.channel(channel.getCid());

        Call<Channel> call = channelClient.removeMembers(user.getId());
        call.enqueue();
    }
}

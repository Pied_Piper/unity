package it.piedpiper.unity.repository.impl;

import static it.piedpiper.unity.utils.Constant.FAIL;

import android.util.Log;

import androidx.annotation.NonNull;


import it.piedpiper.unity.api.ServiceLocator;
import it.piedpiper.unity.api.service.LogoApiService;
import it.piedpiper.unity.callback.CallBack;
import it.piedpiper.unity.model.Logo;
import it.piedpiper.unity.model.LogoResponse;
import it.piedpiper.unity.repository.ILogoRepository;
import it.piedpiper.unity.utils.Constant;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LogoRepository implements ILogoRepository {
    private static final String TAG = "LogoRepository";
    private final LogoApiService mLogoApiService;


    public LogoRepository() {
        this.mLogoApiService = ServiceLocator.getInstance().getLogoApiService();
    }

    @Override
    public void getLogo(String url, CallBack callBack) {
        Call<LogoResponse> logoResponseCall = mLogoApiService.getLogo(url, "Bearer " + Constant.LOGO_API_KEY);

        logoResponseCall.enqueue(new Callback<LogoResponse>() {
            @Override
            public void onResponse(@NonNull Call<LogoResponse> call, @NonNull Response<LogoResponse> response) {
                Log.d(TAG, "onResponse: " + call.request());

                if (response.body() != null && response.isSuccessful()) {
                    Logo logo = response.body().getDomain();
                    //saveDataInDatabase(newsList);
                    callBack.onSuccess(logo);
                } else {
                    callBack.onError(FAIL);
                }
            }

            @Override
            public void onFailure(@NonNull Call<LogoResponse> call, @NonNull Throwable t) {
                Log.d(TAG, "onError: " + call.request());
                Log.d(TAG, "onError: " + t.getMessage());
                callBack.onError(t.getMessage());
            }
        });
    }
}

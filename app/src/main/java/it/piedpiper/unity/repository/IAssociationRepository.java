package it.piedpiper.unity.repository;

import it.piedpiper.unity.callback.CallBack;
import it.piedpiper.unity.model.Association;

public interface IAssociationRepository {
    void createAssociation(Association association, CallBack callBack);

    void updateMembers(String associationID, Association association, CallBack callBack);

    void getAssociationByUniversity(String university, CallBack callBack);
    void getAssociationById(String id, CallBack callBack);
}

package it.piedpiper.unity.repository.impl;

import static it.piedpiper.unity.firebase.FirebaseConstants.UNIVERSITIES_COLLECTION;
import static it.piedpiper.unity.firebase.FirebaseDatabaseReference.DATABASE;
import static it.piedpiper.unity.utils.Constant.FAIL;
import static it.piedpiper.unity.utils.Constant.SUCCESS;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import it.piedpiper.unity.api.ServiceLocator;
import it.piedpiper.unity.api.service.UniversitiesApiService;
import it.piedpiper.unity.callback.CallBack;
import it.piedpiper.unity.firebase.FirebaseRepository;
import it.piedpiper.unity.model.University;
import it.piedpiper.unity.repository.IUniversityRepository;
import it.piedpiper.unity.utils.Utility;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UniversityRepository extends FirebaseRepository implements IUniversityRepository {
    private static final String TAG = "UniversityRepository";
    private final CollectionReference universitiesCollectionReference;
    private final UniversitiesApiService mUniversitiesApiService;


    public UniversityRepository() {
        this.universitiesCollectionReference = DATABASE.collection(UNIVERSITIES_COLLECTION);
        this.mUniversitiesApiService = ServiceLocator.getInstance().getUniversitiesApiService();
    }

    @Override
    public void createUniversity(University university, CallBack callBack) {
        if (university != null) {
            //loadingDialog.start();
            String pushId = universitiesCollectionReference.document().getId();
            DocumentReference documentReference = universitiesCollectionReference.document(pushId);
            fireStoreCreate(documentReference, university, new CallBack() {
                @Override
                public void onSuccess(Object object) {
                    //loadingDialog.stop();
                    callBack.onSuccess(SUCCESS);

                }

                @Override
                public void onError(Object object) {
                    //loadingDialog.stop();
                    callBack.onError(object);
                }
            });
        } else {
            callBack.onError(FAIL);
        }
    }

    @Override
    public void fetchUniversitiesName(String name, CallBack callBack) {
        Call<List<University>> universitiesResponseCall = mUniversitiesApiService.getUniversities(name);

        universitiesResponseCall.enqueue(new Callback<List<University>>() {
            @Override
            public void onResponse(@NonNull Call<List<University>> call, @NonNull Response<List<University>> response) {
                if (response.body() != null && response.isSuccessful()) {
                    List<University> universities = response.body();
                    //saveDataInDatabase(newsList);
                    callBack.onSuccess(universities);
                } else {
                    callBack.onError(FAIL);
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<University>> call, @NonNull Throwable t) {
                callBack.onError(t.getMessage());
            }
        });
    }

    @Override
    public void fetchUniversitiesCountry(String country, CallBack callBack) {
        Call<List<University>> universitiesResponseCall = mUniversitiesApiService.getUniversitiesCountry(country);

        universitiesResponseCall.enqueue(new Callback<List<University>>() {
            @Override
            public void onResponse(@NonNull Call<List<University>> call, @NonNull Response<List<University>> response) {

                Log.d(TAG, "onResponse: " + call.request());

                if (response.body() != null && response.isSuccessful()) {
                    List<University> universities = new ArrayList<>();
                    for(University university : response.body()){
                        boolean check = false;
                        for(University uni : universities){
                            if(university.getName().equals(uni.getName())){
                                check = true;
                                break;
                            }
                        }
                        if(!check){
                            universities.add(university);
                        }
                    }
                    //saveDataInDatabase(newsList);
                    callBack.onSuccess(universities);
                } else {
                    callBack.onError(FAIL);
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<University>> call, @NonNull Throwable t) {
                callBack.onError(t.getMessage());
            }
        });
    }

    @Override
    public void fetchUniversities(String name, String country, CallBack callBack) {
        Call<List<University>> universitiesResponseCall = mUniversitiesApiService.getUniversities(name, country);

        universitiesResponseCall.enqueue(new Callback<List<University>>() {
            @Override
            public void onResponse(@NonNull Call<List<University>> call, @NonNull Response<List<University>> response) {
                if (response.body() != null && response.isSuccessful()) {
                    List<University> universities = response.body();
                    //saveDataInDatabase(newsList);
                    callBack.onSuccess(universities);
                } else {
                    callBack.onError(FAIL);
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<University>> call, @NonNull Throwable t) {
                callBack.onError(t.getMessage());
            }
        });
    }

    @Override
    public void fetchAllUniversities(CallBack callBack) {
        Call<List<University>> universitiesResponseCall = mUniversitiesApiService.getAllUniversities();

        universitiesResponseCall.enqueue(new Callback<List<University>>() {
            @Override
            public void onResponse(@NonNull Call<List<University>> call, @NonNull Response<List<University>> response) {
                if (response.body() != null && response.isSuccessful()) {
                    List<University> universities = response.body();
                    //saveDataInDatabase(newsList);
                    callBack.onSuccess(universities);
                } else {
                    callBack.onError(FAIL);
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<University>> call, @NonNull Throwable t) {
                callBack.onError(t.getMessage());
            }
        });
    }

    @Override
    public void getUniversityByName(String name, CallBack callBack) {
        if (!Utility.isEmptyOrNull(name)) {
            Query query = universitiesCollectionReference.whereEqualTo("name", name).limit(1);
            readDocument(query, new CallBack() {
                @Override
                public void onSuccess(Object object) {
                    if (object != null) {
                        DocumentSnapshot document = (DocumentSnapshot) object;
                        if (document.exists()) {
                            University university = document.toObject(University.class);
                            callBack.onSuccess(university);
                        } else {
                            callBack.onSuccess(null);
                        }
                    } else
                        callBack.onSuccess(null);
                }

                @Override
                public void onError(Object object) {
                    callBack.onError(object);
                }
            });
        } else {
            callBack.onError(FAIL);
        }
    }

    @Override
    public void readAllUniversities(CallBack callBack) {
        Query query = universitiesCollectionReference.orderBy("name");
        readQueryDocuments(query, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                if (object != null) {
                    callBack.onSuccess(getDataFromQuerySnapshot(object));
                } else
                    callBack.onSuccess(null);
            }

            @Override
            public void onError(Object object) {
                callBack.onError(object);
            }
        });
    }

    public List<University> getDataFromQuerySnapshot(Object object) {
        List<University> universities = new ArrayList<>();
        QuerySnapshot queryDocumentSnapshots = (QuerySnapshot) object;
        for (DocumentSnapshot snapshot : queryDocumentSnapshots) {
            University uni = snapshot.toObject(University.class);
            universities.add(uni);
        }
        return universities;
    }
}

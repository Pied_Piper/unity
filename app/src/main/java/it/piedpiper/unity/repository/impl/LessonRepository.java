package it.piedpiper.unity.repository.impl;

import static it.piedpiper.unity.firebase.FirebaseConstants.LESSONS_COLLECTION;
import static it.piedpiper.unity.firebase.FirebaseDatabaseReference.DATABASE;
import static it.piedpiper.unity.utils.Constant.FAIL;
import static it.piedpiper.unity.utils.Constant.SUCCESS;

import android.app.Activity;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import it.piedpiper.unity.callback.CallBack;
import it.piedpiper.unity.firebase.FirebaseRepository;
import it.piedpiper.unity.model.Lesson;
import it.piedpiper.unity.repository.ILessonRepository;
import it.piedpiper.unity.utils.dialog.LoadingDialog;
import it.piedpiper.unity.utils.dialog.ProgressDialogClass;
import it.piedpiper.unity.utils.Utility;

public class LessonRepository extends FirebaseRepository implements ILessonRepository {
    private final LoadingDialog loadingDialog;
    private final ProgressDialogClass progressDialog;
    private final CollectionReference lessonsCollectionReference;

    public LessonRepository(Activity activity) {
        lessonsCollectionReference = DATABASE.collection(LESSONS_COLLECTION);
        loadingDialog = new LoadingDialog(activity);
        progressDialog = new ProgressDialogClass(activity);
    }

    @Override
    public void createLesson(Lesson lesson, CallBack callBack) {
        String pushId = lessonsCollectionReference.document().getId();
        lesson.setId(pushId);
        if (!Utility.isEmptyOrNull(pushId)) {
            loadingDialog.start();
            //lesson.setId(pushId);
            DocumentReference documentReference = lessonsCollectionReference.document(pushId);
            fireStoreCreate(documentReference, lesson, new CallBack() {
                @Override
                public void onSuccess(Object object) {
                    loadingDialog.stop();
                    callBack.onSuccess(SUCCESS);
                }

                @Override
                public void onError(Object object) {
                    loadingDialog.stop();
                    callBack.onError(object);
                }
            });
        } else {
            callBack.onError(FAIL);
        }
    }

    @Override
    public void getLessonById(String id, CallBack callBack) {
        DocumentReference documentReference = lessonsCollectionReference.document(id);
        readDocument(documentReference, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                DocumentSnapshot document = (DocumentSnapshot) object;
                if (document.exists()) {
                    Lesson lesson = document.toObject(Lesson.class);
                    callBack.onSuccess(lesson);
                } else {
                    callBack.onSuccess(null);
                }
            }

            @Override
            public void onError(Object object) {
                callBack.onError(object);
            }
        });
    }

    @Override
    public void getLessonsBySubjectPlace(String subject, String place, CallBack callBack) {
        progressDialog.showDialog();
        Query query = lessonsCollectionReference
                .whereEqualTo("subject", subject)
                .whereEqualTo("place", place);
        readQueryDocuments(query, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                if (object != null) {
                    progressDialog.dismissDialog();
                    callBack.onSuccess(getDataFromQuerySnapshot(object));
                } else {
                    progressDialog.dismissDialog();
                    callBack.onError(null);
                }
            }

            @Override
            public void onError(Object object) {
                callBack.onError(object);
            }
        });
    }

    @Override
    public void getLessonsBySubjectPlacePrice(String subject, String place, String price, CallBack callBack) {
        progressDialog.showDialog();

        Query query = lessonsCollectionReference
                .whereEqualTo("subject", subject)
                .whereEqualTo("place", place)
                .whereLessThanOrEqualTo("price", price);
        readQueryDocuments(query, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                if (object != null) {
                    progressDialog.dismissDialog();
                    callBack.onSuccess(getDataFromQuerySnapshot(object));
                } else {
                    progressDialog.dismissDialog();
                    callBack.onError(null);
                }
            }

            @Override
            public void onError(Object object) {
                callBack.onError(object);
            }
        });
    }

    @Override
    public void readAllLessons(CallBack callBack) {
        //progressDialog.showDialog();

        readQueryDocuments(lessonsCollectionReference, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                if (object != null) {
                    //progressDialog.dismissDialog();
                    callBack.onSuccess(getDataFromQuerySnapshot(object));
                } else {
                    //progressDialog.dismissDialog();
                    callBack.onError(null);
                }
            }

            @Override
            public void onError(Object object) {
                callBack.onError(object);
            }
        });
    }

    public List<Lesson> getDataFromQuerySnapshot(Object object) {
        List<Lesson> lessons = new ArrayList<>();
        QuerySnapshot queryDocumentSnapshots = (QuerySnapshot) object;
        for (DocumentSnapshot snapshot : queryDocumentSnapshots) {
            Lesson lesson = snapshot.toObject(Lesson.class);
            lessons.add(lesson);
        }
        return lessons;
    }
}

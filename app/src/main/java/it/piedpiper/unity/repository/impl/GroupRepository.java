package it.piedpiper.unity.repository.impl;

import static it.piedpiper.unity.firebase.FirebaseConstants.GROUPS_COLLECTION;
import static it.piedpiper.unity.firebase.FirebaseDatabaseReference.DATABASE;
import static it.piedpiper.unity.utils.Constant.FAIL;

import android.app.Activity;

import androidx.lifecycle.LifecycleOwner;
import androidx.paging.PagingConfig;

import com.firebase.ui.firestore.paging.FirestorePagingOptions;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import it.piedpiper.unity.callback.CallBack;
import it.piedpiper.unity.firebase.FirebaseRepository;
import it.piedpiper.unity.model.Group;
import it.piedpiper.unity.repository.IGroupRepository;
import it.piedpiper.unity.utils.Utility;
import it.piedpiper.unity.utils.dialog.LoadingDialog;
import it.piedpiper.unity.utils.dialog.ProgressDialogClass;

public class GroupRepository extends FirebaseRepository implements IGroupRepository {

    private final Activity activity;
    private final CollectionReference groupsCollectionReference;
    private final LoadingDialog loadingDialog;
    private final ProgressDialogClass progressDialog;

    public GroupRepository(Activity activity) {
        this.activity = activity;
        this.loadingDialog = new LoadingDialog(activity);
        this.progressDialog = new ProgressDialogClass(activity);
        this.groupsCollectionReference = DATABASE.collection(GROUPS_COLLECTION);
    }

    @Override
    public void createGroup(Group group, CallBack callBack) {
        if (group != null) {
            loadingDialog.start();
            String pushId = groupsCollectionReference.document().getId();
            DocumentReference documentReference = groupsCollectionReference.document(pushId);
            group.setId(pushId);
            fireStoreCreate(documentReference, group, new CallBack() {
                @Override
                public void onSuccess(Object object) {
                    loadingDialog.stop();
                    callBack.onSuccess(group);

                }

                @Override
                public void onError(Object object) {
                    loadingDialog.stop();
                    callBack.onError(object);
                }
            });
        } else {
            callBack.onError(FAIL);
        }
    }

    @Override
    public void getGroupById(String id, CallBack callBack) {
        DocumentReference documentReference = groupsCollectionReference.document(id);
        readDocument(documentReference, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                DocumentSnapshot document = (DocumentSnapshot) object;
                if (document.exists()) {
                    Group group = document.toObject(Group.class);
                    callBack.onSuccess(group);
                } else {
                    callBack.onSuccess(null);
                }
            }

            @Override
            public void onError(Object object) {
                callBack.onError(object);
            }
        });
    }

    @Override
    public void updateMembers(String groupID, Group group, CallBack callBack){
        if (!Utility.isEmptyOrNull(groupID)) {
            DocumentReference documentReference = groupsCollectionReference.document(groupID);
            fireStoreUpdate(documentReference, group.getMap(), new CallBack() {
                @Override
                public void onSuccess(Object object) {
                    callBack.onSuccess(group);
                }

                @Override
                public void onError(Object object) {
                    callBack.onError(object);
                }
            });
        } else {
            callBack.onError(FAIL);
        }
    }

    @Override
    public void getGroupsByUniversity(String university, CallBack callBack) {
        progressDialog.showDialog();
        Query query = groupsCollectionReference.whereEqualTo("university", university);
        readQueryDocuments(query, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                if (object != null) {
                    callBack.onSuccess(getDataFromQuerySnapshot(object));
                } else
                    callBack.onSuccess(null);
                progressDialog.dismissDialog();
            }

            @Override
            public void onError(Object object) {
                progressDialog.dismissDialog();
                callBack.onError(object);
            }
        });
    }

    @Override
    public void getGroupsByUniversityCourse(String university, String course, CallBack callBack) {
        Query query = groupsCollectionReference
                .whereEqualTo("university", university)
                .whereEqualTo("course", course);
        readQueryDocuments(query, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                if (object != null) {
                    callBack.onSuccess(getDataFromQuerySnapshot(object));
                } else
                    callBack.onSuccess(null);
            }

            @Override
            public void onError(Object object) {
                callBack.onError(object);
            }
        });
    }

    @Override
    public void getGroupsByUniversityCourseSubject(String university, String course, String subject, CallBack callBack) {
        progressDialog.showDialog();
        Query query = groupsCollectionReference
                .whereEqualTo("university", university)
                .whereEqualTo("course", course)
                .whereEqualTo("subject", subject);
        readQueryDocuments(query, new CallBack() {
            @Override
            public void onSuccess(Object object) {
                if (object != null) {
                    callBack.onSuccess(getDataFromQuerySnapshot(object));
                } else
                    callBack.onSuccess(null);
                progressDialog.dismissDialog();
            }

            @Override
            public void onError(Object object) {
                progressDialog.dismissDialog();
                callBack.onError(object);
            }
        });
    }

    @Override
    public FirestorePagingOptions<Group> getGroupsByUniversityCourseSubject(String university, String course, String subject){
        Query query = DATABASE.collection(GROUPS_COLLECTION)
                .whereEqualTo("university", university)
                .whereEqualTo("course", course)
                .whereEqualTo("subject", subject);

        PagingConfig config = new PagingConfig(20, 10, false);
        return new FirestorePagingOptions.Builder<Group>()
                .setLifecycleOwner((LifecycleOwner) activity)
                .setQuery(query, config, Group.class)
                .build();
    }

    public List<Group> getDataFromQuerySnapshot(Object object) {
        List<Group> groups = new ArrayList<>();
        QuerySnapshot queryDocumentSnapshots = (QuerySnapshot) object;
        for (DocumentSnapshot snapshot : queryDocumentSnapshots) {
            Group group = snapshot.toObject(Group.class);
            groups.add(group);
        }
        return groups;
    }
}

package it.piedpiper.unity.repository;

import java.util.List;
import io.getstream.chat.android.client.models.Channel;
import io.getstream.chat.android.client.models.User;
import it.piedpiper.unity.callback.CallBack;

public interface IChannelRepository {
    void createChannel(Channel channel, List<String> members, CallBack callBack);
    void deleteChannel(Channel channel);

    void joinChannel(Channel channel, String user);
    void leaveChannel(Channel channel, User user);
}

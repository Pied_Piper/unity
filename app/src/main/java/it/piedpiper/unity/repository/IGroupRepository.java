package it.piedpiper.unity.repository;

import com.firebase.ui.firestore.paging.FirestorePagingOptions;

import it.piedpiper.unity.callback.CallBack;
import it.piedpiper.unity.model.Group;

public interface IGroupRepository {
    void createGroup(Group group, CallBack callBack);
    void getGroupById(String id, CallBack callBack);

    void updateMembers(String groupID, Group group, CallBack callBack);

    void getGroupsByUniversity(String university, CallBack callBack);
    void getGroupsByUniversityCourse(String university, String course, CallBack callBack);
    void getGroupsByUniversityCourseSubject(String university, String course, String subject, CallBack callBack);

    FirestorePagingOptions<Group> getGroupsByUniversityCourseSubject(String university, String course, String subject);
}

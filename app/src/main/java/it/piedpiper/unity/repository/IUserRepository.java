package it.piedpiper.unity.repository;

import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.ListenerRegistration;

import it.piedpiper.unity.callback.CallBack;
import it.piedpiper.unity.callback.FirebaseCallBack;
import it.piedpiper.unity.model.AuthenticationResponse;
import it.piedpiper.unity.model.User;

public interface IUserRepository {

    void signInWithEmail(String email, String password, CallBack callBack);

    MutableLiveData<AuthenticationResponse> createUserWithEmail(String email, String password);
    void logout(CallBack callBack);


    //DAO
    void readUserDao(CallBack callBack);

    void createUserDao(User user);

    void deleteUserDao(String id);


    //FIRESTORE
    void createUser(User user, CallBack callBack);

    void updateUser(String userID, User user, CallBack callBack);

    void deleteUser(String userID, CallBack callBack);

    void readUserByID(String userID, CallBack callBack);

    void readUserByEmail(String email, CallBack callBack);

    void readUserByUsername(String username, CallBack callBack);

    void readAllUsersBySingleValueEvent(CallBack callBack);

    ListenerRegistration readUser(String email, FirebaseCallBack callBack);

    ListenerRegistration readAllUsersByDataChangeEvent(CallBack callBack);

    ListenerRegistration readAllUsersByChildEvent(FirebaseCallBack firebaseCallBack);
}
